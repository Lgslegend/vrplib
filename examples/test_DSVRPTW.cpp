#include "../VRP_instance.h"
#include "../VRP_solution.h"
#include "../VRP_scenario.h"
#include "../NM.h"
#include "../LS_Program.h"
#include "../tools.h"
#include <queue>
#include <cmath>
#include <limits>

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}

int main(int argc, char **argv) {
	if (argc < 2) {
		cout << "Usage: test_DSVRPTW instance_file" << endl;
		return 1;
	}
	const char* instance_file = argv[1];

	// srand(time(NULL));
	srand(1000);

	// INSTANCE FILE
	VRP_instance& I = VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(instance_file);
	cout << I.toString() << endl;
	// cout << I.toStringCoords(true) << endl;
	// cout << I.toStringDistances() << endl;
	// cout << I.toStringProbasTS() << endl << endl;

	// SOLUTION 
	Solution_DS_VRPTW s(I);
	s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
	s.addRoutes(I._getNumberVehicles());

	// NEIGHBORHOOD MANAGER
	NeighborhoodManager<Solution_DS_VRPTW> nm(s, DS_VRPTW);


	// MAIN
	s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
	cout << "Initial solution:" << endl << s.toString(false) << endl;
	cout << "Cost initial solution: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;
	
	cout << "LS ..." << endl;
	
	LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;
	// lsprog.setTimeOut(0.05);		
	// lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	lsprog.run(s, nm, 10000, 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)

	cout << s.toString(false) << endl;
	cout << "Cost: " << s.getCost()  << "   violations: " <<  s.getWeightViolations()<< endl;



	
	Scenario_SS_DS_VRPTW_CR scenario(I, DS_VRPTW, 0);

	// ====> Uncomment these lines to sample some requests and allow better anticipation
	cout << "Insert some sampled requests (" << scenario.getRequestSequence().size() << ") from time slot 1" << endl;


	for (const VRP_instance::RequestAttributes& req_attr : scenario.getRequestSequence()) {
		s.insertOnlineRequest(*req_attr.request);
	}
	cout << s.toString() << endl;
	lsprog.run(s, nm, 80000, 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	cout << s.toString() << endl;


	// cout << "REMOVE sampled requests" << endl;	// let's remove the sampled requests 
	// s.removeAllSampledOnlineRequests();
	// cout << s.toString() << endl;

	// s.insertWaitingVertex(*I.getVertexFromRegion(VRP_vertex::WAITING, 20),7,3,1.0);

	// const vector<VRP_instance::RequestAttributes> & appeared_online_requests = I.getAppearedOnlineRequests();
	// for (const VRP_instance::RequestAttributes& req : appeared_online_requests)	{
	// 	const VRP_vertex& v = * I.getVertexFromRegion(VRP_vertex::REGULAR_ONLINE, req.vertex->getRegion());
	// 	s.insertOnlineRequest(v, req.request->revealTS);
	// }

	// cout << s.toString() << endl;
	// lsprog.run(s, nm, 40000, 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	// cout << s.toString() << endl;
	cout << "Removing violated sampled requests ..." << endl;
	s.removeAllSampled_violated_OnlineRequests();
	cout << s.toString() << endl;



	const vector<VRP_instance::RequestAttributes> & appeared_online_requests = I.getAppearingOnlineRequests();
	cout << endl << "Iterating over the first 5 appeared requests..." << endl;	
	// int i = 0;
	for (const VRP_instance::RequestAttributes& req : appeared_online_requests) {
		ASSERT(req.vertex == & req.request->getVertex() && req.request != nullptr, "argh");
		
		cout << "Request appeared at region " << req.vertex->getRegion() << " , reveal time: " << req.reveal_time << " (time slot " << req.request->revealTS <<  ") ; vertex:" << req.vertex->toString(true) << endl;

		cout << "Set current time to " << req.reveal_time << endl;
		s.setCurrentTime(req.reveal_time);
		cout << s.toString() << endl;


		I.set_OnlineRequest_asAppeared(req.vertex->getRegion(), req.request->revealTS);		// inform the instance object that the request is from now marked as appeared

		cout << "Trying to insert request (time slot " << req.request->revealTS << ") from vertex " << req.vertex->toString() << " \t Request info: " << req.vertex->toString(true) << " ... ";
		bool success = s.tryInsertOnlineRequest(*req.vertex, req.request->revealTS);
		
		if (success) cout << " OK !" << endl;
		else cout << " No feasible place :(" << endl;


		// cout << "Optimizing (LS) ..." << endl;
		// // lsprog.setTimeOut(2.0);	
		// // lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		// lsprog.run(s, nm, 10000, 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		// cout << s.toString(false) << endl;
		// // cout << "Cost: " << s.getCost()  << "   violations: " <<  s.getWeightViolations() << endl;

		// if (++i == 5) break;
	}

	// cout << s.toString();
	// s.setCurrentTime(235);
	// cout << s.toString();
	// lsprog.run(s, nm, 10000, 10.0, 0.95, true);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	// cout << s.toString(false) << endl;


	// s.increaseWaitingTime(7, 3, 4.0);
	cout << s.toString();

	// cout << s.toString(true) << endl;
	cout << endl << "Current actions at time " << s.getCurrentTime() << ":" << endl;
	for (VehicleAction action : s.getCurrentActions()) {
		cout << "\t" << action.toString() << endl;			// instead you could directly access the VehicleAction fields, e.g.: action.vehicle, action.action, action.pvertex, etc.
	}

	cout << endl << "Next planned actions from time " << s.getCurrentTime() << ":" << endl;
	for (VehicleAction action : s.getNextPlannedActions()) {
		cout << "\t" << action.toString() << endl;
	}

	// cout << s.getRequestAtPosition(16, 2).getVertex().getRegion() << "  " << s.getRequestAtPosition(16, 2).revealTS << endl;

}



