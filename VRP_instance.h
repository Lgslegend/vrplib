/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/* VRP_instance +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	Everything that you need to represent a VRP (dertermistic,dynamic,stochastic) instance
	Classes:
		- VRP_instance : general data of the instance and container for vertices and/or requests
		- VRP_vertex : represents a vertex in a VRP (could also be the depot)
		- VRP_request : represents a request in a dynamic (stochastic) context, associated to a vertex

	An instance of type VRP_instance namely represents a problem instance. 
	It contains datastructures in order to provide a representation of each geographical region in a graph.
	For each of these regions, 4 VRP_vertex instance is created, each representing a type of vertex that 
	could be associated to that region: depot vertex, regular VRP vertex, waiting vertex (*) and finally
	customer vertex (**). 

	*	In dynamic context (and also in static and stochastic one), waiting vertices -aka. relocation vertices-
		can be inserted in a VRPTW solution in order to indicate the vehicle that it should wait at a given 
		location, for a given amount of time; waiting and relocation strategies are basic approaches for anticipiation.

	** 	In static and stochastic context, a customer vertex -aka. customer region- hosts a number of different 
		potential requests, usually one per time slot at which the request could potentially appear.

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#ifndef VRP_INSTANCE_H
#define	VRP_INSTANCE_H

#include <iostream>
#include <vector>
#include <set>
#include <map>
#include <cmath>
#include "tools.h"

using namespace std;

class VRP_request;
class VRP_vertex;
class VRP_instance;



/* VRP_vehicle    ------------------------------------------------------------------ */
class VRP_VehicleType {
public:
	VRP_VehicleType(int nRegions, string name = string(), int capacity = 0, double velocity = 1.0, double costPerDistanceUnit = 1.0);
	bool operator == (const VRP_VehicleType& other) const;
	bool operator != (const VRP_VehicleType& other) const { return ! operator==(other); }
	// ~VRP_VehicleType();

	/* accessors */
	string getName() const {return name;}
	int getCapacity() const {return capacity;}
	double getVelocity() const {return velocity;}
	double getCostPerDistUnit() const {return costPerDistanceUnit;}

	/* distances */
	double 	distance(const VRP_vertex& v1, const VRP_vertex& v2) const;			// returns the distance between two vertices
	double 	distance(int region1, int region2) const;							// idem between regions
	double 	travelTime(const VRP_vertex& v1, const VRP_vertex& v2) const;		// returns the travel time between two vertices, based on the distance and the vehicle velocity
	double 	travelTime(int region1, int region2) const;							// idem between regions
	double 	travelCost(const VRP_vertex& v1, const VRP_vertex& v2) const;		// returns the travel cost between two vertices, based on the distance and the cost per distance unit
	double 	travelCost(int region1, int region2) const;							// idem between regions

	/* other */
	double 	totalServiceTime(const VRP_request& r) const;						// returns the TOTAL service time required for some request (TOTAL = service time required at the vertex [specific to the vehicle] + additional time needed for that request [specific to the request])
	double 	serviceTime(const VRP_vertex& v) const;								// returns the service time (in time units) at some vertex
	double 	serviceTime(int region) const;										// idem with region number

	/* comm */
	string& toString(bool verbose=false) const;
	string& toStringDistances() const;
	string& toString_TravelTimes() const;
	string& toString_TravelCosts() const;

protected:
	friend class VRP_instance;
	friend class VRP_instance_file;
	
	string name;
	int capacity; 
	double velocity;
	double costPerDistanceUnit;

	int nRegions;
	vector< vector<double> > 	dist;	// stores the  distance matrix (in km)
	vector< vector<double> > 	travel_times;	// stores the  travel times matrix (in seconds)
	vector< double > 			service_times;	// stores the  service times
	bool integer_travel_times 		= false;
	bool travel_times_NE_distances 	= false;	// true iff travel_time(i,j) ≠ dist(i,j)*velocity

	void 	setServiceTime(int region, double t);							// set the service time required at some region

	void 	preComputeEuclideanDistances(const vector<double>& coordX, const vector<double>& coordY);		// compute the Euclidean distance matrix dist (typically called after parsed instance file)
	void 	setTravelDistance(int region1, int region2, double d);			// set the travel DISTANCE between two vertices
	void 	setTravelTime(int region1, int region2, double t);				// set the travel TIME and put travel_times_NE_distances to TRUE
};






/* VRP_request    ------------------------------------------------------------------ */

class VRP_request {
protected:
	const VRP_vertex* vertex;	// vertex hosting the request

public:
	friend class VRP_instance;
	friend class VRP_vertex;

	int 	id_instance_file;	// id as told in the instance file
	int 	id;					// id for internal use

	double 	e;    			// beginning of request's time window 	
	double 	l;    			// end of request's time window 		
	double 	add_service_time = 0;  	// additional service time for the request (in addition to the service time at the vertex (which is a VehicleType attribute))
	double 	demand; 		// demand of request 	

	int 	revealTS;		// reveal time slot of potential request 	

	double 	revealTime;		// reveal time of potential request  [!! used only in DS/SS-VRPTW-CR]	(time, not time slot !)	===> used when there is a precise reveal time on the potential request 
	double 	revealTime_min; // used when the request's reveal time is comprised in an interval (e.g. the potential request can appear between t=161 and t'=320)
	double  revealTime_max;	// ^^^

	double 	p;				// probability of the potential request (in %)
	bool 	appeared = true;// false if it is a sampled request; can only be false for type REGULAR_ONLINE

	const VRP_vertex& getVertex() const { ASSERT(vertex != NULL, ""); return *vertex; }

	VRP_request(const VRP_vertex* v=NULL) {
		vertex = v;
		// id = -1;
		id_instance_file = -1;
		e = -1;
		l = numeric_limits<int>::max();
		demand = 0;
		revealTime = -1;		
		revealTime_min = -1;	
		revealTime_max = -1;	
		revealTS = -1;
		p = 0;
	}
	void setAppeared();
	bool hasAppeared() const;

	bool operator==(const VRP_request& other) const;
	bool operator<(const VRP_request& other) const;		// "less than" comparison operator; used for ordering the requests; must be user defined ! 
														// if req_a < req_b, then req_a will be before req_b in the request assignment and ordering
	bool operator>=(const VRP_request& other) const { return ! operator<(other);}
	/* debug */
	bool checkConsistency() const;

	/* comm */
	string& toString(bool verbose=false) const;
	// ostream& operator<<(ostream &strm) { return strm << toString(); }
};




/* VRP_vertex    ------------------------------------------------------------------ */

class VRP_vertex {

public:

	enum VertexType {	// types of vertices
		DEPOT		,	// a vertex associated to the depot; usually does not host any request (there is one, but initialized with null values)
		// UNLOAD_DEPOT,
		REGULAR		,	// a vertex associated to a regular customer; used in the classical VRP(TW); a regular vertex hosts exactly one request
		CUSTOMER	,	// a vertex associated to a customer region; used in SS-VRPs only; a customer location hosts a list of potential requests
		WAITING		,	// a vertex associated to a waiting location; used in DS-VRPs and SS-VRPs; usually a waiting vertex does not normally host any request (there is one, but initialized with null values)
		REGULAR_ONLINE, // a vertex associated to a customer region; usually used in DS-VRPs in order to allow multiple requests in the at the same region
		NO_TYPE		
	};
		

	VRP_vertex(int region, VertexType type, int nTimeSlots, int instance_id=-1);
	bool operator == (const VRP_vertex& v) const;
	bool operator != (const VRP_vertex& v) const { return ! operator==(v); }
	// ~VRP_vertex();

	/* accessors */
	VertexType 			getType() const { return type; }
	int 				getIdInstance() const { return id_instance_file; }
	int 				getRegion() const { return region; }
	const VRP_request&	getRequest(int timeSlot) const;
	const VRP_request&	getRequest() const;

	/* debug */
	bool 	checkConsistency() const;

	/* comm */
	string& toString(bool verbose=false) const;
	// ostream& operator<<(ostream &strm) { return strm << toString(); }


protected:
	friend class VRP_instance;
	friend class VRP_instance_file;
	friend class VRP_request;
	// bool active;
	int region;				// region to which the vertex is associated, in the graph representing the problem instance
	int id_instance_file;	// identification number in the test instance file 
	int nTimeSlots;			// # of timeslots in the instance

	VertexType type;		// type of the vertex

	/* Requests */
	VRP_request* requests_ts;	// one request per time slot (including those with zero probability, and timeslot 0)		
	void setRegion(int reg) { region = reg; }
	void setType(VertexType typ) { type = typ; }

	VRP_request&	getRequest_mutable(int timeSlot);
	VRP_request&	getRequest_mutable();

};













/* VRP_instance    ------------------------------------------------------------------ */
#define MAX_ID	1000000

class VRP_instance {
public:
	struct RequestAttributes {
		const VRP_vertex* vertex = nullptr;
		const VRP_request* request = nullptr;
		int reveal_time = -1;
		int time_slot = -1;
		string& toString(bool verbose=false) const;
	};
protected:
	/* Vertices */
	int nRegions;	// = total number of different regions (including the one associated with the depot) in the graph representing the problem instance; 
	int id_instance_file_to_region[MAX_ID];

	set<VRP_vertex*>		depot_vertices;		// set of vertices associated to a depot vertex, according to the instance
	// set<VRP_vertex*>		u_depot_vertices;	// set of vertices associated to unload depot vertex (where vehicle can unload during the day, if allowed), according to the instance
	set<VRP_vertex*>		reg_vertices;		// set of vertices associated to a regular vertex, according to the instance
	set<VRP_vertex*>		cust_vertices;		// set of vertices associated to a customer region (SS-VRP only), according to the instance; each vertex host a set of potential requests
	set<VRP_vertex*>		wait_vertices;		// set of vertices associated to a waiting vertex, according to the instance
	set<VRP_vertex*> 		reg_online_vertices;// set of subsets: reg_online_vertices[ts] = set of vertices associated to a customer regions (in DS-VRP) at time slot ts, according to the instance; each vertex hosts one potential request

	vector<double> 	coordX;	// stores the coordinates of each region
	vector<double> 	coordY;

	/* Vehicles */
	int _nVeh = -1;    						// number of vehicles (if this is part of the instance)
	// int _Q = -1;     							// vehicles max capacity (if this is part of the instance)
	// double _vehVelocity = 1.0;
	// double vehVelocity = 1.0;			// velocity of the vehicle (number of distance unit per time unit) ---> used during distance matrix pre-computation
	// double costPerDistanceUnit = 1.0;

	/* Vehicle types */
	set<VRP_VehicleType *> vehicle_types;

	/* Time */
	int H;     			// time horizon (= number of time units) [0,1,...,H]
	int nTimeSlots;		// number of time slots (≤H) in the horizon (one request per time slot for each customer region); timeslot 0 corresponds to deterministic requests 	
	double scale;		// scale of the horizon size (if original -i.e. from instance file- horizon size is 480, and here H is 240, then scale is 2)
	int orig_horizon;

	/* Customer potential requests (dynamic/stochastic context), if any */
	int nPotentialRequests = 0;
	vector<RequestAttributes> online_requests_appeared;		// vector containing the sequence of appeared requests, according to the instance; ordered according to the instance (primarily by reveal time, obviously)
	vector<vector<int>> online_requests_appearing_times;	// appearing_times[region][ts] == t iff an online request (at region for time slot ts) appears at to time t (according the the real scenario revealed by the instance)

	VRP_vertex* 	getVertexFromRegion_mutable(VRP_vertex::VertexType type, int region) const; 	// returns a MUTABLE reference pointer to a particular type of vertex, for a given region
	VRP_vertex&		addVertex(int region, VRP_vertex::VertexType type, int instance_id = -42);	// add a new vertex in the instance and returns a reference to it (typically called when parsing instance file); 

	VRP_VehicleType& addVehicleType(string name = string(), int capacity = 0, double velocity = 1.0, double costPerDistanceUnit = 1.0);	// adds new vehicle type to the set vehicle_types

	VRP_vertex&		addOnlineVertex(int region, VRP_vertex::VertexType type, int time_slot, int min_reveal_time, int max_reveal_time, int instance_id = -42);	// for online requests, associates a vertex to a region and one time slot; min and max reveal times define should bound the time slot in the horizon
	void 			addAppearingOnlineRequest(int region, int timeSlot, int revealTime);			// adds an entry to vector online_requests_appeared
	void			setCoordRegion(int region, double x, double y) { coordX[region] = x; coordY[region] = y; }					// sets the coordinates of a particular region of the graph
	void			setCoordRegion(int region, int x, int y) { setCoordRegion(region, (double) x, (double) y); }
	
	
public:
	friend class VRP_instance_file;
	// friend class VRP_request;
	// friend class VRP_vertex;
	// friend class VRP_routes;

	VRP_instance(int nRegions, int H=0, int nTimeSlots=0, int orig_horizon=0, double precision=1);		// 
	

	/* accessors */
	int 			getNumberRegions() const;
	int 			_getNumberVehicles() const;
	// // void 			setNumberVehicles(int n);
	// int 			_getVehicleCapacity() const;
	// // void 			setVehicleCapacity(int capa);
	// int 			_getVehicleVelocity() const;
	int 			getHorizon() const;
	int 			getNumberTimeSlots() const;
	int 			getNumberPotentialRequests() const;
	double 			getScale() const { return scale;}
	// int 			ts2time(int ts) const;																			// converts a timeslot number into a time unit in the horizon
	// int 			getWaitingTimeMultiple() const { return waiting_time_multiple; }

	const VRP_vertex* 		getVertexFromRegion(VRP_vertex::VertexType type, int region) const; 	// returns a reference pointer to a particular type of vertex, for a given region
	const VRP_vertex* 		getVertexFromInstanceFileID(VRP_vertex::VertexType type, int instance_id) const; 	// returns a reference pointer to a particular type of vertex, based on the ID specified in the instance file (VRP_vertex::id_instance_file)

	const set<VRP_VehicleType *>& 	getVehicleTypes() const { return vehicle_types; }
	const VRP_VehicleType& 			getVehicleType(string name) const;

	const set<VRP_vertex *>& getDepotVertices() const { return depot_vertices; }
	// const set<VRP_vertex *>& getUnloadDepotVertices() const { return u_depot_vertices; }
	const set<VRP_vertex *>& getRegularVertices() const { return reg_vertices; }
	const set<VRP_vertex *>& getCustomerVertices() const {return cust_vertices; }
	const set<VRP_vertex *>& getWaitingVertices() const { return wait_vertices; }
	const set<VRP_vertex *>& getOnlineVertices() const { return reg_online_vertices; }
	int 					 getNumberVertices(VRP_vertex::VertexType type) const;	// returns the number of vertices associated to a particular type 


	/* dynamic context */
	/* the term "appeared" means that a potential request has revealed	to be appearing in the real scenario; whether a request is appeared may vary along the online execution, since it depends on the moments at which stochastic events occur */
	/* the term "revealed" means that the stochastic information about a potential request (that is, if it appears or not) is know; again, that property depends on the time at which the question is asked */
	void					set_OnlineRequest_asAppeared(int region, int timeSlot);					// tells the instance that, from now, the potential request must be considered as appeared (in the real scenario)
	void					set_OnlineRequest_asAppeared(const VRP_request& r);						//
	const vector<RequestAttributes> & getAppearingOnlineRequests() const { return online_requests_appeared; }
	bool  					revealedAtTime(const VRP_request& r, int time) const; 					// tells whether, for a given potential request r, and according to the real scenario that realized until current time, the request is revealed or not (if the request associated time slot is past, then returns always true; if r's TS is not started yet, returns always false; otherwise, returns true iff r appeared during the part of the TS until current time)

	void 	setIntegerTravelTimes();	// no matter the vehicle type, the travelTime() function will return integer times (rounded up)

	/* debug */
	bool 	checkConsistency() const;	// performs some basic ckecks related to the data stored (e.g. are all requests such that demand ≤ Q ?)

	/* comm */
	string& toString(bool verbose=false) const;
	string& toStringVehicleTypes(bool verbose=false) const;
	string& toStringCoords(bool and_attributes=false) const;
	string& toStringProbasTS() const;
	string& toStringPotentialRequests() const;
	string& toStringPotentialOnlineRequests() const;
	string& toStringInstanceMetrics() const;
	string& toString_AppearingOnlineRequests(bool verbose=false) const;
	ostream& operator<<(ostream &strm) { return strm << toString(); }
};






class VRP_instance_file {
/* INSTANCE FILES ------------------------------------------------------------------ 

The following functions read a given type of instance file and return a reference to an object VRP_instance, filled with the instance data */
public:
	// Handles instance files of Cordeau-Laporte in benchmarks/cordeau/vrptw/old 	-	static instances 
	static VRP_instance& readInstanceFile_CordeauLaporte_VRPTW_old(const char* filename);

	/* Instance files used for the DS-VRPTW, from Bent & Van Hentenryck */
	static VRP_instance& readInstanceFile_DS_VRPTW__Bent_PVH(const char *filename);
	/* Instance files used for the DS-VRPTW, from Rizzo  */
	static VRP_instance& readInstanceFile_DS_VRPTW__Rizzo(const char *oc_directory, int scenario_number, bool use_lockers);

	// SS-VRPTW-CR instance files in benchmarks/ss-vrptw-cr/paper_conf - given an precision ≥ 1 (e.g. 2 -> means 1 time unit of horizon represents 2 instance real time units), all the temporal data are scaled appropriately
	static VRP_instance& readInstanceFile_SSVRPTW_CR(const char *filename, double precision = 1.0);

	// Instance files used for the SS-VRPTW-CR in benchmarks/ss-vrptw-cr/realdata_optimod_lyon (journal paper, part 1: models)
	// static VRP_instance& readInstanceFile_SSVRPTW_CR_journal_part_1(const char *filename_distances, const char *filename_instance, double scale = 1.0);
	static VRP_instance& readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(const char *filename_distances, const char *filename_instance, int vehicle_capacity, double scale);
};
	

#endif

