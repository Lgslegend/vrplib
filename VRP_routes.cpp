/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_routes +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_routes.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#include <iostream>
#include <vector>
#include <cmath>
#include <limits>
#include "VRP_instance.h"
#include "VRP_routes.h"
#include "tools.h"

using namespace std;

// VRP_routes::VRP_routes(const VRP_instance& instance, const VRP_vertex* depot) : I(instance) {
VRP_routes::VRP_routes(const VRP_instance& instance) : I(instance) {
	/* initialize the pool of node addresses */
	for (int i = 0; i < NODE_POOL_SIZE; i++)
		node_pool_free_addr.push_back(& node_pool[i]);

	/* creating temporary route 0 ; used only for swaping segments */
	nRoutes = 0;
	currentTime = -1.0;
	const VRP_vertex* depot = *I.getDepotVertices().begin();
	addRoute(depot, new VRP_VehicleType(I.getNumberRegions()), true);
}

VRP_routes& VRP_routes::operator = (const VRP_routes& other_routes) {
	for (int n = nRoutes; n < other_routes.nRoutes; n++)
		addRoute(other_routes.routes[n+1].first_node->vertex, other_routes.routes[n+1].vehicle);

	currentTime = other_routes.currentTime;
	waitingStrategy = other_routes.waitingStrategy;

	for (int r=1; r <= nRoutes; r++) {	
		copyRouteFrom(other_routes, r);
	}
	
	return *this;
}
bool VRP_routes::operator == (const VRP_routes& other) const {
	if (nRoutes != other.nRoutes)
		return false;
	for (int r=1; r <= nRoutes; r++) {
		if (getRouteSize(r) != other.getRouteSize(r))
			return false;
		for (int pos=0; pos <= getRouteSize(r); pos++)
			if ((* pos2node(r,pos).vertex) != (* other.pos2node(r,pos).vertex) || pos2node(r,pos).attr != other.pos2node(r,pos).attr)
				return false;
	}
	return true;
}


bool VRP_routes::NodeAttr::operator == (const VRP_routes::NodeAttr& other) const {
	return waiting_time == other.waiting_time && pos == other.pos && veh == other.veh && arrival_time == other.arrival_time 
		&& service_time == other.service_time && departure_time==other.departure_time && fixed == other.fixed && left == other.left;
}

void VRP_routes::copyRouteFrom(const VRP_routes& other_routes, int route) {
	ASSERT(nRoutes == other_routes.nRoutes, "nRoutes=" << nRoutes << "   other_routes.nRoutes=" << other_routes.nRoutes);
	ASSERT(currentTime == other_routes.currentTime, "currentTime=" << currentTime << "   other_routes.currentTime=" << other_routes.currentTime);
	ASSERT(waitingStrategy == other_routes.waitingStrategy, "argh");

	const Route& other_r = other_routes.routes[route];
	ASSERT(other_r.vehicle == routes[route].vehicle, "");	
	clearRoute(route);
	routes[route].vehicle = other_r.vehicle;
	routes[route].unload_depot = other_r.unload_depot;
	routes[route].first_node->vertex = other_r.first_node->vertex;
	routes[route].last_node->vertex = other_r.last_node->vertex;
	currentTime = -1;
	// setAssertLevel(ASSERT_NO_FIXED_LEFT);

	routes[route].first_node->attr = other_r.first_node->attr;	// copy the starting depot attributes

	// routes[route].last_node->attr = other_r.last_node->attr;	// copy the ending depot attributes
	routes[route].last_node->attr.fixed  = false;
	routes[route].last_node->attr.served = false;
	routes[route].last_node->attr.left   = false;

	for (Node* other_node = other_r.first_node->next_node; other_node != other_r.last_node; other_node = other_node->next_node) {		// for each existing node in other_r (excluding the two depot nodes)
		if (other_node->online_request)
			insertVertex(other_node->vertex, route, getRouteSize(route), true, other_node->online_request->revealTS);
		else
			insertVertex(other_node->vertex, route, getRouteSize(route), true);	// insert it in the current route r ; here  copyingSol=true 
		
		pos2node(route, getRouteSize(route)-1).attr = other_node->attr;		// copy the attributes in the node just inserted
	}

	setAssertLevel(ASSERT_ALL);
	routes[route].last_node->attr = other_r.last_node->attr;	// copy the ending depot attributes

	currentTime = other_routes.currentTime;
	updateRouteInfos(route, false, true);		//	(int route, bool routeZero, bool newCurrentTime) 
}


void VRP_routes::removeAllRoutes() {
	for (int r = 1; r <= nRoutes; r++) {
		clearRoute(r);		// we assume that all the ASSERTs are done by clearRoute()
		pushNodePtrBackToPool(routes[r].first_node);
		pushNodePtrBackToPool(routes[r].last_node);
		routes[r].first_node = NULL;
		routes[r].last_node = NULL;
	}
	nRoutes = 0;
}

void VRP_routes::clearRoute(int route) {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	if (routes[route].nNodes == 2) return;

	// first put back every non-depot node in the route back in the node pool
	for (Node* node = routes[route].first_node->next_node; node != routes[route].last_node; node = node->next_node) {
		// ASSERT(vpointer2vnode.count(node->vertex) > 0, "key not found !");
		// vpointer2vnode.erase(node->vertex);
		pushNodePtrBackToPool(node);
	}

	// then connect together both starting and ending depots
	routes[route].first_node->next_node = routes[route].last_node;
	routes[route].last_node->prev_node = routes[route].first_node;

	updateRouteInfos(route);
}




VRP_routes::~VRP_routes() {
	
}

VRP_routes::Node* VRP_routes::getNodePtrFromPool() {
	ASSERT(node_pool_free_addr.size() > 0, "");

	VRP_routes::Node* ptr = node_pool_free_addr.back(); node_pool_free_addr.pop_back();

	return ptr;
}

void VRP_routes::pushNodePtrBackToPool(Node* node_ptr) {
	ASSERT(node_pool_free_addr.size() < NODE_POOL_SIZE, "");
	node_pool_free_addr.push_back(node_ptr);
}

VRP_routes::Node& VRP_routes::pos2node(int route, int pos, bool routeZero) const { 
	checkParamRoute(route, routeZero, true);		// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, routeZero, false, false); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	
	return *(routes[route].pos2node[pos]); 
}


void VRP_routes::checkParamRoute(int route, int routeZero, bool enforceRouteZero) const {
	#ifdef ASSERTS

	ASSERT(route >= 1-routeZero || route == -42, "route " << route << " not allowed"); 	// -42 means 'all routes'
	ASSERT(route <= nRoutes, "route " << route << " does not exist");
	if (enforceRouteZero)
		ASSERT(routeZero==false || route==0, "routeZero=" << routeZero << "   route=" << route);	// routeZero==true --> route==0
	#else
	UNUSED(route); UNUSED(routeZero); UNUSED(enforceRouteZero);
	#endif
}
void VRP_routes::checkParamSize(int size) const {
	#ifdef ASSERTS
	
	ASSERT(size >= 0, "");
	#else
	UNUSED(size);
	#endif
}
void VRP_routes::checkParamPosition(int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed, bool UNLEFTonly) const {
	#ifdef ASSERTS
	
	ASSERT(pos >= 0, "");
	ASSERT(pos <= getRouteSize(route, routeZero) - exclude_depot, endl << toString(true));	
	if (exclude_fixed && assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) {
		auto node = pos2node(route, pos, routeZero);
		_ASSERT_(false == pos2node(route, pos, routeZero).attr.fixed, "route=" << route << "  pos=" << pos << "   node: " << node.vertex->toString() << "  " << node.attr.veh << endl << toString(true) << endl);
	}
	if (UNLEFTonly && assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) {
		auto node = pos2node(route, pos, routeZero);
		ASSERT(!node.attr.left, "route=" << route << "  pos=" << pos << "   node: " << node.vertex->toString() << endl << toString(true) << endl);
	}
	#else
	UNUSED(route); UNUSED(pos); UNUSED(routeZero); UNUSED(exclude_depot); UNUSED(exclude_fixed); UNUSED(UNLEFTonly);
	#endif
}


VRP_routes::AssertLevel VRP_routes::getAssertLevel() {
	return assertLevel;
}
void VRP_routes::setAssertLevel(VRP_routes::AssertLevel level) {
	// ASSERT(0 <= level && level <= )
	assertLevel = level;
}


WaitingStrategy VRP_routes::getWaitingStrategy() {
	return waitingStrategy;
}
void VRP_routes::setWaitingStrategy(WaitingStrategy ws) {
	waitingStrategy = ws;
	updateRouteInfos();
}

int VRP_routes::getNumberFreeRoutes(int route) const {
	if (route != -42) {
		checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
		return routes[route].last_node->attr.fixed == false;
	}

	int n = 0;
	for(int k=1; k <= nRoutes; k++) 
		n += routes[k].last_node->attr.fixed == false;	// if the ending depot is NOT fixed
	return n;
}

int VRP_routes::getNumberModifiableRoutes(int route) const {
	if (route != -42) {
		return (getNumberMoveableVertices(route) >= 1);
		checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	}

	int n = 0;
	for(int k=1; k <= nRoutes; k++) 
		n += (getNumberMoveableVertices(k) >= 1);
	return n;
}

int VRP_routes::getNumberRoutes() const {
	return nRoutes;
}
int VRP_routes::getNumberNonEmptyRoutes(int route) const {
	if (route != -42) {
		checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
		return (getRouteSize(route) > 1);
	}

	int n = 0;
	for(int k=1; k <= nRoutes; k++) 
		if (getRouteSize(k) > 1)	// if the route contains more than only the ending depot
			n++;
	return n;
}

int VRP_routes::getFirstFreePos(int route) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	return routes[route].firstFreeNodePos;
}
int VRP_routes::getNumberFreeVertices(int route) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	if (route != -42)
		return routes[route].nFreeVertices;
	else return nFreeVertices;
}
int VRP_routes::getNumberMoveableVertices(int route) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	if (route != -42) 
		return routes[route].nMoveableVertices;
	else return nMoveableVertices;
}
int VRP_routes::getNumberFreeWaitingVertices(int route) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	if (route != -42) 
		return routes[route].nFreeWaitingVertices;
	else return nFreeWaitingVertices;
}
int VRP_routes::getNumberUNLEFTWaitingVertices(int route) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	if (route != -42) 
		return routes[route].nUNLEFTWaitingVertices;
	else return nUNLEFTWaitingVertices;
}

const VRP_VehicleType& VRP_routes::getVehicleTypeFromRoute(int route) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	return * routes[route].vehicle;
}

const VRP_vertex&	VRP_routes::getVertexFromPos(int route, int pos) const { 
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, false, false, false);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	return * pos2node(route, pos).vertex; 
}

const VRP_request&	VRP_routes::getRequestFromPos(int route, int pos) const { 
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, false, false, false);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	
	if (pos2node(route, pos).vertex->getType() == VRP_vertex::REGULAR_ONLINE)
		return * pos2node(route, pos).online_request;

	return pos2node(route, pos).vertex->getRequest(); 
}


int VRP_routes::getRouteSize(int route) const { 
	return getRouteSize(route, false);
}
int VRP_routes::getRouteSize(int route, bool routeZero) const { 
	checkParamRoute(route, routeZero, false);	// (int route, int routeZero, bool enforceRouteZero)
	return routes[route].nNodes -1; 
}	

void VRP_routes::addRoute(const VRP_vertex* depot, const VRP_VehicleType* vehicle) {
	addRoute(depot, vehicle, false);
}
void VRP_routes::addRoute(const VRP_vertex* depot, const VRP_VehicleType* vehicle, bool init) {
	ASSERT(depot->getType() == VRP_vertex::DEPOT, "type=" << depot->getType());
	ASSERT(nRoutes+1 < MAX_ROUTES, "");

	if (!init)
		nRoutes++;
	Route& route = routes[nRoutes];
	ASSERT(route.first_node == NULL && route.last_node == NULL, "");

	route.vehicle = vehicle;
	route.first_node = getNodePtrFromPool(); 
	route.last_node = getNodePtrFromPool(); 

	// initializing first_node
	route.first_node->vertex 			= depot;
	route.first_node->next_node 		= route.last_node;
	route.first_node->prev_node 		= NULL;
	route.pos2node[0] 					= route.first_node;
	route.first_node->attr.pos 			= 0;
	route.first_node->attr.veh 			= nRoutes;
	route.first_node->attr.waiting_time = 0.0;
	route.first_node->attr.fixed 		= false;
	route.first_node->attr.left 		= false;
	route.first_node->attr.served 		= false;

	route.first_node->attr.arrival_time = 1.0;
	route.first_node->attr.service_time = 1.0;

	// initializing last_node
	route.last_node->vertex 			= depot;
	route.last_node->next_node 			= NULL;
	route.last_node->prev_node 			= route.first_node;
	route.pos2node[1] 					= route.last_node;
	route.last_node->attr.pos 			= 1;
	route.last_node->attr.veh 			= nRoutes;
	route.last_node->attr.waiting_time 	= 0.0;
	route.last_node->attr.fixed 		= false;
	route.last_node->attr.left 			= false;
	route.last_node->attr.served 		= false;

	updateRouteInfos(nRoutes, init);
}


// void VRP_routes::activateAutomaticVehicleUnload(int route, const VRP_vertex& depot) {
// 	_ASSERT_(!routes[route].unload_depot, "already activated !");
// 	_ASSERT_(depot.getType() == VRP_vertex::UNLOAD_DEPOT, "");
// 	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
// 	routes[route].unload_depot = &depot;

// 	updateRouteInfos(route);
// }
void VRP_routes::activateAutomaticVehicleUnload(int route) {
	_ASSERT_(!routes[route].unload_depot, "already activated !");
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	routes[route].unload_depot = true;

	updateRouteInfos(route);
}


void VRP_routes::insertVertexLastPos(const VRP_vertex* vertex, int to_route) { 
	insertVertex(vertex, to_route, getRouteSize(to_route)); 
}

void VRP_routes::insertVertex(const VRP_vertex* vertex, int to_route, int to_pos) {
	insertVertex(vertex, to_route, to_pos, false);
}
void VRP_routes::insertVertex(const VRP_vertex* vertex, int to_route, int to_pos, bool copyingSol, int timeslot) {
	checkParamRoute(to_route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(to_route, to_pos, false, false, true);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	ASSERT(routes[to_route].nNodes < MAX_NODES_PER_ROUTE, "");

	Node& before_node = pos2node(to_route, to_pos);		// our new Node will be inserted *before* the so-called before_node

	// creating the new node to be inserted, and initialize some of the attributes (the remaining will be set by updateRouteInfos() call )
	Node& node = * getNodePtrFromPool();
	node.vertex = vertex;
	node.attr.waiting_time = 0.0;

	if (vertex->getType() == VRP_vertex::REGULAR_ONLINE) {
		ASSERT(timeslot >= 0 && timeslot <= I.getNumberTimeSlots(), timeslot);
		node.online_request = & vertex->getRequest(timeslot);
	} 
	else
		node.online_request = nullptr;

	node.prev_node = before_node.prev_node;
	before_node.prev_node->next_node = &node;
	before_node.prev_node = &node;
	node.next_node = &before_node;

	node.attr.fixed = false;
	node.attr.left = false;
	node.attr.served = false;

	// // ASSERT(vpointer2vnode.count(vertex) == 0, "key exists !");
	// vpointer2vnode.insert({vertex, &node});

	updateRouteInfos(to_route, false, copyingSol);
}

void VRP_routes::insertOnlineVertex(const VRP_vertex* vertex, int to_route, int to_pos, int timeslot) {
	ASSERT(vertex->getType() == VRP_vertex::REGULAR_ONLINE, "argh");
	insertVertex(vertex, to_route, to_pos, false, timeslot);
}

void VRP_routes::removeVertex(int route, int pos, bool checkFixed) {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, false, true, checkFixed); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	Node& node = pos2node(route, pos);

	node.prev_node->next_node = node.next_node;
	node.next_node->prev_node = node.prev_node;

	// ASSERT(vpointer2vnode.count(node.vertex) > 0, "key not found !");
	// vpointer2vnode.erase(node.vertex);
	pushNodePtrBackToPool(&node);

	updateRouteInfos(route);
}

// void VRP_routes::removeUnloadDepotNode(Node& node) {
// 	ASSERT(node.attr.fixed == false, "");
// 	ASSERT(node.vertex->getType() == VRP_vertex::UNLOAD_DEPOT, "");

// 	node.prev_node->next_node = node.next_node;
// 	node.next_node->prev_node = node.prev_node;

// 	pushNodePtrBackToPool(&node);
// }

// void VRP_routes::insertUnloadDepotNode_after(const VRP_vertex* udepot, Node& node) {
// 	ASSERT(node.attr.left == false, "");
// 	ASSERT(udepot->getType() == VRP_vertex::UNLOAD_DEPOT, "");

// 	// creating the new node to be inserted, and initialize some of the attributes (the remaining will be set by updateRouteInfos() call )
// 	Node& new_node = * getNodePtrFromPool();
// 	new_node.vertex = udepot;
// 	new_node.attr.waiting_time = 0.0;

// 	new_node.online_request = nullptr;

// 	new_node.prev_node = &node;
// 	new_node.next_node = node.next_node;
// 	node.next_node = &new_node;
// 	new_node.next_node->prev_node = &new_node;

// 	new_node.attr.fixed = false;
// 	new_node.attr.left = false;
// 	new_node.attr.served = false;
// }



void VRP_routes::swapSegments(int route_1, int pos_1_1, int pos_1_2, int route_2, int pos_2_1, int pos_2_2) {
	checkParamRoute(route_1, false, false);		// (int route, int routeZero, bool enforceRouteZero)
	checkParamRoute(route_2, false, false);		// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route_1, pos_1_1, false, true, true); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(route_1, pos_1_2, false, true, true); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(route_2, pos_2_1, false, true, true); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(route_2, pos_2_2, false, true, true); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	ASSERT(getRouteSize(0, true) == 1, "Route 0 is not empty !");
	if (route_1 == route_2) {
		if (pos_1_1 <= pos_2_1)
			ASSERT(pos_1_2 < pos_2_1, "the two segments in the same route cannot overlap !");
		else ASSERT(pos_2_2 < pos_1_1, "the two segments in the same route cannot overlap !");
	}
	int length_seg1 = pos_1_2 - pos_1_1 + 1;
	bool seg1_before_seg2 = pos_1_1 < pos_2_1;

	if (route_1 == route_2) {
		if (seg1_before_seg2 && pos_1_2 == pos_2_1-1) {
			moveSegment(route_1, pos_2_1, pos_2_2, route_1, pos_1_1, false, true);
			return;
		}
		if (!seg1_before_seg2 && pos_2_2 == pos_1_1-1) {
			moveSegment(route_1, pos_1_1, pos_1_2, route_1, pos_2_1, false, true);
			return;
		}
	}

	moveSegment(route_1, pos_1_1, pos_1_2, 0, 1, true, true);		// move first segment into temporary route 0

	if (route_1 == route_2 && seg1_before_seg2)				// if same route and segment 1 is before segment 2
		pos_2_1 -= length_seg1, pos_2_2 -= length_seg1;			// then now we removed segment 1 we must update pos_2_1 and pos_2_2

	moveSegment(route_2, pos_2_1, pos_2_2, route_1, pos_1_1, false, true);		// move second segment at the last position of segment 1
	
	int to_pos;
	if (route_1 == route_2 && seg1_before_seg2)
		to_pos = pos_2_2 +1;
	else to_pos = pos_2_1;

	moveSegment(0, 1, length_seg1, route_2, to_pos, true, true);		// move first segment at the last position of segment 2
}

// public interface for moveSegment
void VRP_routes::moveSegment(int from_route, int from_pos_1, int from_pos_2, int to_route, int to_pos) {
	moveSegment(from_route, from_pos_1, from_pos_2, to_route, to_pos, false, true);
}
void VRP_routes::moveSegment(int from_route, int from_pos_1, int from_pos_2, int to_route, int to_pos, bool routeZero, bool updateRoutes) {
	checkParamRoute(from_route, routeZero, false);	// (int route, int routeZero, bool enforceRouteZero)
	checkParamRoute(to_route, routeZero, false);	// (int route, int routeZero, bool enforceRouteZero)
	ASSERT(!(routeZero == true) || (from_route == 0 || to_route == 0), "");	// routeZero == true --> from_route == 0 OR to_route == 0

	checkParamPosition(from_route, from_pos_1, from_route==0, true, from_route!=0); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(from_route, from_pos_2, from_route==0, true, from_route!=0); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(to_route, to_pos, to_route==0, false, to_route!=0); 		// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	ASSERT(from_pos_1 <= from_pos_2, "");
	
	ASSERT(from_route != to_route || false == (from_pos_1 < to_pos && to_pos <= from_pos_2), "");	// from_route == to_route ---> {to_pos cannot be comprised in ]from_pos_1, from_pos_2] } !!

	ASSERT(routes[to_route].nNodes + (from_pos_2+1 - from_pos_1) < MAX_NODES_PER_ROUTE, "");
	

	Node& from_first_node 	= pos2node(from_route, from_pos_1, from_route==0);
	Node& from_last_node 	= pos2node(from_route, from_pos_2, from_route==0);
	ASSERT(from_last_node.vertex->getType() != VRP_vertex::DEPOT, "trying to move the depot !");
	Node& before_node 		= pos2node(to_route, to_pos, to_route==0);

	if (from_route==to_route && (to_pos == from_pos_1 || to_pos == from_pos_2+1))	// then nothing to do !
		return;

	// removing sequence of nodes from its current position
	from_last_node.next_node->prev_node = from_first_node.prev_node;
	from_first_node.prev_node->next_node = from_last_node.next_node;


	// inserting it at new position
	from_first_node.prev_node = before_node.prev_node;
	before_node.prev_node->next_node = &from_first_node;
	before_node.prev_node = &from_last_node;
	from_last_node.next_node = &before_node;


	// update the route attributes (and also attributes of the underlying nodes)
	if(updateRoutes) {
		updateRouteInfos(from_route, from_route==0); 
		if (from_route != to_route) 
			updateRouteInfos(to_route, to_route==0);
	}
}

void VRP_routes::moveVertex(int from_route, int from_pos, int to_route, int to_pos) {
	moveSegment(from_route, from_pos, from_pos, to_route, to_pos);
}

void VRP_routes::swapVertices(int route_1, int pos_1, int route_2, int pos_2) {
	swapSegments(route_1, pos_1, pos_1, route_2, pos_2, pos_2);
}


/* inverts a vertex sequence begginning at 'first' and ending at 'last'
	e.g.  D-->A-->B-->C-->D-->E-->D, invertSegment(B,D) produces: D-->A-->D-->C-->B-->E-->D
*/
void VRP_routes::invertSegment(int route, int pos_1, int pos_2) {
	checkParamRoute(route, false, false);		// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos_1, false, true, true); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(route, pos_2, false, true, true); 	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	if (pos_1 == pos_2)
		return;
	ASSERT(pos_1 < pos_2, "");

	Node& node_1 = pos2node(route, pos_1);
	Node& node_2 = pos2node(route, pos_2);

	Node* prev_node_1 = node_1.prev_node;
	Node* next_node_2 = node_2.next_node;

	for (Node* node = &node_1; node != &node_2; node = node->next_node) 
		node->prev_node = node->next_node;
	node_2.prev_node = prev_node_1;

	node_1.next_node = next_node_2;	
	Node* node_ = &node_1;
	for (Node* node = node_1.prev_node; node != prev_node_1; node = node->prev_node)
		node->next_node = node_, node_ = node;


	prev_node_1->next_node = &node_2;
	next_node_2->prev_node = &node_1;

	updateRouteInfos(route);
}

void VRP_routes::setWaitingTime(int route, int pos, double w) {
	ASSERT(w >= 0, "");
	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, false, true, false);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)

	Node& node = pos2node(route, pos);
	// ASSERT(node.vertex->getType() == VRP_vertex::WAITING, "argh");

	// if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) {
		if (w > node.attr.waiting_time)	// if INCREASING
			ASSERT(!node.attr.left, "trying to increase the waiting time of a left node !"<< endl << toString(true) << endl);
		// if (w < node.attr.waiting_time)	// if DECREASING
		// 	ASSERT(!node.attr.fixed, "trying to decrease the waiting time of a fixed node !"<< endl << toString(true) << endl);
		if (w < node.attr.waiting_time && node.attr.fixed) {	// if DECREASING the waiting time of a FIXED node
			ASSERT(!node.attr.left, "trying to decrease the waiting time of a left node !"<< endl << toString(true) << endl);
			if (node.attr.served)
				ASSERT( max(node.attr.arrival_time, node.vertex->getRequest().e) + routes[route].vehicle->totalServiceTime(node.vertex->getRequest()) + w >= max(currentTime, node.attr.service_time + routes[route].vehicle->totalServiceTime(node.vertex->getRequest())), "arrival_time: " << node.attr.arrival_time << "   e:" << node.vertex->getRequest().e << "  w: " << w << "  currentTime: " << currentTime << "  node.attr.service_time: " << node.attr.service_time << "   routes[route].vehicle->totalServiceTime(node.vertex->getRequest()): " << routes[route].vehicle->totalServiceTime(node.vertex->getRequest()) << endl << toString(true));
		}
	// }

	node.attr.waiting_time = w;

	updateRouteInfos(route);
}
double VRP_routes::getWaitingTime(int route, int pos) const {
	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, false, true, false);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)

	Node& node = pos2node(route, pos);
	return node.attr.waiting_time;
}

double	VRP_routes::removeMaxWaitingTime(int route, int pos) {
	Node& node = pos2node(route, pos);
	ASSERT(node.vertex->getType() == VRP_vertex::WAITING, "argh");
	ASSERT(!node.attr.left, "");
	_ASSERT_(waitingStrategy == DRIVE_FIRST, "tested only with DRIVE_FIRST waiting strategy !");

	if (node.attr.waiting_time == 0)
		return 0;

	if (node.attr.service_time + routes[route].vehicle->totalServiceTime(node.vertex->getRequest()) + node.attr.waiting_time < currentTime)
		return node.attr.waiting_time;

	double removed_wtime;
	double time_when_finished_to_park = node.attr.service_time + routes[route].vehicle->totalServiceTime(node.vertex->getRequest());

	if (time_when_finished_to_park + node.attr.waiting_time + 0.00001 < node.attr.departure_time)
		removed_wtime = time_when_finished_to_park + node.attr.waiting_time - max(currentTime, time_when_finished_to_park);
	else
		removed_wtime = node.attr.departure_time - max(currentTime, time_when_finished_to_park);

	ASSERT(removed_wtime >= 0, "removed_wtime = " << removed_wtime << endl << routes[route].vehicle->totalServiceTime(node.vertex->getRequest()) << endl << toString(true));

	// if (removed_wtime > 0)
	// 	cout << "removing " << removed_wtime << " waiting time units to " << node.vertex->toString() << endl;

	double orig_wtime = node.attr.waiting_time;
	if (removed_wtime > 0)
		setWaitingTime(route, pos, node.attr.waiting_time - removed_wtime + 0.00001);
	return orig_wtime;
}


// Efficiency of updateRouteInfos can be improved:
// + iterate only from the first non-fixed node of a route
// + or even from the first modified node (implies to keep track of it)
void VRP_routes::updateRouteInfos(int route, bool routeZero, bool newCurrentTime) {
	checkParamRoute(route, routeZero, true);				// (int route, int routeZero, bool enforceRouteZero)

	ASSERT(waitingStrategy == DRIVE_FIRST || waitingStrategy == DRIVE_FIRST_DELAY_LAST || waitingStrategy == WAIT_FIRST || waitingStrategy == RELOCATION_ONLY, "the selected waiting strategy is not not implemented yet (" << waitingStrategy << ")");
	ASSERT(waitingStrategy == DRIVE_FIRST || I.getHorizon() < numeric_limits<int>::max(), "if waitingStrategy different than DRIVE_FIRST, then time horizon must be set");

	for (int k = 0; k <= nRoutes; k++) {	

		if (route != -42 && k != route) continue;


		// /* remove all the trips to the unload depot (if applicable) */
		// if (k >= 1 && routes[k].unload_depot) {
		// 	int load = 0;
		// 	// for (Node* node_ = depot_start; node_->next_node != routes[k].last_node; node_ = node_->next_node) {
		// 	for (Node* node_ = routes[k].pos2node[routes[k].firstUNLEFTpos]; node_->next_node != routes[k].last_node && node_->next_node; node_ = node_->next_node) {
		// 		if (node_->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
		// 			load += node_->online_request->demand;
		// 		else 
		// 			load += node_->vertex->getRequest().demand;

		// 		int next_demand;
		// 		if (node_->next_node->vertex->getType() == VRP_vertex::UNLOAD_DEPOT) {		// if next node is a unloading intermediate depot

		// 			if (node_->next_node->next_node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
		// 				next_demand = node_->next_node->next_node->online_request->demand;
		// 			else 
		// 				next_demand = node_->next_node->next_node->vertex->getRequest().demand;

		// 			if (load + next_demand <= routes[k].vehicle->getCapacity())		// remove it only if useless
		// 				removeUnloadDepotNode(*node_->next_node);
		// 			else 												// if kept, then the vehicle unloads there
		// 				load = 0;
		// 		} 
		// 		else {

		// 			if (node_->next_node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
		// 				next_demand = node_->next_node->online_request->demand;
		// 			else 
		// 				next_demand = node_->next_node->vertex->getRequest().demand;

		// 			if (load + next_demand > routes[k].vehicle->getCapacity()) {
		// 				insertUnloadDepotNode_after(routes[k].unload_depot, *node_);
		// 				load = 0;
		// 			}
		// 		}
		// 	}
		// }

		routes[k].routeLoad				= 0;
		routes[k].routeLength			= 0.0;
		routes[k].firstFreeNodePos		= -1;
		// routes[k].firstUNLEFTpos		= -1;
		routes[k].firstFreeWaitingPos 	= -1;
		routes[k].firstUNLEFTWaitingPos = -1;

		routes[k].nFreeVertices 		= 0;
		routes[k].nMoveableVertices 	= 0;
		routes[k].nFreeWaitingVertices	= 0;
		routes[k].nUNLEFTWaitingVertices= 0;

		Node& depot_start = *routes[k].first_node;

		// routes[k].pos2node[0] 			= &depot_start;
		// depot_start.attr.pos 			= 0;
		// depot_start.attr.veh 			= k;
		if (!newCurrentTime) {
			// depot_start.attr.arrival_time 	= 1.0;
			// depot_start.attr.service_time 	= 1.0;
			if (! depot_start.attr.left)
				depot_start.attr.departure_time	= max(currentTime + 0.0001, 1.0 + depot_start.attr.waiting_time );		// waiting time can be modified whenever one wants ????!!
		}
		depot_start.attr.fixed 			= true;
		ASSERT(depot_start.attr.waiting_time == 0, "waiting time only allowed at WAITING vertices");

		if (k >= 1 && ! depot_start.attr.left) {
			if (!newCurrentTime && (waitingStrategy == DRIVE_FIRST_DELAY_LAST || waitingStrategy == WAIT_FIRST || waitingStrategy == RELOCATION_ONLY) ) {
				if (depot_start.next_node == routes[k].last_node){	// if the route is empty
					double endtime = min((double)I.getHorizon()+1, routes[k].last_node->vertex->getRequest().l);
					depot_start.attr.departure_time = max(depot_start.attr.departure_time, endtime - routes[k].vehicle->travelTime(*(depot_start.vertex), *routes[k].last_node->vertex) );
				}
			}
			depot_start.attr.left = depot_start.attr.departure_time <= currentTime;
			if (routes[k].firstUNLEFTpos == -1)
				routes[k].firstUNLEFTpos = 0;
			if (! depot_start.attr.served)
				depot_start.attr.served = depot_start.attr.service_time <= currentTime;
		}


		int pos = 1;
		// int pos = max(1,routes[k].firstUNLEFTpos);
		for (Node* node_ = depot_start.next_node; node_ != NULL; node_ = node_->next_node) {
		// ASSERT(routes[k].pos2node[pos], "");
		// for (Node* node_ = routes[k].pos2node[pos]; node_ != NULL; node_ = node_->next_node) {
			Node& node = *node_;

			if (node.prev_node->attr.left)
				routes[k].firstUNLEFTpos = -1;

			ASSERT(node.vertex->getType() != VRP_vertex::CUSTOMER && node.vertex->getType() != VRP_vertex::NO_TYPE, "");
			ASSERT(node.vertex->getType() == VRP_vertex::WAITING || node.attr.waiting_time == 0, "");	// type != WAITING  ==> waiting_time == 0
			ASSERT(node.vertex->getType() == VRP_vertex::REGULAR_ONLINE || node.online_request == nullptr, "argh");


			const VRP_request* node_req;
			if (node.vertex->getType() == VRP_vertex::REGULAR_ONLINE)
				node_req = node.online_request;
			else 
				node_req = &node.vertex->getRequest();

			int totalServiceTime = routes[k].vehicle->totalServiceTime(*node_req);
			// ASSERT(totalServiceTime >= 0, "route:" << route << "   pos:" << node.attr.pos << "   totalServiceTime=" << totalServiceTime);

			/* route attributes */
			routes[k].pos2node[pos] = &node;
			if (k >= 1) {
				ASSERT(node.vertex != NULL, "");
				ASSERT(node.prev_node != NULL, "");

				node.attr.pos = pos;
				node.attr.veh = k;
				node.attr.current_load = node.prev_node->attr.current_load + node_req->demand;
				if (node.attr.current_load > routes[k].vehicle->getCapacity())
					node.attr.current_load = 0;

				routes[k].routeLoad += node_req->demand;
				routes[k].routeLength += routes[k].vehicle->distance(*(node.prev_node->vertex), *(node.vertex));


				if (!node.attr.left) {				// Do nothing if the vehicle already left the node				
					if (!node.attr.fixed) {
						node.attr.fixed = node.prev_node->attr.departure_time <= currentTime; 

						if (!node.attr.fixed) {
							routes[k].nFreeVertices ++;
							if (node.vertex->getType() != VRP_vertex::DEPOT) 
								routes[k].nMoveableVertices ++;
							if (routes[k].firstFreeNodePos == -1)
								routes[k].firstFreeNodePos = pos;
							if (node.vertex->getType() == VRP_vertex::WAITING) {
								routes[k].nFreeWaitingVertices ++;
								if (routes[k].firstFreeWaitingPos == -1)
									routes[k].firstFreeWaitingPos = pos;
							}	
						}
						if (!newCurrentTime) {	// ARRIVAL TIME
							// node.attr.arrival_time = node.prev_node->attr.departure_time + routes[k].vehicle->travelTime(*(node.prev_node->vertex), *(node.vertex));
							node.attr.arrival_time = node.prev_node->attr.departure_time + travelTimeToNextNodeFrom(*node.prev_node);
						}
					}
					if (!node.attr.served) {

						if (!newCurrentTime) {	// SERVICE TIME
							node.attr.service_time = max((double) node_req->e, node.attr.arrival_time);
						}
						node.attr.served = node.attr.service_time <= currentTime;	

					}

					if (!newCurrentTime) {		// DEPARTURE TIME
						// ASSERT(node.vertex->getType() == VRP_vertex::WAITING || node.vertex->getType() == VRP_vertex::REGULAR || node.attr.waiting_time == 0, "");	// type != WAITING or REGULAR ==> waiting_time == 0
						ASSERT(node.vertex->getType() == VRP_vertex::WAITING || node.attr.waiting_time == 0, "");	// type != WAITING  ==> waiting_time == 0
						node.attr.departure_time = node.attr.service_time + totalServiceTime + node.attr.waiting_time;

						if (waitingStrategy == DRIVE_FIRST_DELAY_LAST)
							if (node.next_node == routes[k].last_node) {	// if it is the ndoe before the last one (that is, if node preceeds the ending depot)
								double endtime = min((double)I.getHorizon()+1, routes[k].last_node->vertex->getRequest().l);
								node.attr.departure_time = max(node.attr.departure_time, endtime - routes[k].vehicle->travelTime(*(node.vertex), *routes[k].last_node->vertex) );
							}
						node.attr.departure_time = max(node.attr.departure_time, currentTime + 0.0001);
					}
					node.attr.left = node.attr.departure_time <= currentTime;
					

					if (!node.attr.left && node.vertex->getType() == VRP_vertex::WAITING) {
						routes[k].nUNLEFTWaitingVertices ++;
						if (routes[k].firstUNLEFTWaitingPos == -1)
							routes[k].firstUNLEFTWaitingPos = pos;
					}
					if (routes[k].firstUNLEFTpos == -1)
							routes[k].firstUNLEFTpos = pos;
					
				}

				if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) {
					ASSERT(!node.attr.left || (node.attr.served && node.prev_node->attr.left), endl <<  toString(true) << endl);	// LEFT ==> SERVED and previous is LEFT
					ASSERT(!node.attr.served || (node.attr.fixed && node.prev_node->attr.left), endl <<  toString(true) << endl);		// SERVED ==> FIXED and previous is LEFT
				}
				if (!newCurrentTime)
					ASSERT(node.attr.arrival_time >= node.prev_node->attr.departure_time, "vertex: " << node.vertex->toString() );
			}
			pos++;
		}
		routes[k].nNodes = pos; 	// = total number of nodes in the route, including both starting and ending depots !
	}

	nFreeVertices = 0;
	nMoveableVertices = 0;
	nFreeWaitingVertices = 0;
	nUNLEFTWaitingVertices = 0;
	for (int k = 1; k <= nRoutes; k++) {
		nFreeVertices += routes[k].nFreeVertices;
		nMoveableVertices += routes[k].nMoveableVertices;
		nFreeWaitingVertices += routes[k].nFreeWaitingVertices;
		nUNLEFTWaitingVertices += routes[k].nUNLEFTWaitingVertices;
	}

	if ( !newCurrentTime && (waitingStrategy == WAIT_FIRST || waitingStrategy == RELOCATION_ONLY)) {
		for (int k = 1; k <= nRoutes; k++) {
			if (route != -42 && k != route) continue;
			if (routes[k].last_node->attr.fixed) continue;

			// last node (depot)
			routes[k].last_node->attr.arrival_time = max(routes[k].last_node->attr.arrival_time, min((double)I.getHorizon()+1, routes[k].last_node->vertex->getRequest().l));
			routes[k].last_node->attr.service_time = routes[k].last_node->attr.arrival_time;
			routes[k].last_node->attr.departure_time = routes[k].last_node->attr.arrival_time;
			
			// previous nodes 
			for (Node* node = routes[k].last_node->prev_node; node != NULL; node = node->prev_node) {
				if (node->attr.left) break;
				
				const VRP_request * req;
				if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)	
					req = node->online_request;
				else
					req = & node->vertex->getRequest();

				// node->attr.departure_time = max(node->attr.departure_time, node->next_node->attr.arrival_time - routes[k].vehicle->travelTime(*node->vertex, *node->next_node->vertex));
				node->attr.departure_time = max(node->attr.departure_time, node->next_node->attr.arrival_time - travelTimeToNextNodeFrom(*node) );
				
				if (node->prev_node && !node->attr.served && !node->prev_node->attr.left)
					node->attr.service_time = max(node->attr.service_time, min(req->l, node->attr.departure_time - 	routes[k].vehicle->totalServiceTime(*req) - node->attr.waiting_time));

				if (!node->attr.fixed)
					node->attr.arrival_time = node->attr.service_time;
				
				if (node->prev_node)
					ASSERT(node->attr.arrival_time >= node->prev_node->attr.departure_time, "route " << route << "  vertex: " << node->vertex->toString() << endl << toString(true) );
			}


			if (waitingStrategy == RELOCATION_ONLY) {
				// for (Node* node_ = depot_start.next_node; node_ != NULL; node_ = node_->next_node) {
				ASSERT(routes[k].firstUNLEFTpos >= 0, "");
				for (Node* node = routes[k].pos2node[routes[k].firstUNLEFTpos]; node->next_node != NULL; node = node->next_node) {		// exclude the last depot
					// ASSERT(node->next_node != routes[k].last_node, "");

					if (node->vertex->getType() != VRP_vertex::WAITING && node->next_node != routes[k].last_node) {

						node->attr.departure_time = node->attr.service_time + node->attr.waiting_time;
						if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
							node->attr.departure_time += routes[k].vehicle->totalServiceTime(*node->online_request);
						else 
							node->attr.departure_time += routes[k].vehicle->totalServiceTime(node->vertex->getRequest());

						if (!node->next_node->attr.fixed)
							// node->next_node->attr.arrival_time = node->attr.departure_time + routes[k].vehicle->travelTime(* node->vertex, * node->next_node->vertex);
							node->next_node->attr.arrival_time = node->attr.departure_time + travelTimeToNextNodeFrom(*node);

						if (!node->next_node->attr.served) {
							if (node->next_node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
								node->next_node->attr.service_time = max((double) node->next_node->online_request->e, node->next_node->attr.arrival_time);
							else
								node->next_node->attr.service_time = max((double) node->next_node->vertex->getRequest().e, node->next_node->attr.arrival_time);
						}
					}
				}

			}

		}
		
	}
}

double VRP_routes::travelTimeToNextNodeFrom(const Node& node) const {
	const VRP_VehicleType& veh_type = * routes[node.attr.veh].vehicle;

	if (routes[node.attr.veh].unload_depot == false)
		return veh_type.travelTime(*node.vertex, *node.next_node->vertex);

	const VRP_vertex& depot_vertex = * routes[node.attr.veh].first_node->vertex;

	const VRP_request* next_node_req;
	if (node.next_node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
		next_node_req = node.next_node->online_request;
	else 
		next_node_req = &node.next_node->vertex->getRequest();

	if (node.attr.current_load + next_node_req->demand > veh_type.getCapacity())
		return veh_type.travelTime(*node.vertex, depot_vertex) + veh_type.serviceTime(depot_vertex) + veh_type.travelTime(depot_vertex, *node.next_node->vertex);
	else 
		return veh_type.travelTime(*node.vertex, *node.next_node->vertex);

}

double VRP_routes::getRouteLength(int route) const {
	checkParamRoute(route, false, false);
	if (route > 0) return routes[route].routeLength;
	double cost = 0.0;
	for (int k = 1; k <= nRoutes; k++) 
		cost += routes[k].routeLength;
	return cost;
}

double VRP_routes::getRouteCost(int route) const {
	checkParamRoute(route, false, false);
	if (route > 0) return routes[route].routeLength * routes[route].vehicle->getCostPerDistUnit();
	double cost = 0.0;
	for (int k = 1; k <= nRoutes; k++) 
		cost += routes[k].routeLength * routes[k].vehicle->getCostPerDistUnit();
	return cost;
}

void VRP_routes::setCurrentTime(double t) {
	ASSERT(currentTime <= t, "");
	currentTime = t;
	updateRouteInfos(-42, false, true);
}

bool VRP_routes::isFixed(int route, int pos) const {
	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	checkParamPosition(route, pos, false, false, false);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)

	const Node & node = * routes[route].pos2node[pos];
	return node.attr.fixed;
}

void VRP_routes::selectRandomMoveableVertex(int* found_route, int* found_pos, int route) const {
	int x;

	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(getNumberMoveableVertices(route) > 0, "no free vertex !");
	*found_route = route;
	x = rand() % getNumberMoveableVertices(route) + 1;

	*found_pos = routes[*found_route].firstFreeNodePos + x - 1;	

	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(!pos2node(*found_route, *found_pos).attr.fixed, "trying to return a fixed vertex !" << endl << toString(true) << endl);
	ASSERT(pos2node(*found_route, *found_pos).vertex->getType() != VRP_vertex::DEPOT, "trying to return a non-moveable vertex ! (a depot)");
}

void VRP_routes::selectRandomFreeVertex(int* found_route, int* found_pos, int route) const {
	int x;

	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(getNumberFreeVertices(route) > 0, "no free vertex in the selected route ! route=" << route << endl << toString(true));
	*found_route = route;
	x = rand() % getNumberFreeVertices(route) + 1;

	ASSERT(*found_route > 0, "no route found !");

	// find the position of the x'th moveable vertex
	*found_pos = routes[*found_route].firstFreeNodePos + x - 1;	

	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(!pos2node(*found_route, *found_pos).attr.fixed, "trying to return a fixed vertex ! node: "<< pos2node(*found_route, *found_pos).vertex->toString() << endl << toString(true) << endl);
}

void VRP_routes::selectRandomFreeWaitingVertex(int* found_route, int* found_pos, int route) const { 
	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(getNumberFreeWaitingVertices(route) > 0, "no free waiting vertex in the selected route ! route=" << route);
	*found_route = route;
	int x = rand() % getNumberFreeWaitingVertices(route) + 1;
	int curr_pos = routes[route].firstFreeWaitingPos;
	for (int i=1; i < x; i++) {
		do
			curr_pos++;
		while( pos2node(route, curr_pos).vertex->getType() != VRP_vertex::WAITING );
		ASSERT(curr_pos < getRouteSize(route), "");
	}
	*found_pos = curr_pos;

	ASSERT(pos2node(*found_route, *found_pos).vertex->getType() == VRP_vertex::WAITING, "not returning a waiting vertex ! node:"<< pos2node(*found_route, *found_pos).vertex->toString() << endl << toString(true) << endl);
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(!pos2node(*found_route, *found_pos).attr.fixed, "trying to return a fixed vertex ! node: "<< pos2node(*found_route, *found_pos).vertex->toString() << endl << toString(true) << endl);
}


void VRP_routes::selectRandomUNLEFTWaitingVertex(int* found_route, int* found_pos, int route) const { 
	checkParamRoute(route, false, false);				// (int route, int routeZero, bool enforceRouteZero)
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(getNumberUNLEFTWaitingVertices(route) > 0, "no unleft waiting vertex in the selected route ! route=" << route);
	*found_route = route;
	int x = rand() % getNumberUNLEFTWaitingVertices(route) + 1;
	int curr_pos = routes[route].firstUNLEFTWaitingPos;
	for (int i=1; i < x; i++) {
		do
			curr_pos++;
		while( pos2node(route, curr_pos).vertex->getType() != VRP_vertex::WAITING );
		ASSERT(curr_pos < getRouteSize(route), "");
	}
	*found_pos = curr_pos;

	ASSERT(pos2node(*found_route, *found_pos).vertex->getType() == VRP_vertex::WAITING, "not returning a waiting vertex ! node:"<< pos2node(*found_route, *found_pos).vertex->toString() << endl << toString(true) << endl);
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(!pos2node(*found_route, *found_pos).attr.left, "trying to return a left vertex ! node: "<< pos2node(*found_route, *found_pos).vertex->toString() << endl << toString(true) << endl);
}

void VRP_routes::selectRandomMoveableSegment(int* found_pos1, int* found_pos2, int route, int min_size, int max_size) const {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	checkParamSize(min_size); checkParamSize(max_size);
	ASSERT(min_size >= 1 && min_size <= max_size, "");
	if (assertLevel != VRP_routes::ASSERT_NO_FIXED_LEFT) 
		ASSERT(getNumberMoveableVertices(route) >= min_size, "not enough moveable vertices in the selected route !");

	int nbConsecutiveCandidateNodes = getNumberMoveableVertices(route);
	// cout << "nbConsecutiveCandidateNodes in route " << route << ": " << nbConsecutiveCandidateNodes << endl;

	*found_pos1 = rand() % (nbConsecutiveCandidateNodes - min_size +1) + routes[route].firstFreeNodePos;
	if (min_size == max_size)
		*found_pos2 = *found_pos1 + min_size - 1;
	else {
		int remaining_moveable_vertices_including_pos1 = getRouteSize(route) - *found_pos1;
		int nbSuppVertices = rand() % min(max_size, remaining_moveable_vertices_including_pos1);
		*found_pos2 = *found_pos1 + nbSuppVertices;
	} 
	checkParamPosition(route, *found_pos1, false, true, true);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
	checkParamPosition(route, *found_pos2, false, true, true);	// (int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed)
}


double VRP_routes::getArrivalTimeAtPos(int route, int pos) const {
	return pos2node(route,pos).attr.arrival_time;
}
double VRP_routes::getServiceTimeAtPos(int route, int pos) const {
	return pos2node(route,pos).attr.service_time;
}
double VRP_routes::getDepartureTimeAtPos(int route, int pos) const {
	return pos2node(route,pos).attr.departure_time;
}


void VRP_routes::getCurrentAction(int route, int *pos, ActionType *action, double *start_time, double *end_time, double t) {
	if (t == -42) t = currentTime;
	if (routes[route].first_node->attr.departure_time > t) {	// if currently waiting at the starting depot
		*pos = 0;
		*action = WAIT_AT;
		*start_time = routes[route].first_node->attr.arrival_time;
		*end_time = routes[route].first_node->attr.departure_time;
		return;
	}
	int p = 1;
	double last_departure_time = routes[route].first_node->attr.departure_time;
	for (const Node* node = routes[route].first_node->next_node; node != NULL; node = node->next_node) {
		const VRP_request* request;
		if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE) request = node->online_request;
		else request = & node->vertex->getRequest();

		if (t < node->attr.arrival_time) {
			*pos = p;
			*action = MOVE_TO;
			*start_time = last_departure_time;
			*end_time = node->attr.arrival_time;
			return;
		}
		if (t < node->attr.service_time) {
			*pos = p;
			*action = WAIT_SERVE;
			*start_time = node->attr.arrival_time;
			*end_time = node->attr.service_time;
			return;	
		}
		if (node->attr.service_time <= t && t < node->attr.service_time + routes[route].vehicle->totalServiceTime(*request)) {
			*pos = p;
			*action = SERVE;
			*start_time = node->attr.service_time;
			*end_time = node->attr.service_time + routes[route].vehicle->totalServiceTime(*request);
			return;	
		}
		if (t < node->attr.departure_time) {
			*pos = p;
			*action = WAIT_AT;
			*start_time = node->attr.service_time + routes[route].vehicle->totalServiceTime(*request);
			*end_time = node->attr.departure_time;
			return;	
		}
		p++;
		last_departure_time = node->attr.departure_time;
	}
}
void VRP_routes::getNextPlannedAction(int route, int *pos, ActionType *action, double *start_time, double *end_time, double t) {
	checkParamRoute(route, false, false);	// (int route, int routeZero, bool enforceRouteZero)
	if (t == -42) t = currentTime;
	if (t < routes[route].first_node->attr.departure_time) {	// if currently waiting at the starting depot
		if (routes[route].first_node->next_node->vertex->getType() == VRP_vertex::DEPOT) {
			*pos = 0;
			*action = NO_ACTION;
		} else {
			*pos = 1;
			*action = MOVE_TO;
			*start_time = routes[route].first_node->attr.departure_time;
			*end_time = routes[route].first_node->next_node->attr.arrival_time;
		}
		return;
	}
	int p = 1;
	for (const Node* node = routes[route].first_node->next_node; node != NULL; node = node->next_node) {
		const VRP_request* request;
		if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE) request = node->online_request;
		else request = & node->vertex->getRequest();
		
		if (t < node->attr.arrival_time && node->attr.arrival_time < node->attr.service_time) {
			*pos = p;
			*action = WAIT_SERVE;
			*start_time = node->attr.arrival_time;
			*end_time = node->attr.service_time;
			return;	
		}

		if ( (node->attr.arrival_time <= t && t < node->attr.service_time)
				|| (t < node->attr.arrival_time && node->attr.arrival_time == node->attr.service_time) ) {
			*pos = p;
			*action = SERVE;
			*start_time = node->attr.service_time;
			*end_time = node->attr.service_time + routes[route].vehicle->totalServiceTime(*request);
			return;	
		}


		if (node->attr.service_time <= t && t < node->attr.service_time + routes[route].vehicle->totalServiceTime(*request)) {
			if (node->attr.service_time + routes[route].vehicle->totalServiceTime(*request) == node->attr.departure_time) {
				*pos = p+1;
				*action = MOVE_TO;
				*start_time = node->attr.departure_time;
				*end_time = node->attr.arrival_time;
				return;
			} else {
				ASSERT(node->attr.service_time + routes[route].vehicle->totalServiceTime(*request) < node->attr.departure_time, "");
				*pos = p;
				*action = WAIT_AT;
				*start_time = node->attr.service_time + routes[route].vehicle->totalServiceTime(*request);
				*end_time = node->attr.departure_time;
				return;
			}	
		}
		if (node->attr.service_time + routes[route].vehicle->totalServiceTime(*request) <= t && t < node->attr.departure_time) {
			*pos = p+1;
			*action = MOVE_TO;
			*start_time = node->attr.departure_time;
			*end_time = node->next_node->attr.arrival_time;
			return;	
		}
		p++;
	}
}


bool VRP_routes::getAttributes(const VRP_vertex& v, double *waiting_time, int *pos, int *route, double *arrival_time, double *service_time, double *departure_time, bool *fixed, bool *left, bool *served) const {
	// if (vpointer2vnode.count(&v) == 0)
	// 	return false;

	// Node* node;
	// auto it = vpointer2vnode.find(&v);
	// if (it != vpointer2vnode.end())
	// 	node = it->second;
	// else _ASSERT_(false, "argh");

	for (int k = 0; k <= nRoutes; k++) {
		for (Node* node = routes[k].first_node->next_node; node != NULL; node = node->next_node) {
			if (node->vertex == &v) {
				*waiting_time 	= node->attr.waiting_time;
				*pos 			= node->attr.pos;
				*route 			= node->attr.veh;
				*arrival_time 	= node->attr.arrival_time;
				*service_time 	= node->attr.service_time;
				*departure_time = node->attr.departure_time;
				*fixed 			= node->attr.fixed;
				*left 			= node->attr.left;
				*served 		= node->attr.served;
				return true;
			}
		}
	}

	// *waiting_time 	= node->attr.waiting_time;
	// *pos 			= node->attr.pos;
	// *route 			= node->attr.veh;
	// *arrival_time 	= node->attr.arrival_time;
	// *service_time 	= node->attr.service_time;
	// *departure_time = node->attr.departure_time;
	// *fixed 			= node->attr.fixed;
	// *left 			= node->attr.left;
	// *served 		= node->attr.served;

	// return true;
	return false;
}


string& VRP_routes::toString(bool verbose) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);
	if (verbose) {
		out << "nRoutes: " << getNumberRoutes() 
			<< ", nNonEmptyRoutes: " << getNumberNonEmptyRoutes() 
			<< ", nFreeRoutes: " << getNumberFreeRoutes() 
			<< ", nModifiableRoutes: " << getNumberModifiableRoutes() 
			<< ", nFreeVertices: " << getNumberFreeVertices() 
			<< ", nMoveableVertices: " << getNumberMoveableVertices()
			<< ", nFreeWaitingVertices: " << getNumberFreeWaitingVertices()
			<< ", nUNLEFTWaitingVertices: " << getNumberUNLEFTWaitingVertices()
			<< ", currentTime = " << currentTime 
			<< ", waitingStrategy = " << waitingStrategy 
			<< endl << LINE << endl;
	}

	for(int k=1; k <= nRoutes; k++) {
		out << "Route " << setw(2) << k  << "(" << routes[k].vehicle->getName();
		if(routes[k].unload_depot) out << "*U";
		out << "):\t";
		if (verbose) {
			out
				// << ", nNodes: "<< routes[k].nNodes 
				// << ", size: "<< getRouteSize(k) 
				<< "veh: " << routes[k].vehicle->toString()
				<< ", firstFreeNodePos: "<< routes[k].firstFreeNodePos 
				<< ", firstUNLEFTpos: "<< routes[k].firstUNLEFTpos 
				<< ", firstFreeWaitingPos: "<< routes[k].firstFreeWaitingPos 
				<< ", nFreeVertices: "<< getNumberFreeVertices(k) 
				<< ", nMoveableVertices: "<< getNumberMoveableVertices(k) 
				<< ", nUNLEFTWaitingVertices: "<< getNumberUNLEFTWaitingVertices(k)
				<< ", routeLoad: "<< routes[k].routeLoad 
				<< ", routeLength: "<< routes[k].routeLength
				<< endl; 
		}
		if (verbose)
			out << "\tVERTEX" << setw(6) << "POS" << setw(6) << "LOAD" << setw(8) << "WAIT" << "\tFIXED\tSERVED\tLEFT\tARRIVAL T.\tSERVICE T.\tDEPART. T.\tvertex instance id[REQUEST INFO.]" << endl;
		for (Node* node = routes[k].first_node; node != NULL; node = node->next_node) {
			const VRP_request* req;
			if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE)
				req = node->online_request;
			else req = &node->vertex->getRequest();
			if(!verbose) {
				if(req->l < node->attr.arrival_time) out << outputMisc::redExpr(req->l < node->attr.arrival_time) << "!" << outputMisc::resetColor();
				
				out << outputMisc::greenExpr(true) << outputMisc::blueExpr(node->attr.fixed) << outputMisc::magentaExpr(node->attr.served) << outputMisc::redExpr(node->attr.left) ;
				
				if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE) out << "O" << node->online_request->revealTS << "_" << setfill('0') << setw(3) << node->vertex->getRegion() << setfill(' ') ;
				else out << node->vertex->toString();
				out << outputMisc::resetColor();
				
				if (node->attr.waiting_time > 0 || (node->vertex->getType() == VRP_vertex::WAITING && waitingStrategy != RELOCATION_ONLY)) out << "[" << node->attr.waiting_time << "]";
				if (node->vertex->getType() == VRP_vertex::REGULAR_ONLINE && !node->online_request->hasAppeared()) out << "*";
				if (node->next_node) out << " -";
				if (node->next_node && node->attr.current_load > 0 && node->next_node->attr.current_load == 0) out << "U- ";
				else out << " ";
			} else {
				out << "\t" << node->vertex->toString();
				out << setw(8) << node->attr.pos << setw(6) << node->attr.current_load
					<< setw(8) << node->attr.waiting_time << "\t" << node->attr.fixed << "\t" << node->attr. served
					<< "\t" << node->attr.left << "\t" << setw(8) << node->attr.arrival_time << "\t" << setw(8) << node->attr.service_time << "\t" 
					<< setw(8) << node->attr.departure_time << "\t" << node->vertex->toString(true) 
					<< "  s+:" << routes[k].vehicle->totalServiceTime(*req);
				if (node->next_node) out 	<< "ttime=" << routes[k].vehicle->travelTime(* node->vertex, * node->next_node->vertex)
											<< " ttime+=" << travelTimeToNextNodeFrom(* node)
											<< " dist=" << routes[k].vehicle->distance(* node->vertex, * node->next_node->vertex);
				out	<< endl;
			}
		}
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}

