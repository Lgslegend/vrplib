# README #

This is the VRPlib library for Vehicle Routing Problems, oriented Dynamic and/or Stochastic VRPs.

It contains the object oriented representations of a VRP solutions and its constraints, 
together with complex evaluation functions in case of Dyanmic and/or Stochastic extensions.

In particular, solutions for the DS-VRPTW (see **[1]**) and the SS-VRTPW-CR (see **[2]**, **[3]**) 
can be represented, and their expected cost computed, using the present library. 

The library also includes a basic Local Search algorithm, and APIs that allows translations 
between the inner representatin of a (SS/DS-)VRPTW in the library to binary IP vehicle flow 
formulation (so the library can be used coupled with CPLEX/Gurobi/...).

Pratical implementations of (SS/DS-)VRPs solvers can be found in separated repositories: 

* [SS-VRPTW-CR](https://bitbucket.org/mstguillain/ss-vrptw-cr)

* [DS-VRPTW](https://bitbucket.org/mstguillain/ds-vrptw)

* SS-VRP: work in progress

Check the [Wiki](https://bitbucket.org/mstguillain/vrplib/wiki/Home).


---


Copyright 2017 Michael Saint-Guillain.

The library is under GNU Lesser General Public License (LGPL) v3.

**Contact**: Michael Saint-Guillain *<m dot stguillain at famous google mail dot com>*

---


**[1]** Saint-Guillain, M., Deville, Y., Solnon, C., 2015. *A Multistage Stochastic Programming 
Approach to the Dynamic and Stochastic VRPTW.* In: CPAIOR 2015. Springer International Publishing, pp. 357–374.    
[_Download link_](http://becool.info.ucl.ac.be/biblio/multistage-stochastic-programming-approach-dynamic-and-stochastic-vrptw)

**[2]** Saint-Guillain, M., Solnon, C., Deville, Y., 2017. *The Static and Stochastic VRP 
with Time Windows and both random Customers and Reveal Times.* In: EvoSTOC 2017. Springer International Publishing, pp. 110–127.   
[_Download link_](http://becool.info.ucl.ac.be/biblio/static-and-stochastic-vrp-time-windows-and-both-random-customers-and-reveal-times)

**[3]** Saint-Guillain, M., Solnon, C., Deville, Y., 2017. *The Static and Stochastic VRPTW both random Customers and Reveal Times: algorithms and recourse strategies.*   
_To be published_