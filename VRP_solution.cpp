/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_solution +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_solution.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#include <iostream>
#include <cstdlib>	// rand
#include <limits>
// #include <vector>
// #include <cmath>
#include "VRP_instance.h"
#include "VRP_routes.h"
#include "VRP_solution.h"
#include "tools.h"

using namespace std;




/* ---------------------------------------------------------------------------------------- */
/* VRP_solution    ------------------------------------------------------------------------ */
/* ---------------------------------------------------------------------------------------- */
void VRP_solution::init() {
	scale = I.getScale();
	// routes = new VRP_routes(I, I.getVertexFromRegion(VRP_vertex::DEPOT, 0));
	// ASSERT(I.get_depot_vertices().size() == 1, "Argh !");
	// routes = new VRP_routes(I, *I.get_depot_vertices().begin());
	routes = new VRP_routes(I);
	n_violations_route.clear(); n_violations_route.push_back(-1);	// for route 0 - not used
	w_violations_route.clear(); w_violations_route.push_back(-1);	// for route 0 - not used
	// cost_route.clear();
	// cost_route.push_back(-1.0);
	// cout << "Creating new solution" << endl;

	// for (int k=1; k <= I.getNumberVehicles(); k++) {		// Create one route for each vehicle (according to the maximum number of vehicles as described by the instance)
	// 	const VRP_vertex* depot_v = *I.get_depot_vertices().begin();
	// 	routes->addRoute(depot_v);
	// 	n_violations_route.push_back(0); 
	// 	w_violations_route.push_back(0.0);
	// 	// cost_route.push_back(0.0);
	// }

	// for (const VRP_vertex* depot_v : I.get_depot_vertices()) {
	// 	for (const VRP_vehicle* vehicle : depot_v->get_vehicles()) {
	// 		routes->addRoute(depot_v, vehicle);
	// 		n_violations_route.push_back(0); 
	// 		w_violations_route.push_back(0.0);
	// 	}
	// }

	initial_solution_generated = false;
	n_violations = 0;
	w_violations = 0.0;
	// cost = 0.0;
}

VRP_solution::VRP_solution(const VRP_instance& instance) : I(instance) {
	init();
}

VRP_solution::VRP_solution(const VRP_solution& sol) : I(sol.I) {
	init();
	for (int route = 1; route <= sol.getNumberRoutes(); route++)
		addRoute(sol.routes->getVertexFromPos(route, 0), sol.routes->getVehicleTypeFromRoute(route));
	*this = sol;
}

VRP_solution& VRP_solution::operator = (const VRP_solution& sol) {
	ASSERT(this != &sol, "trying to copy the solution to itself !");
	ASSERT( &(this->I) == &(sol.I) , "trying to copy a solution from a solution based on a different instance !" );

	*(this->routes) = *(sol.routes);

	this->initial_solution_generated = sol.initial_solution_generated;
	this->type = sol.type;

	this->n_violations = sol.n_violations; this->n_violations_route = sol.n_violations_route;
	this->w_violations = sol.w_violations; this->w_violations_route = sol.w_violations_route;

	return *this;
}

void VRP_solution::copyRouteFrom(const VRP_solution& sol, SolutionElement route) {
	ASSERT(this != &sol, "trying to copy the solution to itself !");
	ASSERT( &(this->I) == &(sol.I) , "trying to copy a solution from a solution based on a different instance !" );
	ASSERT(route.type == ROUTE, "");
	ASSERT(routes->getVehicleTypeFromRoute(route.route) == sol.routes->getVehicleTypeFromRoute(route.route), "");
	this->routes->copyRouteFrom(*sol.routes, route.route);
	n_violations_route[route.route] = sol.n_violations_route[route.route];
	w_violations_route[route.route] = sol.w_violations_route[route.route];
}

VRP_solution::~VRP_solution() {
	delete routes;
}

int VRP_solution::addRoute(const VRP_vertex& depot, const VRP_VehicleType& vehicle, int n) {
	for (int i = 0; i < n; i++) {
		routes->addRoute(&depot, &vehicle);
		n_violations_route.push_back(0); 
		w_violations_route.push_back(0.0);
	}
	return getNumberRoutes();
}

void VRP_solution::addRoutes(int n) {
	_ASSERT_(n > 0, "");
	_ASSERT_(I.getDepotVertices().size() == 1, "");
	_ASSERT_(I.getVehicleTypes().size() == 1, "");
	for (int route = 1; route <= n; route++)
		addRoute(**I.getDepotVertices().begin(), **I.getVehicleTypes().begin());

}

// void VRP_solution::activateAutomaticVehicleUnload(int route, const VRP_vertex& depot) {
void VRP_solution::activateAutomaticVehicleUnload(int route) {
	_ASSERT_(0 < route && route <= getNumberRoutes(), "invalid route identifyer");
	// _ASSERT_(depot.getType() == VRP_vertex::UNLOAD_DEPOT, "");
	routes->activateAutomaticVehicleUnload(route);
}

void VRP_solution::clearAllRoutes() {
	for (int k=1; k <= getNumberRoutes(); k++)
		routes->clearRoute(k);
}

void VRP_solution::solution_changed(int route) {
	update_violations(route);
} 

void VRP_solution::update_violations(int route) {
	ASSERT(initial_solution_generated, "");
	
	/* update VIOLATIONS */
	for (int route_=1; route_ <= routes->getNumberRoutes(); route_++) {		// for each route
		if (route != -42 && route_ != route) continue;
		int vehicle_load = 0;
		n_violations_route[route_] = 0;
		w_violations_route[route_] = 0.0;
		if (routes->automaticVehicleUnload(route_) == false) {		// there can be a capacity violation only if the automatic unloading is not activated on that route
			for (int pos=1; pos <= routes->getRouteSize(route_); pos++) {		// for each vertex in the route, including the ending depot vertex
				vehicle_load += routes->getRequestFromPos(route_, pos).demand;
			}
			int maxCapacity = routes->getVehicleTypeFromRoute(route_).getCapacity();
			n_violations_route[route_] += vehicle_load > maxCapacity;
			w_violations_route[route_] += max(0, vehicle_load - maxCapacity);
		}
	}
	n_violations = 0;
	w_violations = 0.0;
	for (int route_=1; route_ <= routes->getNumberRoutes(); route_++) {
		n_violations += n_violations_route[route_];
		w_violations += w_violations_route[route_];
	}
} 

double VRP_solution::getCost(int route) const {
	return routes->getRouteCost(route);
}

int VRP_solution::getNumberViolations(int route) const {
	if (route > 0) return n_violations_route[route];
	return n_violations;
}
double VRP_solution::getWeightViolations(int route) const {
	if (route > 0) return w_violations_route[route];
	return w_violations;
}


void VRP_solution::copyFromSol_scaleWaitingTimes(const VRP_solution& sol, bool try_preserve_feasibility) {
	// initializeFromSol_scaleWaitingTimes(sol, sol.scale / this->scale);
	_ASSERT_(&sol != this, "Trying to copy a solution into itself !");
	_ASSERT_(initial_solution_generated, "argh");
	_ASSERT_(getNumberRoutes() == sol.getNumberRoutes(), "");
	clearAllRoutes();

	for (int route=1; route <= sol.getNumberRoutes(); route++) {
		// _ASSERT_(routes->getVehicleTypeFromRoute(route) == sol.routes->getVehicleTypeFromRoute(route), routes->getVehicleTypeFromRoute(route).toString() << " ≠ " << sol.routes->getVehicleTypeFromRoute(route).toString());
		for (int pos=1; pos < sol.getRouteSize(route); pos++) {
			routes->insertVertexLastPos(& sol.routes->getVertexFromPos(route,pos), route);
			double new_wtime = sol.routes->getWaitingTime(route, pos) * (sol.scale / this->scale);

			if (try_preserve_feasibility) {
				// if the scaling involves a rounding up in the travel time, then remove one unit of waiting time to compensate (to avoid infeasibility)
				const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(route);
				if (scale != sol.scale && ceil(veh.travelTime(getVertexAtPosition(route, pos-1), getVertexAtPosition(route, pos)) * (this->scale/sol.scale)) > veh.travelTime(sol.getVertexAtPosition(route, pos-1), sol.getVertexAtPosition(route, pos)) ) 		{
					if (abs(new_wtime - floor(new_wtime)) < 0.0001)	// but do it only if scaling the waiting time does itself not remove one waiting time unit 
						new_wtime--;
				}
			}
			
			new_wtime = max((double) (type==SS_VRPTW_CR), floor(new_wtime));
			routes->setWaitingTime(route, pos, new_wtime);
		}
	}
	solution_changed();
}


bool VRP_solution::operator == (const VRP_solution& other) const {
	return (*routes) == (*other.routes);
}


int VRP_solution::selectRandomRoute(int min_size, bool free_, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route) const {
	int candidate_routes[VRP_routes::MAX_ROUTES];
	int nb_candidate_routes = 0;

	for (int k=1; k <= getNumberRoutes(); k++) {
		if (		(!free_ || !isRouteFixed(k)) 												// free==true 		==> the ending depot of the route cannot be fixed
				&& 	(!modifiable || getNumberMoveableVertices(k) >= min_size) 					// modifiable==true ==> #moveableVertices(in route k) ≥ min_size 
				&& 	(!containsFreeWaitingV || getNumberFreeWaitingVertices(k) >= min_size) 		// containsFreeWaitingV==true ==> #freeWaitingVertices(in route k) ≥ min_size 
				&& 	(!containsUNLEFTwaiting || getNumberUNLEFTWaitingVertices(k) >= min_size) 	// containsUNLEFTwaiting==true ==> #unleftWaitingVertices(in route k) ≥ min_size 
				&& 	(not_route <= 0 || k != not_route)) {										// not_route > 0 	==> k ≠ not_route
			candidate_routes[nb_candidate_routes] = k;
			nb_candidate_routes ++;
		}
	}
	ASSERT(nb_candidate_routes > 0, "no route with ≥min_size consecutive free==" << free_ << " and moveable== "<< modifiable << " vertices !" << endl << toString(true));
	int route = candidate_routes[rand() % nb_candidate_routes];

	return route; 
}

bool VRP_solution::exchangeable(const SolutionElement& v1, const SolutionElement& v2) const {
	ASSERT(v1.request && v2.request,"");
	if (v1.request->demand > routes->getVehicleTypeFromRoute(v2.route).getCapacity() || v2.request->demand > routes->getVehicleTypeFromRoute(v1.route).getCapacity())
		return false;
	return true;
}

void VRP_solution::selectRandomMoveableVertex(SolutionElement* v, int route, int not_route) const {
	ASSERT(route ==-42 || not_route ==-42, "");		// if route is set, then not_route isn't and vice versa
	v->type = VERTEX;
	int route_ = route;
	if (route_ < 0)
		route_ = selectRandomRoute(1, true, true, false, false, not_route);		// (int min_size, bool free_, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route)
	routes->selectRandomMoveableVertex(&(v->route), &(v->pos), route_);
	v->request = & routes->getRequestFromPos(v->route, v->pos);
}

void VRP_solution::selectRandomInsertionPosition(SolutionElement* p, int route, int not_route) const {
	ASSERT(route ==-42 || not_route ==-42, "");		// if route is set, then not_route isn't and vice versa
	p->type = POSITION;
	int route_ = route;
	if (route_ < 0)
		route_ = selectRandomRoute(1, true, false, false, false, not_route);		// (int min_size, bool free_, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route)
	routes->selectRandomFreeVertex(&(p->route), &(p->pos), route_);			
}

void VRP_solution::selectRandomInsertionPosition(const SolutionElement& e, SolutionElement* p, int route, int not_route) const {
	ASSERT(e.type == VERTEX || e.type == SEGMENT, "type: " << e.type);
	ASSERT(e.type != VERTEX || e.request, "");
	if (e.type == VERTEX) {
		do {
			selectRandomInsertionPosition(p, route, not_route);
		} while ((p->route == e.route && (p->pos == e.pos || p->pos == e.pos +1)) || routes->getVehicleTypeFromRoute(p->route).getCapacity() < e.request->demand);			// find a position that is different than v's current one, and in a route that has a vehicle able to handle v's request demand
	}
	if (e.type == SEGMENT) {
		double max_demand = 0, count = 0;
		for (int pos = e.pos_1; pos <= e.pos_2; pos++)
			max_demand = max(max_demand, routes->getRequestFromPos(e.route, pos).demand);
		do {
			selectRandomInsertionPosition(p, route, not_route);
			count++;
		} while (max_demand > routes->getVehicleTypeFromRoute(p->route).getCapacity() && count < 5);	
	}
}

void VRP_solution::selectRandomMoveableSegment(SolutionElement* s, int min_size, int max_size, int route, int not_route) const {
	ASSERT(min_size >= 1 && min_size <= max_size, "");
	ASSERT(route ==-42 || not_route ==-42, "");		// if route is set, then not_route isn't and vice versa
	int route_ = route;
	if (route_ <= 0)
		route_ = selectRandomRoute(min_size, true, true, false, false, not_route);	// (int min_size, bool free_, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route)
	s->route = route_;
	routes->selectRandomMoveableSegment(&(s->pos_1), &(s->pos_2), route_, min_size, max_size);
	s->type = SEGMENT;
}

void VRP_solution::selectRandomWaitingLocation(SolutionElement* wl) const {
	_ASSERT_(false, "");
	wl->type = WAITING_LOC;
	wl->region = rand() % I.getNumberVertices(VRP_vertex::WAITING)  +1; 	// exclude the depot region
}

void VRP_solution::selectRandomFreePlannedWaitingVertex(SolutionElement* w, int route) const {
	int route_ = route;
	if (route_ < 0)
		route_ = selectRandomRoute(1, true, false, true, false, false);		// (int min_size, bool free_, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route)
	w->type = VERTEX;
	routes->selectRandomFreeWaitingVertex(&(w->route), &(w->pos), route_);
	w->region = routes->getVertexFromPos(w->route, w->pos).getRegion();
}

void VRP_solution::selectRandomUNLEFTWaitingVertex(SolutionElement* w, int route) const {
	int route_ = route;
	if (route_ < 0)
		route_ = selectRandomRoute(1, true, false, false, true, false);		// (int min_size, bool free_, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route)
	w->type = VERTEX;
	routes->selectRandomUNLEFTWaitingVertex(&(w->route), &(w->pos), route_);
}

void VRP_solution::selectBestInsertionPosition(const SolutionElement& v, SolutionElement* p) {
	_ASSERT_(false, "not implemented yet " << v.route << p->route);
	UNUSED(v); UNUSED(p);
}
void VRP_solution::selectBestRelocationPosition(const SolutionElement& v, SolutionElement* p) {
	// _ASSERT_(false, "not implemented yet " << v.route << p->route);
	ASSERT(v.type == VERTEX, "");
	// selectRandomInsertionPosition(p);

	SolutionElement best_pos;
	best_pos.type = POSITION; best_pos.route = v.route; best_pos.pos = v.pos;
	double best_cost_diff = 0;
	double best_viol_diff = 0;


	WaitingStrategy ws = routes->getWaitingStrategy();
	routes->setWaitingStrategy(DRIVE_FIRST);



	const VRP_vertex& vertex = getVertexAtElement(v);
	const VRP_request& request = routes->getRequestFromPos(v.route, v.pos);
	const VRP_vertex& prev_vertex = getVertexAtPosition(v.route, v.pos-1);
	const VRP_vertex& next_vertex = getVertexAtPosition(v.route, v.pos+1);
	const VRP_VehicleType& curr_veh = routes->getVehicleTypeFromRoute(v.route);

	double cost_gain = curr_veh.travelCost(prev_vertex, vertex) + curr_veh.travelCost(vertex, next_vertex) - curr_veh.travelCost(prev_vertex, next_vertex) ;


	double time_saved_by_removing = -curr_veh.travelTime(prev_vertex, next_vertex) + (curr_veh.travelTime(prev_vertex, vertex) + curr_veh.totalServiceTime(request) + curr_veh.travelTime(vertex, next_vertex));
	
	// WHAT do we SAVE in TW VIOLATIONS in v'S ROUTE by REMOVING the VERTEX ?
	double viol_reduction = max((double) 0, routes->getArrivalTimeAtPos(v.route, v.pos) -  request.l);		// first the vertex violation itself
	for (int pos = v.pos + 1; pos <= getRouteSize(v.route); pos++) {	
		const VRP_request& curr_request = routes->getRequestFromPos(v.route, pos);
		
		time_saved_by_removing = min(time_saved_by_removing, routes->getArrivalTimeAtPos(v.route, pos) - curr_request.e);
		if (time_saved_by_removing <= 0) break;

		double curr_viol = max((double) 0, routes->getArrivalTimeAtPos(v.route, pos) -  curr_request.l);
		viol_reduction += min(curr_viol, time_saved_by_removing);
	}


	for (int route = 1; route <= getNumberRoutes(); route++) {
		// if (route == v.route) continue;
		if (routes->getFirstFreePos(route) < 0) continue;

		for (int pos = routes->getFirstFreePos(route); pos <= getRouteSize(route); pos++) {
			if (route == v.route && (pos == v.pos || pos == v.pos+1)) continue;
			const VRP_vertex& curr_vertex = getVertexAtPosition(route, pos);
			const VRP_vertex& prev_curr_vertex = getVertexAtPosition(route, pos);
			const VRP_vertex& next_curr_vertex = getVertexAtPosition(route, pos);
			const VRP_VehicleType& new_veh = routes->getVehicleTypeFromRoute(route);

			// WHAT DO WE VIOLATE MORE BY INSERTING IN THAT ROUTE ?

			double viol_increase = max((double) 0, routes->getDepartureTimeAtPos(route, pos-1) + new_veh.travelTime(prev_curr_vertex, vertex) - request.l ) ;	// first its own violation if we insert here
				
			double time_loss_by_inserting =  max((double) 0, ( max(routes->getDepartureTimeAtPos(route, pos-1) + new_veh.travelTime(prev_curr_vertex, vertex), request.e) + new_veh.totalServiceTime(request) + new_veh.travelTime(vertex, curr_vertex)) - routes->getArrivalTimeAtPos(route, pos));

			for (int pos_ = pos + 1; pos_ <= getRouteSize(route); pos_++) {							// then violations of all the following vertices
				if (route == v.route && (pos_ == v.pos || pos_ == v.pos+1)) continue;
				const VRP_request& curr_request = routes->getRequestFromPos(route, pos_);

				double wait = max((double) 0, routes->getArrivalTimeAtPos(route, pos_) + time_loss_by_inserting - curr_request.e);
				time_loss_by_inserting -= wait;
				if (time_loss_by_inserting <= 0) break;

				double curr_viol = max((double) 0, routes->getArrivalTimeAtPos(route, pos_) - curr_request.l);
				viol_increase += max((double) 0, max((double) 0, routes->getArrivalTimeAtPos(route, pos_) + time_loss_by_inserting - curr_request.l) - curr_viol);	// increase in violation = +[new violation - curr_viol]	
			}
			
			
			double cost_diff = new_veh.travelCost(prev_curr_vertex, vertex) + new_veh.travelCost(vertex, next_curr_vertex) - new_veh.travelCost(prev_curr_vertex, next_curr_vertex) - cost_gain;
			if (cost_diff < best_cost_diff) {
				best_cost_diff = cost_diff;
				best_pos.route = route;
				best_pos.pos = pos;
			}
			double viol_diff = viol_increase - viol_reduction;
			if (viol_diff < best_viol_diff) {
				best_viol_diff = viol_diff;
				best_pos.route = route;
				best_pos.pos = pos;
			}


		}
	}



	routes->setWaitingStrategy(ws);
	*p = best_pos;


	// cout << v.toString() << " ==> " << best_pos.toString() << endl;
}
void VRP_solution::selectWorstMoveableVertex(SolutionElement* v) {
	// _ASSERT_(false, "not implemented yet "  << v->route);
	// selectRandomMoveableVertex(v);



	SolutionElement best_v;
	best_v.type = VERTEX; best_v.route = 1; best_v.pos = 1;
	double best_cost_diff = 0;
	double best_viol_diff = 0;


	WaitingStrategy ws = routes->getWaitingStrategy();
	routes->setWaitingStrategy(DRIVE_FIRST);

	for (int route = 1; route <= getNumberRoutes(); route++) {
		if (routes->getFirstFreePos(route) < 0) continue;

		for (int pos = routes->getFirstFreePos(route); pos < getRouteSize(route); pos++) {
			const VRP_vertex& vertex = getVertexAtPosition(route, pos);
			const VRP_request& request = routes->getRequestFromPos(route, pos);
			const VRP_vertex& prev_vertex = getVertexAtPosition(route, pos-1);
			const VRP_vertex& next_vertex = getVertexAtPosition(route, pos+1);
			const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(route);

			double cost_gain = veh.travelCost(prev_vertex, vertex) + veh.travelCost(vertex, next_vertex) - veh.travelCost(prev_vertex, next_vertex) ;
			if ((-cost_gain) < best_cost_diff) {
				best_cost_diff = -cost_gain;
				best_v.route = route;
				best_v.pos = pos;
			}



			// WHAT do we SAVE in TW VIOLATIONS in ROUTE by REMOVING the VERTEX ?
			double time_saved_by_removing = -veh.travelTime(prev_vertex, next_vertex) + (veh.travelTime(prev_vertex, vertex) + veh.totalServiceTime(request) + veh.travelTime(vertex, next_vertex));

			double viol_reduction = max((double) 0, routes->getArrivalTimeAtPos(route, pos) -  request.l);		// first the vertex violation itself
			for (int pos_ = pos + 1; pos_ <= getRouteSize(route); pos_++) {													// then violations of all the following vertices
				const VRP_request& curr_request = routes->getRequestFromPos(route, pos_);
				
				time_saved_by_removing = min(time_saved_by_removing, routes->getArrivalTimeAtPos(route, pos_) - curr_request.e);
				if (time_saved_by_removing <= 0) break;

				double curr_viol = max((double) 0, routes->getArrivalTimeAtPos(route, pos_) -  curr_request.l);
				viol_reduction += min(curr_viol, time_saved_by_removing);
			}

			if ((-viol_reduction) < best_viol_diff) {
				best_viol_diff = -viol_reduction;
				best_v.route = route;
				best_v.pos = pos;
			}

		}
	}


	
	routes->setWaitingStrategy(ws);
	*v = best_v;
	// cout << best_v.toString() << endl;
}


void VRP_solution::moveVertex(const SolutionElement& v, const SolutionElement& p)	{
	ASSERT(v.type == VERTEX, "");
	ASSERT(p.type == POSITION, "");
	routes->moveVertex(v.route, v.pos, p.route, p.pos);
	solution_changed(v.route);
	if (v.route != p.route)
		solution_changed(p.route);
}
void VRP_solution::moveSegment(const SolutionElement& s, const SolutionElement& p)	{
	ASSERT(s.type == SEGMENT, "");
	ASSERT(p.type == POSITION, "");
	routes->moveSegment(s.route, s.pos_1, s.pos_2, p.route, p.pos);
	solution_changed(s.route);
	if (s.route != p.route)
		solution_changed(p.route);
}
void VRP_solution::swapVertices(const SolutionElement& v1, const SolutionElement& v2) {
	ASSERT(v1.type == VERTEX, "");
	ASSERT(v2.type == VERTEX, "");
	routes->swapVertices(v1.route, v1.pos, v2.route, v2.pos);
	solution_changed(v1.route);
	if (v1.route != v2.route)
		solution_changed(v2.route);
}
void VRP_solution::swapVertices(int route1, int pos1, int route2, int pos2) {
	routes->swapVertices(route1, pos1, route2, pos2);
}

void VRP_solution::swapSegments(const SolutionElement& s1, const SolutionElement& s2) {
	ASSERT(s1.type == SEGMENT, "");
	ASSERT(s2.type == SEGMENT, "");
	routes->swapSegments(s1.route, s1.pos_1, s1.pos_2, s2.route, s2.pos_1, s2.pos_2);
	solution_changed(s1.route);
	if (s1.route != s2.route)
		solution_changed(s2.route);
}

void VRP_solution::invertSegment(const SolutionElement& s) {
	ASSERT(s.type == SEGMENT, "");
	routes->invertSegment(s.route, s.pos_1, s.pos_2);
	solution_changed(s.route);
}

void VRP_solution::insertWaitingVertex(const SolutionElement& w_loc, const SolutionElement& pos) {
	_ASSERT_(false, "can't be called from an instance of class VRP_solution !");
	UNUSED(w_loc); UNUSED(pos);
}
double VRP_solution::removeWaitingVertex(const SolutionElement& w_vertex) {
	_ASSERT_(false, "can't be called from an instance of class VRP_solution !");
	UNUSED(w_vertex);
	return -1;
}
double VRP_solution::increaseWaitingTime(const SolutionElement& w_vertex, double increment) {
	_ASSERT_(false, "can't be called from an instance of class VRP_solution !");
	UNUSED(w_vertex); UNUSED(increment);
	return -1;
} 
double VRP_solution::decreaseWaitingTime(const SolutionElement& w_vertex, double decrement) {
	_ASSERT_(false, "can't be called from an instance of class VRP_solution !");
	UNUSED(w_vertex); UNUSED(decrement);
	return -1;
}


int VRP_solution::getNumberRoutes() const {
	return routes->getNumberRoutes();
}
int VRP_solution::getRouteSize(int route) const {
	return routes->getRouteSize(route);
}
int VRP_solution::getNumberNonFixedRoutes() const {
	return routes->getNumberFreeRoutes();
}
int VRP_solution::getNumberModifiableRoutes() const {
	return routes->getNumberModifiableRoutes();
}
int VRP_solution::getNumberNonEmptyRoutes() const {
	return routes->getNumberNonEmptyRoutes();
}
int VRP_solution::getNumberFreePostions(int route) const {		
	return routes->getNumberFreeVertices(route);				
}
int VRP_solution::getNumberMoveableVertices(int route) const {		
	return routes->getNumberMoveableVertices(route);				
}
int VRP_solution::getNumberFreeWaitingVertices(int route) const {
	return routes->getNumberFreeWaitingVertices(route);
}
int VRP_solution::getNumberUNLEFTWaitingVertices(int route) const {
	return routes->getNumberUNLEFTWaitingVertices(route);
}
int VRP_solution::getNumberUnvisitedWaitingVertices() const {
	_ASSERT_(false, "can't be called from an instance of class VRP_solution !");
	return 0;
}
bool VRP_solution::isRouteFixed(int route) const {
	return routes->getNumberFreeRoutes(route) == false;
}
bool VRP_solution::isPositionFixed(int route, int pos) const {
	return routes->isFixed(route, pos);
}
double	VRP_solution::getArrivalTimeAtElement(const SolutionElement& v) const {
	ASSERT(v.type == VERTEX, "");
	return routes->getArrivalTimeAtPos(v.route, v.pos);
}
double	VRP_solution::getServiceTimeAtElement(const SolutionElement& v) const {
	ASSERT(v.type == VERTEX, "");
	return routes->getServiceTimeAtPos(v.route, v.pos);
}
double	VRP_solution::getDepartureTimeAtElement(const SolutionElement& v) const {
	ASSERT(v.type == VERTEX, "");
	return routes->getDepartureTimeAtPos(v.route, v.pos);
}

const VRP_vertex& VRP_solution::getVertexAtElement(const SolutionElement& v) const {
	ASSERT(v.type == VERTEX, "");
	return getVertexAtPosition(v.route, v.pos);
}
const VRP_vertex& VRP_solution::getVertexAtPosition(int route, int pos) const {
	return routes->getVertexFromPos(route, pos);
}
const VRP_request& VRP_solution::getRequestAtPosition(int route, int pos) const {
	return routes->getRequestFromPos(route, pos);
}	
const VRP_VehicleType& VRP_solution::getVehicleTypeAtRoute(int route) const {
	return routes->getVehicleTypeFromRoute(route);
}	

VRP_solution::VertexAttr VRP_solution::getCurrentAttributes(const VRP_vertex& v) const {
	VertexAttr attr;
	attr.visited = routes->getAttributes(v, &attr.waiting_time, &attr.pos, &attr.route, &attr.arrival_time, &attr.service_time, &attr.departure_time, &attr.fixed, &attr.left, &attr.served);
	return attr;
}


double VRP_solution::getTraveledDistancePerVehicleType(const VRP_VehicleType& veh) const {
	double distance = 0.0;
	for (int route = 1; route <= getNumberRoutes(); route++)
		if (getVehicleTypeAtRoute(route) == veh)
			distance += routes->getRouteLength(route);
	return distance;
}


string& VRP_solution::toString(bool verbose) const {
	ostringstream out; 
	out << routes->toString(verbose);
	static string str = ""; str = out.str();
	return (str);
}

void VRP_solution::checkSolutionConsistency() const {
	// TODO
}







bool VRP_solution::SolutionElement::operator == (const VRP_solution::SolutionElement& other) const {
	return 		type == other.type 
			&& 	route == other.route 	&& 	route_1 == other.route_1 	&& 	route_2 == other.route_2
			&&	pos == other.pos		&& 	pos_1 == other.pos_1		&&	pos_2 == other.pos_2
			&& 	region == other.region;
}
bool VRP_solution::SolutionElement::operator < (const VRP_solution::SolutionElement& other) const {
	if (operator==(other)) return false;

	if (type != other.type)
		return type < other.type;
	if (route != other.route)
		return route < other.route;
	if (route_1 != other.route_1)
		return route_1 < other.route_1;
	if (route_2 != other.route_2)
		return route_2 < other.route_2;
	if (pos != other.pos)
		return pos < other.pos;
	if (pos_1 != other.pos_1)
		return pos_1 < other.pos_1;
	if (pos_2 != other.pos_2)
		return pos_2 < other.pos_2;
	if (region != other.region)
		return region < other.region;

	return false;
}

void VRP_solution::SolutionElement::getPos(SolutionElement* e) const {
	ASSERT(type == VERTEX || type == SEGMENT, "");
	e->clear();	// reinitialiaze object 
	e->type = POSITION;
	e->route = route;
	if (type == SEGMENT) e->pos = pos_1;
	else e->pos = pos;
}
void VRP_solution::SolutionElement::getVertex(SolutionElement* e) const {
	ASSERT(type == POSITION, "");
	e->clear();	// reinitialiaze object 
	e->type = VERTEX;
	e->route = route;
	e->pos = pos;
}
void VRP_solution::SolutionElement::getRegion(SolutionElement* e) const {
	ASSERT(type == VERTEX, "");
	ASSERT(region >= 0, "");
	e->clear();	// reinitialiaze object 
	e->type = WAITING_LOC;
	e->region = region;
}
void VRP_solution::SolutionElement::getSegment(int length, SolutionElement* e) const {
	ASSERT(type == POSITION, "");
	e->clear();	// reinitialiaze object 
	e->type = SEGMENT;
	e->route = route;
	e->pos_1 = pos;
	e->pos_2 = pos + length -1;
}
int VRP_solution::SolutionElement::getLength() const {
	ASSERT(type == SEGMENT, "");
	return pos_2 - pos_1 + 1;
}

string& VRP_solution::SolutionElement::toString() const {
	ostringstream out; 
	switch(type) {
		case VERTEX:
			out << "r" << route << ":p" << pos; break;
		case SEGMENT:
			out << "r" << route << ":p" << pos_1 << "-" << pos_2; break;
		case POSITION:
			out << "r" << route << ":p" << pos; break;
		case WAITING_LOC:
			out << "w" << region; break;
		case ROUTE:
			out << "r" << route; break;
		default: out << "no type set !";
	}
	static string str = ""; str = out.str();
	return (str);
}

void VRP_solution::SolutionElement::getRoute(SolutionElement* e) const {
	ASSERT(type != NO_TYPE, "");
	e->clear();	// reinitialiaze object 
	e->type = ROUTE;
	e->route = route;
}

void VRP_solution::getSolutionElementAtPosition(SolutionElement* v, int route, int pos) const {
	ASSERT(route > 0 && route <= routes->getNumberRoutes(), "");
	ASSERT(pos >= 0 && pos <= routes->getRouteSize(route), "");
	v->type = VERTEX;
	v->route = route; v->pos = pos;
	v->route_1 = -1, v->route_2 = -1, v->pos_1 = -1, v->pos_2 = -1;
}

void VRP_solution::getSolutionElementLastPositionFromRoute(SolutionElement* pos, int route) const {
	ASSERT(route > 0 && route <= routes->getNumberRoutes(), "route = " << route << "  getNumberRoutes() = " << routes->getNumberRoutes());
	pos->type = POSITION;
	pos->route = route; 
	pos->pos = routes->getRouteSize(route);
	pos->route_1 = -1, pos->route_2 = -1, pos->pos_1 = -1, pos->pos_2 = -1;
}


string& VehicleAction::toString() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	switch(action) {
		case NO_ACTION:
			out << "Vehicle " << vehicle << ": no action."; break;
		case SERVE:
			out << "Vehicle " << vehicle << ": SERVE vertex " << pvertex->toString() << " (pos: " << pos << "); start t.: " << start_time << ", end t.: " << end_time; break;
		case MOVE_TO:
			out << "Vehicle " << vehicle << ": MOVE TO vertex " << pvertex->toString() << " (pos: " << pos << "); start t.: " << start_time << ", end t.: " << end_time; break;
		case WAIT_AT:
			out << "Vehicle " << vehicle << ": WAIT AT vertex " << pvertex->toString() << " (pos: " << pos << "); start t.: " << start_time << ", end t.: " << end_time; break;
		case WAIT_SERVE:
			out << "Vehicle " << vehicle << ": WAIT to SERVE vertex " << pvertex->toString() << " (pos: " << pos << "); start t.: " << start_time << ", end t.: " << end_time; break;
		default: out << "no type set !";
	}
	static string str = ""; str = out.str();
	return (str);
}















/* ---------------------------------------------------------------------------------------- */
/* Solution_VRPTW    ---------------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------- */
Solution_VRPTW::Solution_VRPTW(const VRP_instance& instance) : VRP_solution(instance) {
	type = VRPTW;
	ASSERT(instance.getHorizon() < numeric_limits<int>::max(), "horizon size isn't set !");
}
Solution_VRPTW::Solution_VRPTW (const Solution_VRPTW &sol): VRP_solution(sol) {	// copy constructor

}		
Solution_VRPTW& Solution_VRPTW::operator = (const Solution_VRPTW& sol) {
	VRP_solution::operator = (sol);
	return *this;
}

void Solution_VRPTW::solution_changed(int route) {
	update_violations(route);
} 

void Solution_VRPTW::update_violations(int route) {
	/* update VIOLATIONS */
	for (int route_=1; route_ <= routes->getNumberRoutes(); route_++) {		// for each route
		if (route != -42 && route_ != route) continue;
		
		n_violations_route[route_] = 0;
		w_violations_route[route_] = 0.0;
		VRP_solution::update_violations(route_);

		for (int pos=1; pos <= routes->getRouteSize(route_); pos++) {		// for each vertex in the route, including the ending depot vertex
			int factor = 1;
			const VRP_request& req = routes->getRequestFromPos(route_, pos);
			n_violations_route[route_] += routes->getArrivalTimeAtPos(route_, pos) > req.l ;
			if (req.getVertex().getType() == VRP_vertex::REGULAR_ONLINE)
				factor = ((req.hasAppeared()*3) + 1);
			w_violations_route[route_] += factor * max(0.0, routes->getArrivalTimeAtPos(route_, pos) - req.l) ;
		}
		if (I.getHorizon() < numeric_limits<int>::max())
			n_violations_route[route_] += routes->getArrivalTimeAtPos(route_, routes->getRouteSize(route_)) > I.getHorizon() +1;	// add violation for the horizon length
	}
	n_violations = 0;
	w_violations = 0.0;
	for (int route_=1; route_ <= routes->getNumberRoutes(); route_++) {
		n_violations += n_violations_route[route_];
		w_violations += w_violations_route[route_];
	}

	// /* update COST */
	// for (int route_=1; route_ <= routes->getNumberRoutes(); route_++) {		// for each route
	// 	if (route != -42 && route_ != route) continue;
	// 	const VRP_vertex* prev_v = & routes->getVertexFromPos(route_,0);
	// 	cost_route[route_] = 0.0;
	// 	for (int pos=1; pos <= routes->getRouteSize(route_); pos++) {		// for each vertex in the route, including the ending depot vertex
	// 		const VRP_vertex* v = & routes->getVertexFromPos(route_,pos);
	// 		cost_route[route_] += this->I.travelCost(*prev_v, *v);
	// 		prev_v = v;
	// 	}
	// }
	// cost = 0.0;
	// for (int route_=1; route_ <= routes->getNumberRoutes(); route_++)
	// 	cost += cost_route[route_];

} 

// double Solution_VRPTW::getWeightViolations(int route) const {
// 	// ASSERT(route == -42, "not implemented yet " << route);
// 	double w_viol = 0.0;
// 	for (int route_=1; route_ <= routes->getNumberRoutes(); route_++) {		// for each route
// 		if (route != -42 && route_ != route) continue;
// 		int vehicle_load = 0;
// 		int factor = 1;
// 		for (int pos=1; pos <= routes->getRouteSize(route_); pos++) {		// for each vertex in the route, including the ending depot vertex
// 			const VRP_request& req = routes->getRequestFromPos(route_, pos);
// 			if (req.getVertex().getType() == VRP_vertex::REGULAR_ONLINE)
// 				factor = ((req.hasAppeared()*3) + 1);
// 			w_viol += factor * max(0.0, routes->getArrivalTimeAtPos(route_, pos) - req.l );
// 			vehicle_load += req.demand;
// 			// cout << "Factor: " << factor << "   routes->getArrivalTimeAtPos(route_, pos) - req.l " << routes->getArrivalTimeAtPos(route_, pos) - req.l  << endl;
// 			factor = 1;
// 		}
// 		if (I.getHorizon() < numeric_limits<int>::max())
// 			w_viol += factor * max(0.0, routes->getArrivalTimeAtPos(route_, routes->getRouteSize(route_)) - (I.getHorizon()+1) );	// add violation for the horizon length
// 		w_viol += factor * max(0.0, (double) vehicle_load - I.getVehicleCapacity());
// 		// cout << "route " << route_ << "   routes->getArrivalTimeAtPos(route_, routes->getRouteSize(route_)) = " << routes->getArrivalTimeAtPos(route_, routes->getRouteSize(route_)) << "   (I.getHorizon()+1) = " << (I.getHorizon()+1) << endl;
// 		// cout << "route tw viol = " << routes->getArrivalTimeAtPos(route_, routes->getRouteSize(route_)) - (I.getHorizon()+1) << endl;
// 		// cout << "route load viol = " << (double) vehicle_load - I.getVehicleCapacity() << endl;
// 	}

// 	return w_viol;
// }

bool Solution_VRPTW::isViolated(int route, int pos) const {
	return routes->getRequestFromPos(route, pos).l < routes->getArrivalTimeAtPos(route, pos);
}

void Solution_VRPTW::generateInitialSolution() {
	ASSERT(!initial_solution_generated, "");
	ASSERT(getNumberRoutes() > 0, "");

	for (const VRP_vertex* pvertex : I.getRegularVertices()) {
		SolutionElement pos;
		SolutionElement v; v.type = VERTEX; v.request = & pvertex->getRequest();
		selectRandomInsertionPosition(v, &pos);
		routes->insertVertex(pvertex, pos.route, pos.pos);
	}
	initial_solution_generated = true;
	solution_changed();
}


string& Solution_VRPTW::toString(bool verbose) const {
	ostringstream out; 
	out << routes->toString(verbose);
	if (getNumberViolations() > 0) 
		out << outputMisc::redExpr(true) << "INFEASIBLE !! (" << getNumberViolations() << ")" << outputMisc::resetColor() << endl;
	static string str = ""; str = out.str();
	return (str);
}






















/* ---------------------------------------------------------------------------------------- */
/* Solution_DS_VRPTW    ------------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------- */
void Solution_DS_VRPTW::init() {
	type = DS_VRPTW;

	in_solution = new bool*[I.getNumberRegions()+1];
	for (int i = 0; i < I.getNumberRegions()+1; ++i) {
		in_solution[i] = new bool[I.getNumberTimeSlots()+1];
		for (int ts = 0; ts < I.getNumberTimeSlots()+1; ts++)
			in_solution[i][ts] = false;
	}

	// for (const VRP_vertex* pvertex : I.get_regular_vertices())
	// 	revealed_at_region.insert({pvertex, false});

	// for (const VRP_vertex* pvertex : I.get_regular_vertices())
	// 	in_solution.insert({pvertex, false});

}
Solution_DS_VRPTW::Solution_DS_VRPTW(const VRP_instance& instance) : Solution_VRPTW(instance) {
	init();
}
Solution_DS_VRPTW::~Solution_DS_VRPTW() {
	for (int i = 0; i < I.getNumberRegions()+1; ++i)
		delete in_solution[i];
	delete in_solution;
}
Solution_DS_VRPTW::Solution_DS_VRPTW (const Solution_DS_VRPTW &sol): Solution_VRPTW(sol) {	// copy constructor
	init();
	// revealed_at_region = sol.revealed_at_region;
	// in_solution = sol.in_solution;
	for (int i = 0; i < I.getNumberRegions()+1; ++i)
		for (int ts = 0; ts < I.getNumberTimeSlots()+1; ts++)
			in_solution[i][ts] = sol.in_solution[i][ts];
	// copy(& sol.in_solution[0][0], & sol.in_solution[0][0] + (I.getNumberRegions()+1) * (I.getNumberTimeSlots()+1), &in_solution[0][0]);
}		
Solution_DS_VRPTW& Solution_DS_VRPTW::operator = (const Solution_DS_VRPTW& sol) {
	Solution_VRPTW::operator = (sol);
	// revealed_at_region = sol.revealed_at_region;
	// in_solution = sol.in_solution;
	for (int i = 0; i < I.getNumberRegions()+1; ++i)
		for (int ts = 0; ts < I.getNumberTimeSlots()+1; ts++)
			in_solution[i][ts] = sol.in_solution[i][ts];
	// copy(& sol.in_solution[0][0], & sol.in_solution[0][0] + (I.getNumberRegions()+1) * (I.getNumberTimeSlots()+1), &in_solution[0][0]);
	return *this;
}
void Solution_DS_VRPTW::copyRouteFrom(const Solution_DS_VRPTW& sol, SolutionElement route) {
	Solution_VRPTW::copyRouteFrom(sol, route);
	
	for (int i = 0; i < I.getNumberRegions()+1; ++i)
		for (int ts = 0; ts < I.getNumberTimeSlots()+1; ts++)
			in_solution[i][ts] = sol.in_solution[i][ts];
	// copy(& sol.in_solution[0][0], & sol.in_solution[0][0] + (I.getNumberRegions()+1) * (I.getNumberTimeSlots()+1), &in_solution[0][0]);
}

void Solution_DS_VRPTW::generateInitialSolution() {
	ASSERT(!initial_solution_generated, "");
	ASSERT(getNumberRoutes() > 0, "");

	for (const VRP_vertex* pvertex : I.getRegularVertices()) {
		// if (revealed_at_region[pvertex]) {
			SolutionElement pos;
			SolutionElement v; v.type = VERTEX; v.request = & pvertex->getRequest();
			selectRandomInsertionPosition(v, &pos);
			routes->insertVertex(pvertex, pos.route, pos.pos);
			// ASSERT(in_solution.count(pvertex) == 1, "argh");
			// in_solution[pvertex] = true;
		// }
	}

	for (const VRP_vertex* pvertex : I.getOnlineVertices()) {
		if (pvertex->getRequest(0).hasAppeared()) {
			SolutionElement pos;
			SolutionElement v; v.type = VERTEX; v.request = & pvertex->getRequest(0);
			selectRandomInsertionPosition(v, &pos);
			routes->insertOnlineVertex(pvertex, pos.route, pos.pos, 0);
			in_solution[pvertex->getRegion()][0] = true;
		}
	}

	initial_solution_generated = true;
	solution_changed();
}


int Solution_DS_VRPTW::getNumberUnvisitedWaitingVertices() const {
	return numeric_limits<int>::max();
}

void Solution_DS_VRPTW::selectRandomWaitingLocation(SolutionElement* wl) const {
	ASSERT(I.getWaitingVertices().size(), "");
	int x = rand() % I.getWaitingVertices().size();
	int i = 0;
	const VRP_vertex* pvertex = NULL;
	for (const VRP_vertex* pv : I.getWaitingVertices()) {
		if (i == x) {
			pvertex = pv; 
			break;
		} i++;
	}
	wl->type = WAITING_LOC;
	wl->region = pvertex->getRegion();
}

void Solution_DS_VRPTW::getWaitingLocationFromRegion(SolutionElement* wl, int region) const {
	ASSERT(region >= 0, "");
	wl->type = WAITING_LOC;
	wl->region = region;
}

void Solution_DS_VRPTW::insertWaitingVertex(const SolutionElement& w_loc, const SolutionElement& pos, double waiting_time) {
	ASSERT(w_loc.type == WAITING_LOC, "");
	ASSERT(pos.type == POSITION, "");

	routes->insertVertex(I.getVertexFromRegion(VRP_vertex::WAITING, w_loc.region), pos.route, pos.pos);				
	routes->setWaitingTime(pos.route, pos.pos, waiting_time);	
	solution_changed(pos.route);
}

void Solution_DS_VRPTW::insertWaitingVertex(const VRP_vertex& v, int route, int pos, double waiting_time) {
	routes->insertVertex(&v, route, pos);				
	routes->setWaitingTime(route, pos, waiting_time);
	solution_changed(route);
}


double Solution_DS_VRPTW::removeWaitingVertex(const SolutionElement& w_vertex) {
	ASSERT(w_vertex.type == VERTEX, "");
	ASSERT(routes->getVertexFromPos(w_vertex.route, w_vertex.pos).getType() == VRP_vertex::WAITING, "");
	double wtime = routes->getWaitingTime(w_vertex.route, w_vertex.pos);

	routes->removeVertex(w_vertex.route, w_vertex.pos);
	solution_changed(w_vertex.route);
	return wtime;
}
double Solution_DS_VRPTW::removeWaitingVertex(int route, int pos) {
	ASSERT(routes->getVertexFromPos(route, pos).getType() == VRP_vertex::WAITING, "");
	double wtime = routes->getWaitingTime(route, pos);

	routes->removeVertex(route, pos);
	solution_changed(route);
	return wtime;
}


double Solution_DS_VRPTW::increaseWaitingTime(const SolutionElement& w_vertex, double increment) {
	ASSERT(w_vertex.type == VERTEX, "");
	return increaseWaitingTime(w_vertex.route, w_vertex.pos, increment);
}
double Solution_DS_VRPTW::increaseWaitingTime(int route, int pos, double increment) {
	// const VRP_vertex& pvertex = routes->getVertexFromPos(route, pos);
	ASSERT(routes->getVertexFromPos(route, pos).getType() == VRP_vertex::WAITING || routes->getVertexFromPos(route, pos).getType() == VRP_vertex::REGULAR, "");

	double new_wtime = min((double)I.getHorizon(), max((double)1.0, routes->getWaitingTime(route, pos) + increment));
	
	if ((int)new_wtime % (int)waiting_time_multiple)
		new_wtime += waiting_time_multiple - (int)new_wtime % (int)waiting_time_multiple;	// new_wtime is set to the nearest (up) multiple of waiting_time_multiple
	ASSERT((int) new_wtime % (int)waiting_time_multiple == 0, "argh");

	double actual_increment = new_wtime - routes->getWaitingTime(route, pos);

	routes->setWaitingTime(route, pos, (int) new_wtime );
	solution_changed(route);
	return actual_increment;
}
double Solution_DS_VRPTW::decreaseWaitingTime(const SolutionElement& w_vertex, double decrement) {
	double actual_decrement = increaseWaitingTime(w_vertex, -decrement);	
	return -actual_decrement;
}

void Solution_DS_VRPTW::setWaitingTime(const SolutionElement& w_vertex, double w_time) {
	// ASSERT( (int) w_time % I.getWaitingTimeMultiple() == 0, "argh");
	double increment = w_time - routes->getWaitingTime(w_vertex.route, w_vertex.pos);
	increaseWaitingTime(w_vertex, increment);
}
double Solution_DS_VRPTW::decreaseWaitingTime(int route, int pos, double decrement) {
	double actual_decrement = increaseWaitingTime(route, pos, -decrement);	
	return -actual_decrement;
}


void Solution_DS_VRPTW::setWaitingTime(int route, int pos, double w_time) {
	// ASSERT( (int) w_time % I.getWaitingTimeMultiple() == 0, "argh");
	double increment = w_time - routes->getWaitingTime(route, pos);
	increaseWaitingTime(route, pos, increment);
}

void Solution_DS_VRPTW::setWaitingTimeMultiple(double multiple) {
	ASSERT(multiple > 0 && multiple < I.getHorizon(), "argh ! multiple = " << multiple);
	ASSERT(multiple == floor(multiple), "floating point numbers are not supported");
	// ASSERT(waiting_time_multiple == 1, "waiting_time_multiple has already been set. Not possible to change its value again (not implemented yet)");
	ASSERT( !initial_solution_generated || (multiple <= waiting_time_multiple && (int)waiting_time_multiple % (int)multiple == 0), "waiting_time_multiple has already been set. Not possible to change its value again (not implemented yet)");
	waiting_time_multiple = multiple;
}


void Solution_DS_VRPTW::setCurrentTime(double t) {
	ASSERT(t >= 0, "");
	routes->setCurrentTime(t);
}
void Solution_DS_VRPTW::setWaitingStrategy(WaitingStrategy ws) {
	routes->setWaitingStrategy(ws);
}

VehicleAction Solution_DS_VRPTW::getCurrentActionsVeh(int vehicle, int time) const {
	VehicleAction a;
	a.vehicle = vehicle;
	routes->getCurrentAction(vehicle, & a.pos, & a.action, & a.start_time, & a.end_time, time);
	a.pvertex = & routes->getVertexFromPos(vehicle, a.pos);
	return a;
}

VehicleAction Solution_DS_VRPTW::getNextPlannedActionsVeh(int vehicle, int time) const {
	VehicleAction a;
	a.vehicle = vehicle;
	routes->getNextPlannedAction(vehicle, & a.pos, & a.action, & a.start_time, & a.end_time, time);
	a.pvertex = & routes->getVertexFromPos(vehicle, a.pos);
	return a;
}

const vector<VehicleAction>& Solution_DS_VRPTW::getCurrentActions(int time) {
	current_actions.clear();
	current_actions.resize(getNumberRoutes());
	for (int k = 1; k <= getNumberRoutes(); k++) {
		current_actions[k-1].vehicle = k;
		routes->getCurrentAction(k, & current_actions[k-1].pos, & current_actions[k-1].action, & current_actions[k-1].start_time, & current_actions[k-1].end_time, time);
		current_actions[k-1].pvertex = & routes->getVertexFromPos(k, current_actions[k-1].pos);
	}
	return current_actions;
}
const vector<VehicleAction>& Solution_DS_VRPTW::getNextPlannedActions(int time) {
	next_planned_actions.clear();
	next_planned_actions.resize(getNumberRoutes());
	for (int k = 1; k <= getNumberRoutes(); k++) {
		next_planned_actions[k-1].vehicle = k;
		routes->getNextPlannedAction(k, & next_planned_actions[k-1].pos, & next_planned_actions[k-1].action, & next_planned_actions[k-1].start_time, & next_planned_actions[k-1].end_time, time);
		next_planned_actions[k-1].pvertex = & routes->getVertexFromPos(k, next_planned_actions[k-1].pos);
	}
	return next_planned_actions;
}

bool Solution_DS_VRPTW::insertFeasible(const VRP_vertex& v, SolutionElement *pos_e, int timeslot, bool pick_first_feasible) {
	ASSERT(v.getType() == VRP_vertex::REGULAR_ONLINE, "online request should be a REGULAR_ONLINE type vertex");

	WaitingStrategy ws = routes->getWaitingStrategy();
	routes->setWaitingStrategy(DRIVE_FIRST);

	double best_cost = numeric_limits<double>::max(), best_violations = numeric_limits<double>::max();
	SolutionElement best_pos;
	best_pos.type = POSITION;

	bool found = false;
	for (int k = 1; k <= routes->getNumberRoutes(); k++) {
		if (routes->getFirstFreePos(k) < 0) continue;

		for (int pos = routes->getFirstFreePos(k); pos <= routes->getRouteSize(k); pos++) {

			// Stop the search in this route if no feasible place further
			const VRP_vertex& curr_v = routes->getVertexFromPos(k, pos);
			const VRP_request& req = v.getRequest(timeslot);
			const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(k);

			if (curr_v.getType() != VRP_vertex::DEPOT && curr_v.getType() != VRP_vertex::WAITING)
				if (routes->getDepartureTimeAtPos(k, pos-1) + veh.travelTime(routes->getVertexFromPos(k, pos-1), v) > req.l)
					break;

			double original_wtime = -1;
			if (routes->getVertexFromPos(k, pos-1).getType() == VRP_vertex::WAITING) 
				original_wtime = routes->removeMaxWaitingTime(k, pos -1);

			routes->insertOnlineVertex(&v, k, pos, timeslot);
			solution_changed(k);

			// cout << getNumberViolations() << endl;
			if (getNumberViolations() < best_violations) {
				best_violations = getNumberViolations();
				best_pos.route = k;
				best_pos.pos = pos;
			}
			if (getNumberViolations() == 0) {
				best_violations = 0;
				found = true;
				if (getCost() < best_cost) {
					best_cost = getCost();
					best_pos.route = k;
					best_pos.pos = pos;
				}
			} 
			routes->removeVertex(k, pos);
			if (routes->getVertexFromPos(k, pos-1).getType() == VRP_vertex::WAITING) 
				routes->setWaitingTime(k, pos -1, original_wtime);
			solution_changed(k);
			if (found && pick_first_feasible) break;
		}
		if (found && pick_first_feasible) break;
	}
	routes->setWaitingStrategy(ws);

	// ASSERT(best_pos.route > 0, "no insertion position found ! (not even an infeasible one... everything's fixed ?!)");
	*pos_e = best_pos;
	return found;
}

bool Solution_DS_VRPTW::insertOnlineRequest(const VRP_request& req) {
	return insertOnlineRequest(req.getVertex(), req.revealTS);
}
bool Solution_DS_VRPTW::insertOnlineRequest(const VRP_vertex& v, int timeslot) {
	SolutionElement pos;
	bool feasible = insertFeasible(v, &pos, timeslot);
	if (!feasible)
		selectRandomInsertionPosition(&pos);
	insertOnlineRequest(v, pos, timeslot);
	return feasible;
}				
void Solution_DS_VRPTW::insertOnlineRequest(const VRP_vertex& v, SolutionElement pos, int timeslot) {
	ASSERT(! in_solution[v.getRegion()][timeslot], "already in the solution !");
	ASSERT(initial_solution_generated, "argh");
	ASSERT(v.getType() == VRP_vertex::REGULAR_ONLINE, "online request should be an online regular vertex");
	ASSERT(pos.type == POSITION, "");
	WaitingStrategy ws = routes->getWaitingStrategy();
	routes->setWaitingStrategy(DRIVE_FIRST);
	routes->insertOnlineVertex(&v, pos.route, pos.pos, timeslot);
	solution_changed(pos.route);
	// routes->setCurrentTime(routes->getCurrentTime());	===>	what was that for ?!..
	if (getNumberViolations() > 0 && routes->getVertexFromPos(pos.route, pos.pos -1).getType() == VRP_vertex::WAITING) {	// if inserting turns the solution infeasible, then it may be because we wanted to insert there whilst removing waiting time at the previous vertex
		// double time_loss_by_inserting = I.travelTime(routes->getVertexFromPos(pos.route, pos.pos-1), v) + v.getRequest().duration + I.travelTime(v, routes->getVertexFromPos(pos.route, pos.pos)) - I.travelTime(routes->getVertexFromPos(pos.route, pos.pos-1), routes->getVertexFromPos(pos.route, pos.pos));
		// routes->removeMaxWaitingTime(pos.route, pos.pos -1, time_loss_by_inserting + 0.0001);
		routes->removeMaxWaitingTime(pos.route, pos.pos -1);
	}
	routes->setWaitingStrategy(ws);
	
	// ASSERT(in_solution.count(&v) == 1, "argh");
	// ASSERT(in_solution[&v] == false, "request at vertex " << v.toString() << " is already in solution !");
	// in_solution[&v] = true;
	in_solution[v.getRegion()][timeslot] = true;
}


bool Solution_DS_VRPTW::tryInsertOnlineRequest(const VRP_vertex& v, int timeslot, bool pick_first_feasible) {
	ASSERT(v.getType() == VRP_vertex::REGULAR_ONLINE, "online request should be an online regular vertex");

	// ASSERT(revealed_at_region.count(&v) == 1, "argh");
	// ASSERT(revealed_at_region[&v] == false, "request at vertex " << v.toString() << " was already revealed !");

	// ASSERT(in_solution.count(&v) == 1, "argh");
	// ASSERT(in_solution[&v][timeslot] == false, "request at vertex " << v.toString() << " is already in solution !");
	if (false == in_solution[v.getRegion()][timeslot]) {
		// revealed_at_region[&v] = true;
		SolutionElement pos;
		
		if(insertFeasible(v, &pos, timeslot, pick_first_feasible))
			insertOnlineRequest(v, pos, timeslot);
		else
			return false;
		
	}
	return true;
}
bool Solution_DS_VRPTW::tryInsertOnlineRequest(int region, int timeslot, bool pick_first_feasible) {
	const VRP_vertex &v = *I.getVertexFromRegion(VRP_vertex::REGULAR_ONLINE, region);
	return tryInsertOnlineRequest(v, timeslot, pick_first_feasible);
}
bool Solution_DS_VRPTW::tryInsertOnlineRequest(const VRP_request& req, bool pick_first_feasible) {
	return tryInsertOnlineRequest(req.getVertex().getRegion(), req.revealTS, pick_first_feasible);
}
// void Solution_DS_VRPTW::cancelRevealedRequest(const VRP_vertex& v, bool try_remove) {
// 	ASSERT(v.getType() == VRP_vertex::REGULAR, "online request should be a regular vertex");

// 	ASSERT(revealed_at_region.count(&v) == 1, "argh");
// 	ASSERT(revealed_at_region[&v] == true, "request at vertex " << v.toString() << " wasn't revealed !");

// 	ASSERT(in_solution.count(&v) == 1, "argh");
// 	ASSERT(!try_remove || in_solution[&v] == true, "request at vertex " << v.toString() << " wasn't in solution !");

// 	revealed_at_region[&v] = false;
// 	if (try_remove) {
// 		Solution_DS_VRPTW::VertexAttr attr = getCurrentAttributes(v);
// 		if (! attr.fixed) {
// 			routes->removeVertex(attr.route, attr.pos);
// 			in_solution[&v] = false;
// 		}
// 	}
// }

void Solution_DS_VRPTW::removeSampledOnlineRequest(int route, int pos) {
	const VRP_vertex& v = routes->getVertexFromPos(route, pos);
	int timeslot = routes->getRequestFromPos(route, pos).revealTS;
	ASSERT(v.getType() == VRP_vertex::REGULAR_ONLINE, "argh");

	ASSERT(routes->getRequestFromPos(route, pos).hasAppeared() == false, "route: " << route << ", pos: " << pos << endl << toString());

	// cout << "removing sampled request " << routes->getRequestFromPos(route, pos).toString(true) << endl;
	routes->removeVertex(route, pos);
	in_solution[v.getRegion()][timeslot] = false;
}
void Solution_DS_VRPTW::removeAllSampledOnlineRequests() {
	for (int route = 1; route <= getNumberRoutes(); route++) {
		for (int pos = getRouteSize(route)-1; pos > 0; pos--) {
			const VRP_vertex& v = routes->getVertexFromPos(route, pos);
			if (v.getType() == VRP_vertex::REGULAR_ONLINE && routes->getRequestFromPos(route, pos).hasAppeared() == false && !isPositionFixed(route, pos))
				removeSampledOnlineRequest(route, pos);
		}
	}
	solution_changed();
}

void Solution_DS_VRPTW::removeAllSampled_violated_OnlineRequests() {
	for (int route = 1; route <= getNumberRoutes(); route++)
		for (int pos = getRouteSize(route); pos > 0; pos--)
			if (getVertexAtPosition(route, pos).getType() == VRP_vertex::REGULAR_ONLINE && !isPositionFixed(route, pos) && !getRequestAtPosition(route, pos).hasAppeared() && isViolated(route, pos))
				removeSampledOnlineRequest(route, pos);
	solution_changed();
}

// int Solution_DS_VRPTW::getNumberRevealedRequests() const {
// 	int n = 0;
// 	for (auto& x: revealed_at_region)
// 		n += x.second;
// 	return n;
// }
// int Solution_DS_VRPTW::getNumberVisitedVertices(VRP_vertex::VertexType type) const {
// 	int n = 0;
// 	switch(type) {

// 		case VRP_vertex::REGULAR:
// 			for (auto& x: in_solution)
// 				n += x.second;
// 			return n;

// 		case VRP_vertex::WAITING:
// 			_ASSERT_(false, "not implemented for vertex type" << type);
// 			break;

// 		case VRP_vertex::DEPOT:
// 			_ASSERT_(false, "not implemented for vertex type" << type);
// 			break;

// 		default: _ASSERT_(false, "wrong type " << type);
// 	}

// }
// bool Solution_DS_VRPTW::visits(const VRP_vertex& v) {
// 	ASSERT(in_solution.count(&v) == 1, "argh");
// 	return in_solution[&v];
// }






















// /* ---------------------------------------------------------------------------------------- */
// /* Solution_DS_VRPTW_GSA    --------------------------------------------------------------- */
// /* ---------------------------------------------------------------------------------------- */

// Solution_DS_VRPTW::Solution_DS_VRPTW_GSA(const VRP_instance& instance, const VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR,Solution_DS_VRPTW_GSA>& scenario_pool) : Solution_DS_VRPTW(instance) {
// 	this->scenario_pool = &scenario_pool;
// }

// double ::getCost(int route = -42) const {
// 	ASSERT(route == -42, "");
// 	Solution_DS_VRPTW s_copy(s);
// 	int nb_rejects_total = 0;
// 	for (const Scenario_SS_DS_VRPTW_CR& scenario : scenario_pool->getScenarios()) {
// 		for (const VRP_instance::RequestAttributes& req_attr : scenario.getRequestSequence()) {
// 			s_copy.insertOnlineRequest(*req_attr.vertex, req_attr.time_slot);
// 		}
// 	}
// 	return (double) nb_rejects_total / (double) scenario_pool->getScenarios().size();
// }


















/* ---------------------------------------------------------------------------------------- */
/* Solution_SS_VRPTW_CR    ---------------------------------------------------------------- */
/* ---------------------------------------------------------------------------------------- */

void Solution_SS_VRPTW_CR::init(RecourseStrategy strategy, double initialWaitingTime, double waiting_time_multiple) {
	if (initialWaitingTime == -42)
		// this->initialWaitingTime = I.getWaitingTimeMultiple();
		this->initialWaitingTime = 1;
	else 
		this->initialWaitingTime = initialWaitingTime;
	this->recourseStrategy = strategy;

	/* Based on the instance, order all the potential requests of positive propability  --> fill requests_ordered */
	for (const VRP_vertex* pvertex : I.getCustomerVertices()) {
		for (int ts=0; ts <= I.getNumberTimeSlots(); ts++) {
			const VRP_request& req = pvertex->getRequest(ts);
			if (req.p > 0)
				requests_ordered.insert(req);
		}
	}

	expectedCost = numeric_limits<double>::max();

	// initialize the set of unvisited waiting vertices
	for (const VRP_vertex* pvertex : I.getWaitingVertices())
		unvisitedWaitingVertices.insert(pvertex);

	// allocate MIP solution variables
	mipsol.x = new double**[I.getNumberRegions()];
	for (int i = 0; i < I.getNumberRegions(); i++) {
		mipsol.x[i] = new double*[I.getNumberRegions()];
		for (int j = 0; j < I.getNumberRegions(); j++) {
			mipsol.x[i][j] = new double[getNumberRoutes()+1];
			for (int p = 0; p < getNumberRoutes()+1; p++) 
				mipsol.x[i][j][p] = 0.0;
		}
	}
	mipsol.tau = new double**[I.getNumberRegions()];
	for (int i = 0; i < I.getNumberRegions(); i++) {
		mipsol.tau[i] = new double*[I.getHorizon()+1];
		for (int l = 0; l < I.getHorizon()+1; l++) {
			mipsol.tau[i][l] = new double[getNumberRoutes()+1];
			for (int p = 0; p < getNumberRoutes()+1; p++) 
				mipsol.tau[i][l][p] = 0.0;
		}
	}    
	mipsol.y = new double*[I.getNumberRegions()];
	for (int i = 0; i < I.getNumberRegions(); i++) {
		mipsol.y[i] = new double[getNumberRoutes()+1];
		for (int p = 0; p < getNumberRoutes()+1; p++) 
			mipsol.y[i][p] = 0.0;
	} 
	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;

	this->waiting_time_multiple = waiting_time_multiple;		// default for SS-VRPTW-CR
}

Solution_SS_VRPTW_CR::Solution_SS_VRPTW_CR(const VRP_instance& instance, RecourseStrategy strategy, double initialWaitingTime, double waiting_time_multiple) : Solution_DS_VRPTW(instance) {
	// ASSERT(strategy != NOT_SET, "Recourse strategy not set !");
	init(strategy, initialWaitingTime, waiting_time_multiple);

	type = SS_VRPTW_CR;
}

Solution_SS_VRPTW_CR::Solution_SS_VRPTW_CR(const Solution_SS_VRPTW_CR& sol) : Solution_DS_VRPTW(sol.I) {	// copy constructor
	init(sol.recourseStrategy, sol.initialWaitingTime, sol.waiting_time_multiple);
	*this = sol;
}

Solution_SS_VRPTW_CR& Solution_SS_VRPTW_CR::operator = (const Solution_SS_VRPTW_CR& sol) {
	Solution_DS_VRPTW::operator = (sol);

	expectedCost = sol.expectedCost;
	initialWaitingTime = sol.initialWaitingTime;
	recourseStrategy = sol.recourseStrategy;

	assignment_done = sol.assignment_done;
	requests_ordered = sol.requests_ordered;
	requests_ordered_per_waiting_locations = sol.requests_ordered_per_waiting_locations;
	unassigned_requests = sol.unassigned_requests;

	unvisitedWaitingVertices = sol.unvisitedWaitingVertices;

	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;

	return *this;
}

void Solution_SS_VRPTW_CR::copyRouteFrom(const Solution_SS_VRPTW_CR& sol, SolutionElement route) {
	VRP_solution::copyRouteFrom(sol, route);

	expectedCost = sol.expectedCost;			// we assume that the current sol must be exactly the same as sol
	initialWaitingTime = sol.initialWaitingTime;
	recourseStrategy = sol.recourseStrategy;

	assignment_done = sol.assignment_done;
	requests_ordered = sol.requests_ordered;
	requests_ordered_per_waiting_locations = sol.requests_ordered_per_waiting_locations;
	unassigned_requests = sol.unassigned_requests;

	unvisitedWaitingVertices = sol.unvisitedWaitingVertices;

	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;
}


// void Solution_SS_VRPTW_CR::initializeFromSol_scaleWaitingTimes(const Solution_SS_VRPTW_CR& sol, double scale) {
// 	// cout << "pouet" << endl;
// 	VRP_solution::initializeFromSol_scaleWaitingTimes(sol, scale);
// 	unvisitedWaitingVertices = sol.unvisitedWaitingVertices;
// 	solution_changed();
// 	initial_solution_generated = true;
// }

void Solution_SS_VRPTW_CR::copyFromSol_scaleWaitingTimes(const Solution_SS_VRPTW_CR& sol, bool try_preserve_feasibility) {
	// cout << "pouet" << endl;
	VRP_solution::copyFromSol_scaleWaitingTimes(sol, try_preserve_feasibility);
	unvisitedWaitingVertices = sol.unvisitedWaitingVertices;
	solution_changed();
	// initial_solution_generated = true;
}

void Solution_SS_VRPTW_CR::solution_changed(int route) {
	// ASSERT(route == -42, "not implemented yet " << route); UNUSED(route);
	UNUSED(route);

	assignment_done = false;
	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;

	if (initial_solution_generated && getNumberViolations() > 0) {
		expectedCost = numeric_limits<double>::max();
		return;
	}

	updateRequestAssignmentOrdering();
	updateExpectedCost();
} 

void Solution_SS_VRPTW_CR::setRecourseStrategy(RecourseStrategy recourseStrategy) {
	this->recourseStrategy = recourseStrategy;
	assignment_done = false;
	updateRequestAssignmentOrdering();
	updateExpectedCost();
}

void Solution_SS_VRPTW_CR::clearAllRoutes() {
	for (int k=1; k <= getNumberRoutes(); k++)
		for (int pos=1; pos < routes->getRouteSize(k); pos++)
			unvisitedWaitingVertices.insert(& routes->getVertexFromPos(k, pos));
	VRP_solution::clearAllRoutes();
}

void Solution_SS_VRPTW_CR::mipsol_setX(int i, int j, const double *vals) {
	ASSERT(getNumberRoutes() > 0, "");
	memcpy(mipsol.x[i][j], vals, (getNumberRoutes()+1)*sizeof(double));    
	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;
}
void Solution_SS_VRPTW_CR::mipsol_setTAU(int i, int l, const double *vals) {
	ASSERT(getNumberRoutes() > 0, "");
	memcpy(mipsol.tau[i][l], vals, (getNumberRoutes()+1)*sizeof(double));    
	mipsol.uptodate_waiting = false;
}
void Solution_SS_VRPTW_CR::mipsol_setY(int i, const double *vals) {
	ASSERT(getNumberRoutes() > 0, "");
	memcpy(mipsol.y[i], vals, (getNumberRoutes()+1)*sizeof(double));    
	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;
}


void Solution_SS_VRPTW_CR::mipsol_setX(int i, int j, int p, double val) {
	mipsol.x[i][j][p] = val;
	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;
}
void Solution_SS_VRPTW_CR::mipsol_setTAU(int i, int l, int p, double val) {
	mipsol.tau[i][l][p] = val;
	mipsol.uptodate_waiting = false;
}
void Solution_SS_VRPTW_CR::mipsol_setY(int i, int p, double val) {
	mipsol.y[i][p] = val;
	mipsol.uptodate_routes = false;
	mipsol.uptodate_waiting = false;
}

const vector<vector<Solution_SS_VRPTW_CR::MIPSol_Arc>> &  Solution_SS_VRPTW_CR::mipsol_getSubtours() const {
	ASSERT(mipsol.uptodate_routes, "you must call update_routes_from_mipsol() first !");
	return mipsol.subtours;
}
const vector<Solution_SS_VRPTW_CR::MIPSol_Arc> & Solution_SS_VRPTW_CR::mipsol_getArcs() const {
	ASSERT(mipsol.uptodate_routes, "you must call update_routes_from_mipsol first !");
	return mipsol.arcs;
}
const vector<Solution_SS_VRPTW_CR::MIPSol_WaitTime> & Solution_SS_VRPTW_CR::mipsol_getWaitTimes() const {
	ASSERT(mipsol.uptodate_waiting, "you must call update_waitingTimes_from_mipsol first !");
	return mipsol.wait_times;
}
void Solution_SS_VRPTW_CR::update_from_mipsol() {
	update_routes_from_mipsol();
	update_waitingTimes_from_mipsol();
}
void Solution_SS_VRPTW_CR::update_routes_from_mipsol() {
	mipsol.arcs.clear();

	ASSERT(getNumberRoutes() > 0, "");
	clearAllRoutes();


	// FIND ROUTES FROM MIPSOL
	for (int k=1; k <= getNumberRoutes(); k++) {
		const VRP_vertex* curr_pvertex = & routes->getVertexFromPos(k, 0);
		do {
			for (const VRP_vertex* pv : unvisitedWaitingVertices) {
				int i = pv->getRegion();
				if (mipsol.x[curr_pvertex->getRegion()][i][k] > 0.5) {
					mipsol.arcs.push_back(MIPSol_Arc(curr_pvertex->getRegion(), i, k));
					
					curr_pvertex = I.getVertexFromRegion(VRP_vertex::WAITING, i);
					routes->insertVertexLastPos(curr_pvertex, k);
					
					unvisitedWaitingVertices.erase(curr_pvertex);
					break;
				}
			}
			if (mipsol.x[curr_pvertex->getRegion()][0][k] > 0.5) {
				mipsol.arcs.push_back(MIPSol_Arc(curr_pvertex->getRegion(), 0, k));
				break;
			}
		} while (curr_pvertex->getType() != VRP_vertex::DEPOT);
	}

	// DETECT SUBTOURS
	mipsol.subtours.clear();
	vector<const VRP_vertex*> vertices;

	while (unvisitedWaitingVertices.size() > 0) {
		const VRP_vertex* start_v = *unvisitedWaitingVertices.begin();
		unvisitedWaitingVertices.erase(start_v);
		vertices.push_back(start_v);

		const VRP_vertex* subtour_curr_v = start_v;	
		int start_v_route = -1;
		bool found = false;
		for (const VRP_vertex* pv : unvisitedWaitingVertices) {
			for (int k=1; k <= getNumberRoutes(); k++) {
				if (mipsol.x[start_v->getRegion()][pv->getRegion()][k] > 0.5) {
					mipsol.subtours.push_back(vector<MIPSol_Arc>());
					mipsol.subtours[mipsol.subtours.size()-1].push_back(MIPSol_Arc(start_v->getRegion(), pv->getRegion()));
					start_v_route = k;
					subtour_curr_v = pv;

					unvisitedWaitingVertices.erase(pv);
					vertices.push_back(pv);
					found = true;
				}
				if (found) break;
			}
			if (found) break;
		}

		
		while (subtour_curr_v != start_v) {	// if start_v is indeed the starting point of a subtour
			if (mipsol.x[subtour_curr_v->getRegion()][start_v->getRegion()][start_v_route] > 0.5) {
				mipsol.subtours[mipsol.subtours.size()-1].push_back(MIPSol_Arc(subtour_curr_v->getRegion(), start_v->getRegion()));
				break;
			}
			found = false;
			for (const VRP_vertex* pv : unvisitedWaitingVertices) {
				int k = start_v_route;
				if (mipsol.x[subtour_curr_v->getRegion()][pv->getRegion()][k] > 0.5) {
					mipsol.subtours[mipsol.subtours.size()-1].push_back(MIPSol_Arc(subtour_curr_v->getRegion(), pv->getRegion()));
					subtour_curr_v = pv;

					unvisitedWaitingVertices.erase(pv);
					vertices.push_back(pv);
					found = true;
				}
				if (found) break;
			}
		}
	}
	

	for (auto v : vertices)	// put the subtour waiting vertices back in the set
		unvisitedWaitingVertices.insert(v);

	assignment_done = false;
	mipsol.uptodate_routes = true;
}
void Solution_SS_VRPTW_CR::update_waitingTimes_from_mipsol() {
	ASSERT(mipsol.uptodate_routes, "");

	mipsol.wait_times.clear();

	for (int k = 1; k <= routes->getNumberRoutes(); k++) {
		for (int pos = 1; pos < routes->getRouteSize(k); pos++) {
			auto & vertex = routes->getVertexFromPos(k, pos);
			int l;
			for (l = 1; l <= I.getHorizon(); l++) {
				if (mipsol.tau[vertex.getRegion()][l][k] > 0.5) {
					mipsol.wait_times.push_back(MIPSol_WaitTime(vertex.getRegion(), l, k));
					routes->setWaitingTime(k, pos, (double) l);
					break;
				}
			} ASSERT(l <= I.getHorizon(), "");
		}
	}

	mipsol.uptodate_waiting = true;
	assignment_done = false;
	updateRequestAssignmentOrdering();
	updateExpectedCost();
}


double Solution_SS_VRPTW_CR::getCost(int route) const {
	ASSERT(initial_solution_generated, "");
	ASSERT(route == -42, "not implemented yet " << route); UNUSED(route);
	if (getNumberViolations() > 0)
		return numeric_limits<double>::max();
	return expectedCost;
}

// only violations are when the vehicle returns too late to the depot !
int Solution_SS_VRPTW_CR::getNumberViolations(int route) const {
	ASSERT(initial_solution_generated, "");
	ASSERT(route == -42, "not implemented yet " << route); UNUSED(route);
	int nviol = 0;

	for (int route=1; route <= routes->getNumberRoutes(); route++) {
		nviol += routes->getArrivalTimeAtPos(route, routes->getRouteSize(route)) > I.getHorizon();
	}

	return nviol;
}

// only violations are when the vehicle returns too late to the depot !
double Solution_SS_VRPTW_CR::getWeightViolations(int route) const {
	ASSERT(initial_solution_generated, "");
	ASSERT(route == -42, "not implemented yet " << route); UNUSED(route);
	double weight_viol = 0;

	for (int route=1; route <= routes->getNumberRoutes(); route++) {
		weight_viol += max(0.0, routes->getArrivalTimeAtPos(route, routes->getRouteSize(route)) - I.getHorizon() );
	}

	return weight_viol;
}

void Solution_SS_VRPTW_CR::print_OrderedSet_requests() const {
	for(auto req : requests_ordered) {		
		SolutionElement w; w.type = VERTEX;
		cout << req.toString(true) <<  "feasible locations:   ";
		for (int route=1; route <= routes->getNumberRoutes(); route++) {		// 
			for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {		// -1 because we exclude the ending depot
				w.route = route, w.pos = pos;
				if (t_min(req, w) <= t_max(req, w))
					cout << w.toString() << "  ";
			}
		}
		cout << endl;
	}
}


void Solution_SS_VRPTW_CR::generateInitialSolution(bool empty) {
	ASSERT(!initial_solution_generated, "");
	ASSERT(getNumberRoutes() > 0, "");
	ASSERT(unvisitedWaitingVertices.size(), "");

	if (!empty) {
		while(unvisitedWaitingVertices.size()) {
			SolutionElement pos, w_loc;
			selectRandomWaitingLocation(&w_loc);
			selectRandomInsertionPosition(&pos);
			insertWaitingVertex(w_loc, pos);
		}
	}

	solution_changed();
	initial_solution_generated = true;
}


int Solution_SS_VRPTW_CR::getNumberUnvisitedWaitingVertices() const {
	return unvisitedWaitingVertices.size();
}

void Solution_SS_VRPTW_CR::selectRandomWaitingLocation(SolutionElement* wl) const {
	ASSERT(unvisitedWaitingVertices.size(), "");

	int x = rand() % unvisitedWaitingVertices.size();
	int i = 0;
	const VRP_vertex* pvertex = NULL;
	for (const VRP_vertex* pv : unvisitedWaitingVertices) {
		if (i == x) {
			pvertex = pv; 
			break;
		} i++;
	}

	wl->type = WAITING_LOC;
	wl->region = pvertex->getRegion();
}
void Solution_SS_VRPTW_CR::getWaitingLocationFromRegion(SolutionElement* wl, int region) const {
	ASSERT(region >= 0, "");
	const VRP_vertex* pvertex = NULL;
	for (const VRP_vertex* pv : unvisitedWaitingVertices)
		if (pv->getRegion() == region) { pvertex = pv; break;}
	ASSERT(pvertex, ""); UNUSED(pvertex);
	
	Solution_DS_VRPTW::getWaitingLocationFromRegion(wl, region);
}

void Solution_SS_VRPTW_CR::insertWaitingVertex(const SolutionElement& w_loc, const SolutionElement& pos) {
	ASSERT(w_loc.type == WAITING_LOC, "");
	ASSERT(pos.type == POSITION, "");
	if (w_loc.region == 0) {
		for (auto pvertex : unvisitedWaitingVertices)
			cout << pvertex->toString() << "  ";
		_ASSERT_(false, "argh");
	}

	const VRP_vertex* pvertex = I.getVertexFromRegion(VRP_vertex::WAITING, w_loc.region);
	bool ok = false;
	for (auto pv : unvisitedWaitingVertices) 
		if (pv->getRegion() == w_loc.region) {
			ok = true;
			break;
		}
	ASSERT(ok, "not found ! sol:" << endl << toString());		// check the vertex is in the set unvisitedWaitingVertices
	UNUSED(ok);

	routes->insertVertex(pvertex, pos.route, pos.pos);					// insert it
	unvisitedWaitingVertices.erase(pvertex);							// remove from the set
	routes->setWaitingTime(pos.route, pos.pos, initialWaitingTime);		// set waiting time

	solution_changed();
}


double Solution_SS_VRPTW_CR::removeWaitingVertex(const SolutionElement& w_vertex) {
	const VRP_vertex* pvertex = & routes->getVertexFromPos(w_vertex.route, w_vertex.pos);
	unvisitedWaitingVertices.insert(pvertex);							// insert back in the set of available (= unvisited) waiting vertices

	double wtime = Solution_DS_VRPTW::removeWaitingVertex(w_vertex);

	solution_changed();
	return wtime;
}


double Solution_SS_VRPTW_CR::increaseWaitingTime(const SolutionElement& w_vertex, double increment) {
	double actual_increment = Solution_DS_VRPTW::increaseWaitingTime(w_vertex, increment);
	solution_changed();
	return actual_increment;
}
double Solution_SS_VRPTW_CR::decreaseWaitingTime(const SolutionElement& w_vertex, double decrement) {
	double actual_decrement = Solution_DS_VRPTW::decreaseWaitingTime(w_vertex, decrement);	
	solution_changed();
	return actual_decrement;
}
void Solution_SS_VRPTW_CR::setWaitingTime(const SolutionElement& w_vertex, double w_time) {
	ASSERT(w_time > 0, "argh");
	Solution_DS_VRPTW::setWaitingTime(w_vertex, w_time);
	solution_changed();
}




// void Solution_SS_VRPTW_CR::moveVertex(const SolutionElement& v, const SolutionElement& p) {
// 	VRP_solution::moveVertex(v, p);
// 	solution_changed();
// }	
// void Solution_SS_VRPTW_CR::moveSegment(const SolutionElement& v, const SolutionElement& p) {
// 	VRP_solution::moveSegment(v,p);
// 	solution_changed();
// }	
// void Solution_SS_VRPTW_CR::swapVertices(const SolutionElement& v1, const SolutionElement& v2) {
// 	VRP_solution::swapVertices(v1, v2);
// 	solution_changed();
// }
// void Solution_SS_VRPTW_CR::swapSegments(const SolutionElement& s1, const SolutionElement& s2) {
// 	VRP_solution::swapSegments(s1, s2);
// 	solution_changed();
// }
// void Solution_SS_VRPTW_CR::invertSegment(const SolutionElement& s) {
// 	VRP_solution::invertSegment(s);
// 	solution_changed();
// }

void Solution_SS_VRPTW_CR::updateRequestAssignmentOrdering() {
	// ASSERT(initial_solution_generated, "");
	
	requests_ordered_per_waiting_locations.resize(routes->getNumberRoutes()+1);
	/* CLEAR requests_ordered_per_waiting_locations AND unassigned_requests */
	for (int route=1; route <= getNumberRoutes(); route++) {
		requests_ordered_per_waiting_locations[route].resize(routes->getRouteSize(route));
		for (auto& waiting_loc : requests_ordered_per_waiting_locations[route])
			waiting_loc.clear();
	}
	unassigned_requests.clear();

	for (auto& r : requests_ordered) {
		/* fill feasibleWaitingLocations the set of feasible waiting location for r */
		feasibleWaitingLocations.clear();
		SolutionElement w; 
		for (int route=1; route <= routes->getNumberRoutes(); route++) {		// 
			for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {		// -1 because we exclude the ending depot
				ASSERT(routes->getVertexFromPos(route, pos).getType() == VRP_vertex::WAITING, "");
				getSolutionElementAtPosition(&w, route, pos);
				if (t_min(r, w) <= t_max(r, w))
					feasibleWaitingLocations.push_back(w);
			}
		}
		/* sort feasibleWaitingLocations */
		if (feasibleWaitingLocations.size() > 0) {
			auto sortRule = [&] (const SolutionElement& a, const SolutionElement& b) -> bool
			{ 
				const VRP_vertex& v_r = r.getVertex();
				const VRP_vertex& v_a = routes->getVertexFromPos(a.route, a.pos);
				const VRP_vertex& v_b = routes->getVertexFromPos(b.route, b.pos);
				const VRP_VehicleType& veh_a = routes->getVehicleTypeFromRoute(a.route);
				const VRP_VehicleType& veh_b = routes->getVehicleTypeFromRoute(b.route);

				return veh_a.travelTime(v_r, v_a) < veh_b.travelTime(v_r, v_b); 
			};

			sort(feasibleWaitingLocations.begin(), feasibleWaitingLocations.end(), sortRule);
				
			w = feasibleWaitingLocations.front();
			ASSERT(routes->getVertexFromPos(w.route, w.pos).getType() == VRP_vertex::WAITING, "w is not a waiting vertex ! " << w.toString());
			requests_ordered_per_waiting_locations[w.route][w.pos].push_back(r);
		} 
		else 
			unassigned_requests.push_back(r);
	}

	for (int route=1; route <= getNumberRoutes(); route++) {
		ASSERT((int) requests_ordered_per_waiting_locations[route].size() == routes->getRouteSize(route), "");
		ASSERT(requests_ordered_per_waiting_locations[route][0].size() == 0, "");
	}

	assignment_done = true;
}


void Solution_SS_VRPTW_CR::print_requestsAssignmentOrdering() const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(assignment_done, "");
	int route_n = 0;
	for (auto& route : requests_ordered_per_waiting_locations) {
		if (route_n > 0) cout << "Route " << route_n << endl;
		route_n++;
		int pos = 0;
		for (auto& waiting_loc : route) {
			if (pos > 0) cout << "   pos " << pos;
			pos++;
			for (auto& r: waiting_loc)
				cout << " : " << r.toString();
			cout << endl;
		}
		cout << endl;
		
	}
	cout << endl << "unassigned_requests: ";
	for (auto& r : unassigned_requests)
		cout << r.toString() << "   ";
	cout << endl;
}

const vector< vector< vector<VRP_request>>> & Solution_SS_VRPTW_CR::getRequestAssignment() const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(assignment_done, "");
	return requests_ordered_per_waiting_locations;
}

const vector<VRP_request> & Solution_SS_VRPTW_CR::getUnassignedRequests() const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(assignment_done, "");
	return unassigned_requests;
}

const set<VRP_request> & Solution_SS_VRPTW_CR::getOrderedRequests() const {
	return requests_ordered;
}


void Solution_SS_VRPTW_CR::updateExpectedCost() {
	// ASSERT(initial_solution_generated, "");
	ASSERT(assignment_done, "");
	ASSERT(I.getNumberPotentialRequests() > 0, "");

	switch (recourseStrategy) {
		case R_INFTY:
			updateExpectedCost_R_INFTY();
			break;
		case R_CAPA:
			updateExpectedCost_R_CAPA();
			break;
		case R_CAPA_PLUS:
			updateExpectedCost_R_CAPA_PLUS();
			break;
		default: _ASSERT_(false, "invalid recourse strategy");
	}
}


void Solution_SS_VRPTW_CR::updateExpectedCost_R_INFTY() {

	double expectedNumberRejects = 0.0;
	int H = I.getHorizon();

	/* initialize probas matrices */
	hProbas.resize(I.getNumberPotentialRequests() + 1);	
	for (auto& vec : hProbas)
		vec.assign(H+1, 0.0);
	g1.resize(I.getNumberPotentialRequests() + 1);	
	for (auto& vec : g1)
		vec.assign(H+1, 0.0);
	g2.resize(I.getNumberPotentialRequests() + 1);	
	for (auto& vec : g2)
		vec.assign(H+1, 0.0);

	/* Start computation */
	for (int route=1; route <= routes->getNumberRoutes(); route++) {		// 
		const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(route);

		for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {		// -1 because we exclude the ending depot
			SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
			const VRP_vertex& w_vertex = getVertexAtElement(w);

			int prev_r_id = -1;
			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
			for (auto& r: waiting_loc) {
				double p_r = r.p / 100.0;
				
				/* Computing f(r) */
				double f_r = 0.0;
				for (int t = 1; t <= H; t++) {

					if(prev_r_id == -1) {	// if first request of the waiting location
						g1[r.id][t] = p_r * (t == max(getArrivalTimeAtElement(w), t_min(r, w)));
						g2[r.id][t] = (1.0 - p_r) * (t == max(getArrivalTimeAtElement(w), r.revealTime));
					} 
					else {
						ASSERT(prev_r_id > 0, prev_r_id);
						if (t == t_min(r, w) ) {
							for (int t_ = getArrivalTimeAtElement(w); t_ <= t_min(r, w); t_++)
								g1[r.id][t] += p_r * hProbas[prev_r_id][t_];
						}
						else if (t >= t_min(r, w) ) 
							g1[r.id][t] = p_r* hProbas[prev_r_id][t];

						if (t == r.revealTime ) {
							for (int t_ = getArrivalTimeAtElement(w); t_ <= r.revealTime; t_++)
								g2[r.id][t] += (1.0 - p_r) * hProbas[prev_r_id][t_];
						}
						else if (t > r.revealTime )
							g2[r.id][t] = (1.0 - p_r) * hProbas[prev_r_id][t];

					}

					double t_ = t - veh.travelTime(w_vertex,r.getVertex()) - veh.totalServiceTime(r) - veh.travelTime(r.getVertex(), w_vertex);	
					ASSERT(floor(t_) == t_, "not an integer ! (did you set instance.interger_travel_times = true ?)" << t_);
					int t__ = max(0, (int) t_);
					hProbas[r.id][t] = g1[r.id][t__] * satisfiable(r, t__, w, 0) + g1[r.id][t] * (1-satisfiable(r, t, w, 0)) + g2[r.id][t];

					f_r += g1[r.id][t] * (t_min(r, w) <= t && satisfiable(r, t, w, 0));
				}
				expectedNumberRejects += p_r - f_r;
				prev_r_id = r.id;
			}
		}
	}
	/* add probability mass of all unassigned requests */
	for (auto& r : unassigned_requests)
		expectedNumberRejects += r.p / 100.0;
	
	expectedCost = expectedNumberRejects;	
}



void Solution_SS_VRPTW_CR::updateExpectedCost_R_CAPA() {
	double expectedNumberRejects = 0.0;
	const VRP_VehicleType& veh_type = getVehicleTypeAtRoute(1);

	int Q = veh_type.getCapacity(), H = I.getHorizon();

	for (int i = 2; i <= getNumberRoutes(); i++) ASSERT(Q == getVehicleTypeAtRoute(i).getCapacity(), "works only for the same capacity at each vehicle !");

	/* initialize probas matrices */
	hProbas_Q.resize(I.getNumberPotentialRequests() + 1);	
	for (auto& vec : hProbas_Q) {
		vec.resize(H + 1);	
		for (auto& vec_ : vec)
			vec_.assign(Q +1, 0.0);	
	}
	g1_Q.resize(I.getNumberPotentialRequests() + 1);	
	for (auto& vec : g1_Q) {
		vec.resize(H + 1);	
		for (auto& vec_ : vec)
			vec_.assign(Q +1, 0.0);	
	}
	g2_Q.resize(I.getNumberPotentialRequests() + 1);	
	for (auto& vec : g2_Q) {
		vec.resize(H + 1);	
		for (auto& vec_ : vec)
			vec_.assign(Q +1, 0.0);	
	}

	/* Start computation */
	for (int route=1; route <= routes->getNumberRoutes(); route++) {		// 
		const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(route);
		const VRP_request* prev_r = NULL;
		SolutionElement w_prev_r;

		for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {		// -1 because we exclude the ending depot
			SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
			const VRP_vertex& w_vertex = getVertexAtElement(w);

			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
				auto& r = *it;
				double p_r = r.p / 100.0;

				if (t_min(r, w) > t_max(r, w))
					cout << endl << "Request " << r.toString() << ":  " << t_min(r, w) << " - " << t_max(r, w) << endl;

				/* Computing f(r) */
				double f_r = 0.0;
				for (int t = 1; t <= H; t++) {

					for (int q = 0; q <= Q; q++) {

						if(prev_r == NULL) {	// if first request of the ROUTE
							g1_Q[r.id][t][q] = p_r * (t == max( getArrivalTimeAtElement(w), t_min(r, w) ) && q == 0);
							g2_Q[r.id][t][q] = (1.0 - p_r) * (t == max(getArrivalTimeAtElement(w), r.revealTime ) && q == 0);
						} 
						else if (w_prev_r != w) {		// if it is the first request of the waiting location
							/* G1 */
							if (t == max( getArrivalTimeAtElement(w), t_min(r, w)))
								for (int t_ = getArrivalTimeAtElement(w_prev_r); t_ <= getDepartureTimeAtElement(w_prev_r); t_++)
									g1_Q[r.id][t][q] += p_r * hProbas_Q[prev_r->id][t_][q];
							
							/* G2 */
							if (t == max( getArrivalTimeAtElement(w), r.revealTime))
								for (int t_ = getArrivalTimeAtElement(w_prev_r); t_ <= getDepartureTimeAtElement(w_prev_r); t_++)
									g2_Q[r.id][t][q] += (1.0 - p_r) * hProbas_Q[prev_r->id][t_][q];
						}
						else {
							ASSERT(prev_r, "");
							/* G1 */
							if (t == t_min(r, w)) {
								for (int t_ = getArrivalTimeAtElement(w); t_ <= t_min(r, w); t_++)
									g1_Q[r.id][t][q] += p_r * hProbas_Q[prev_r->id][t_][q];
							}
							else if (t > t_min(r, w))
								g1_Q[r.id][t][q] = p_r * hProbas_Q[prev_r->id][t][q];
						
							/* G2 */
							if (t == max(getArrivalTimeAtElement(w), r.revealTime) ) {
								for (int t_ = getArrivalTimeAtElement(w); t_ <= max(getArrivalTimeAtElement(w), r.revealTime); t_++)
									g2_Q[r.id][t][q] += (1.0 - p_r) * hProbas_Q[prev_r->id][t_][q];
							}
							else if (t > max(getArrivalTimeAtElement(w), r.revealTime) )
								g2_Q[r.id][t][q] = (1.0 - p_r) * hProbas_Q[prev_r->id][t][q];
						}

						hProbas_Q[r.id][t][q] = g1_Q[r.id][t][q] * (1- satisfiable(r, t, w, q))   +   g2_Q[r.id][t][q];
						if (q - r.demand >= 0) {
							double t_ = t - veh.travelTime(w_vertex,r.getVertex()) - veh.totalServiceTime(r) - veh.travelTime(r.getVertex(), w_vertex);	
							ASSERT(floor(t_) == t_, "not an integer ! (did you set instance.integer_travel_times = true ?)" << t_);
							int t__ = max(0, (int) t_);

							hProbas_Q[r.id][t][q] += g1_Q[r.id][t__][q-(int)r.demand] * satisfiable(r, t__, w, q - r.demand);
						}
						

						f_r += g1_Q[r.id][t][q] * (t_min(r, w) <= t && satisfiable(r, t, w, q));
					}
				}
				expectedNumberRejects += p_r - f_r;
				prev_r = &r;
				getSolutionElementAtPosition(&w_prev_r, route, pos);
			}
		}
	}
	/* add probability mass of all unassigned requests */
	for (auto& r : unassigned_requests)
		expectedNumberRejects += r.p / 100.0;
	
	expectedCost = expectedNumberRejects;	
}


bool Solution_SS_VRPTW_CR::satisfiable(const VRP_request& r, int t, const SolutionElement& w, int q) const {
	const VRP_VehicleType& veh_type = getVehicleTypeAtRoute(w.route);
	if (recourseStrategy == R_INFTY)
		return t <= t_max(r, w);
	else
		return t <= t_max(r, w) && q + r.demand <= veh_type.getCapacity();
}



double Solution_SS_VRPTW_CR::t_min(const VRP_request& r, const SolutionElement& w) const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(w.type == VERTEX, "");
	const VRP_vertex& w_vertex = routes->getVertexFromPos(w.route, w.pos);
	ASSERT(w_vertex.getType() == VRP_vertex::WAITING, "");

	double arrival_time_w = routes->getArrivalTimeAtPos(w.route, w.pos);

	const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(w.route);
	return max( arrival_time_w, max( (double)r.revealTime, r.e - veh.travelTime(w_vertex, r.getVertex()) ) );
}

double Solution_SS_VRPTW_CR::t_max(const VRP_request& r, const SolutionElement& w) const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(w.type == VERTEX, "");
	const VRP_vertex& w_vertex = routes->getVertexFromPos(w.route, w.pos);
	ASSERT(w_vertex.getType() == VRP_vertex::WAITING, "");

	double departure_time_w = routes->getDepartureTimeAtPos(w.route, w.pos);

	const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(w.route);
	return min( r.l - veh.travelTime(w_vertex, r.getVertex()), departure_time_w - veh.travelTime(w_vertex, r.getVertex()) - veh.totalServiceTime(r) - veh.travelTime(r.getVertex(), w_vertex) );
}

	

double Solution_SS_VRPTW_CR::t_min_Rplus(const VRP_request& r, const VRP_vertex& v, const SolutionElement& w_r) const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(assignment_done, "");
	ASSERT(w_r.type == VERTEX, "");
	double arrival_time_w_r = routes->getArrivalTimeAtPos(w_r.route, w_r.pos);

	const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(w_r.route);
	return max( arrival_time_w_r, max( r.revealTime, r.e - veh.travelTime(v, r.getVertex())) );
}

double Solution_SS_VRPTW_CR::t_max_Rplus(const VRP_request& r, const VRP_vertex& v, const SolutionElement& w_r) const {
	// ASSERT(initial_solution_generated, "");
	ASSERT(assignment_done, "");
	ASSERT(w_r.type == VERTEX, "");
	const VRP_vertex& w_vertex_next = routes->getVertexFromPos(w_r.route, w_r.pos +1);					// waiting location (or depot) following the current w_r waiting location in the first stage solution
	double arrival_time_w_r_next = routes->getArrivalTimeAtPos(w_r.route, w_r.pos +1);				// arrival time planned at the waiting location (or depot) directly following w_r

	const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(w_r.route);
	return min( r.l - veh.travelTime(v, r.getVertex()), arrival_time_w_r_next - veh.travelTime(v, r.getVertex()) - veh.totalServiceTime(r) - veh.travelTime(r.getVertex(), w_vertex_next) );
}

bool Solution_SS_VRPTW_CR::satisfiable_Rplus(const VRP_request& r, int t, const VRP_vertex& v, int q, const SolutionElement& w_r) const {
	const VRP_VehicleType& veh_type = getVehicleTypeAtRoute(w_r.route);
	return t <= t_max_Rplus(r, v, w_r) && q + r.demand <= veh_type.getCapacity();
}



void Solution_SS_VRPTW_CR::updateExpectedCost_R_CAPA_PLUS() {
	double expectedNumberRejects = 0.0;

	const VRP_VehicleType& veh_type = getVehicleTypeAtRoute(1);
	int Q = veh_type.getCapacity(), H = I.getHorizon();
	for (int i = 2; i <= getNumberRoutes(); i++) ASSERT(Q == getVehicleTypeAtRoute(i).getCapacity(), "works only for the same capacity at each vehicle !");


	/* initialize probas matrices */
	h_Qplus_w__prev.resize(H + 1);	
	for (auto& vec_ : h_Qplus_w__prev)
		vec_.assign(Q + 1, 0.0);
	
	h_Qplus__prev.resize(H + 1);	
	for (auto& vec_ : h_Qplus__prev) {
		vec_.resize(Q + 1);
		for (auto& vec__ : vec_)
			vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
	}
	
	g1_Qplus_w__curr.resize(H + 1);	
	for (auto& vec_ : g1_Qplus_w__curr) 
		vec_.assign(Q + 1, 0.0);	
	
	g1_Qplus__curr.resize(H + 1);	
	for (auto& vec_ : g1_Qplus__curr) {
		vec_.resize(Q + 1);
		for (auto& vec__ : vec_)
			vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
	}
	
	g2_Qplus_w__curr.resize(H + 1);	
	for (auto& vec_ : g2_Qplus_w__curr)
		vec_.assign(Q + 1, 0.0);	
	
	g2_Qplus__curr.resize(H + 1);	
	for (auto& vec_ : g2_Qplus__curr) {
		vec_.resize(Q + 1);
		for (auto& vec__ : vec_)
			vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
	}
	

	/* Start computation */
	for (int route=1; route <= routes->getNumberRoutes(); route++) {		// 
		const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(route);

		const VRP_request* prev_r = NULL;
		SolutionElement w_prev_r;
		int pos_w_prev_r = -1;

		for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {		// -1 because we exclude the ending depot
			SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
			const VRP_vertex& w_vertex = getVertexAtElement(w);

			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
				const VRP_request& r = *it;
				double p_r = r.p / 100.0;

				/* Computing f(r) */
				double f_r = 0.0;
				for (int t = 1; t <= H; t++) {

					for (int q = 0; q <= Q; q++) {

						/**********  g1_Qplus_w  ~~~  g2_Qplus_w  **********/
						g1_Qplus_w__curr[t][q] = 0.0;
						g2_Qplus_w__curr[t][q] = 0.0;

						if(prev_r == NULL) {			// if first request of the ROUTE
							g1_Qplus_w__curr[t][q] = p_r * (t == max( getArrivalTimeAtElement(w), t_min_Rplus(r, w_vertex, w) ) && q == 0 );
							g2_Qplus_w__curr[t][q] = (1.0 - p_r) * (t == max(getArrivalTimeAtElement(w), r.revealTime ) && q == 0 );
						} 
						else if (w_prev_r != w) {		// if it is the first request of the waiting location
							/* G1 */
							if (t == max( getArrivalTimeAtElement(w), t_min_Rplus(r, w_vertex, w)))
								for (int t_ = getArrivalTimeAtElement(w_prev_r); t_ <= getDepartureTimeAtElement(w_prev_r); t_++) {
									g1_Qplus_w__curr[t][q] += p_r * h_Qplus_w__prev[t_][q];
									for (const VRP_request& r_ : requests_ordered_per_waiting_locations[route][pos_w_prev_r])
										g1_Qplus_w__curr[t][q] += p_r * h_Qplus__prev[t_][q][r_.id];
								}
							
							/* G2 */
							if (t == max( getArrivalTimeAtElement(w), r.revealTime))
								for (int t_ = getArrivalTimeAtElement(w_prev_r); t_ <= getDepartureTimeAtElement(w_prev_r); t_++) {
									g2_Qplus_w__curr[t][q] += (1.0 - p_r) * h_Qplus_w__prev[t_][q];
									for (const VRP_request& r_ : requests_ordered_per_waiting_locations[route][pos_w_prev_r])
										g2_Qplus_w__curr[t][q] += (1.0 - p_r) * h_Qplus__prev[t_][q][r_.id];
								}
						}
						else {
							/* G1 */
							if (t == t_min_Rplus(r, w_vertex, w)) {
								for (int t_ = getArrivalTimeAtElement(w); t_ <= t_min_Rplus(r, w_vertex, w); t_++)
									g1_Qplus_w__curr[t][q] += p_r * h_Qplus_w__prev[t_][q];
							}
							else if (t > t_min_Rplus(r, w_vertex, w))
								g1_Qplus_w__curr[t][q] = p_r * h_Qplus_w__prev[t][q];
						
							/* G2 */
							if (t == max(getArrivalTimeAtElement(w), r.revealTime) ) {
								for (int t_ = getArrivalTimeAtElement(w); t_ <= max(getArrivalTimeAtElement(w), r.revealTime); t_++)
									g2_Qplus_w__curr[t][q] += (1.0 - p_r) * h_Qplus_w__prev[t_][q];
							}
							else if (t > max(getArrivalTimeAtElement(w), r.revealTime) )
								g2_Qplus_w__curr[t][q] = (1.0 - p_r) * h_Qplus_w__prev[t][q];
						}


						/**********  g1_Qplus  ~~~  g2_Qplus    : r' < r  **********/
						g1_Qplus__curr[t][q].assign(I.getNumberPotentialRequests() + 1, 0.0);	
						g2_Qplus__curr[t][q].assign(I.getNumberPotentialRequests() + 1, 0.0);	

						for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;

							ASSERT(prev_r, "");
							/* G1 */
							if (t == t_min_Rplus(r, r_.getVertex(), w)) {
								for (int t_ = getArrivalTimeAtElement(w); t_ <= t_min_Rplus(r, r_.getVertex(), w); t_++)
									g1_Qplus__curr[t][q][r_.id] += p_r * h_Qplus__prev[t_][q][r_.id];
							}
							else if (t > t_min_Rplus(r, r_.getVertex(), w))
								g1_Qplus__curr[t][q][r_.id] = p_r * h_Qplus__prev[t][q][r_.id];
						
							/* G2 */
							if (t == max(getArrivalTimeAtElement(w), r.revealTime) ) {
								for (int t_ = getArrivalTimeAtElement(w); t_ <= max(getArrivalTimeAtElement(w), r.revealTime); t_++)
									g2_Qplus__curr[t][q][r_.id] += (1.0 - p_r) * h_Qplus__prev[t_][q][r_.id];
							}
							else if (t > max(getArrivalTimeAtElement(w), r.revealTime) )
								g2_Qplus__curr[t][q][r_.id] = (1.0 - p_r) * h_Qplus__prev[t][q][r_.id];
						}

						

	
						/**********  f(r)  **********/
						f_r += g1_Qplus_w__curr[t][q] * (t_min_Rplus(r, w_vertex, w) <= t && satisfiable_Rplus(r, t, w_vertex, q, w));
						for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
							f_r += g1_Qplus__curr[t][q][r_.id] * (t_min_Rplus(r, r_.getVertex(), w) <= t && satisfiable_Rplus(r, t, r_.getVertex(), q, w));
						}	
						
					}

				}

				for (int t = 1; t <= H; t++) {
					for (int q = 0; q <= Q; q++) {

						int next_revealtime = 0;
						if (it+1 != waiting_loc.cend()) next_revealtime = (it+1)->revealTime;


						/**********  h_Qplus_w  **********/
						h_Qplus_w__prev[t][q] = 0.0;
							
						h_Qplus_w__prev[t][q] += g1_Qplus_w__curr[t][q] * (1- satisfiable_Rplus(r, t, w_vertex, q, w)) + g2_Qplus_w__curr[t][q];

						if (t - veh.travelTime(r.getVertex(), w_vertex) < next_revealtime) {
							if (q - r.demand >= 0) {
								int t_ = max(0, t - (int) floor( veh.travelTime(w_vertex,r.getVertex()) + veh.totalServiceTime(r) + veh.travelTime(r.getVertex(), w_vertex)));
								h_Qplus_w__prev[t][q] += g1_Qplus_w__curr[t_][q-(int)r.demand] * satisfiable_Rplus(r, t_, w_vertex, q - r.demand, w);

								for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
									int t_ = max(0, t - (int) floor( veh.travelTime(r_.getVertex(),r.getVertex()) + veh.totalServiceTime(r) + veh.travelTime(r.getVertex(), w_vertex)));
									h_Qplus_w__prev[t][q] += g1_Qplus__curr[t_][q-(int)r.demand][r_.id] * satisfiable_Rplus(r, t_, r_.getVertex(), q - r.demand, w);
								}
							}
						}

						for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
							int t_ = max(0, t - (int) floor( veh.travelTime(r_.getVertex(), w_vertex)));
							if (t_ < next_revealtime) {
								h_Qplus_w__prev[t][q] += g1_Qplus__curr[t_][q][r_.id] * (1 - satisfiable_Rplus(r, t_, r_.getVertex(), q, w));
								h_Qplus_w__prev[t][q] += g2_Qplus__curr[t_][q][r_.id];
							}
						}

						/**********  h_Qplus  : r  **********/
						h_Qplus__prev[t][q].assign(I.getNumberPotentialRequests() + 1, 0.0);

						if (t >= next_revealtime && q - r.demand >= 0) {
							int t_ = max(0, t - (int) floor( veh.travelTime(w_vertex,r.getVertex()) + veh.totalServiceTime(r)));
							h_Qplus__prev[t][q][r.id] += g1_Qplus_w__curr[t_][q - r.demand] * satisfiable_Rplus(r, t_, w_vertex, q - r.demand, w);
							for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
								int t_ = max(0, t - (int) floor( veh.travelTime(r_.getVertex(),r.getVertex()) + veh.totalServiceTime(r)));
								h_Qplus__prev[t][q][r.id] += g1_Qplus__curr[t_][q - r.demand][r_.id] * satisfiable_Rplus(r, t_, r_.getVertex(), q - r.demand, w);
							}
						}

						/**********  h_Qplus  : r' < r  **********/
						if (t >= next_revealtime) {
							for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
								h_Qplus__prev[t][q][r_.id] += g1_Qplus__curr[t][q][r_.id] * (1 - satisfiable_Rplus(r, t, r_.getVertex(), q, w)) + g2_Qplus__curr[t][q][r_.id];
							}
						}

					}

				}

				expectedNumberRejects += p_r - f_r;
				prev_r = &r;
				getSolutionElementAtPosition(&w_prev_r, route, pos);
				pos_w_prev_r = pos;

			}
		}
	}
	/* add probability mass of all unassigned requests */
	for (auto& r : unassigned_requests)
		expectedNumberRejects += r.p / 100.0;
	
	expectedCost = expectedNumberRejects;	
}



// void Solution_SS_VRPTW_CR::updateExpectedCost_R_CAPA_PLUS() {
// 	double expectedNumberRejects = 0.0;
// 	int Q = I.getVehicleCapacity(), H = I.getHorizon();

// 	/* initialize probas matrices */
// 	h_Qplus_w.resize(I.getNumberPotentialRequests() + 1);	
// 	for (auto& vec : h_Qplus_w) {
// 		vec.resize(H + 1);	
// 		for (auto& vec_ : vec)
// 			vec_.assign(Q + 1, 0.0);
// 	}
// 	h_Qplus.resize(I.getNumberPotentialRequests() + 1);	
// 	for (auto& vec : h_Qplus) {
// 		vec.resize(H + 1);	
// 		for (auto& vec_ : vec) {
// 			vec_.resize(Q + 1);
// 			for (auto& vec__ : vec_)
// 				vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
// 		}
// 	}
// 	g1_Qplus_w.resize(I.getNumberPotentialRequests() + 1);	
// 	for (auto& vec : g1_Qplus_w) {
// 		vec.resize(H + 1);	
// 		for (auto& vec_ : vec) 
// 			vec_.assign(Q + 1, 0.0);	
// 	}
// 	g1_Qplus.resize(I.getNumberPotentialRequests() + 1);	
// 	for (auto& vec : g1_Qplus) {
// 		vec.resize(H + 1);	
// 		for (auto& vec_ : vec) {
// 			vec_.resize(Q + 1);
// 			for (auto& vec__ : vec_)
// 				vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
// 		}
// 	}
// 	g2_Qplus_w.resize(I.getNumberPotentialRequests() + 1);	
// 	for (auto& vec : g2_Qplus_w) {
// 		vec.resize(H + 1);	
// 		for (auto& vec_ : vec)
// 			vec_.assign(Q + 1, 0.0);	
// 	}
// 	g2_Qplus.resize(I.getNumberPotentialRequests() + 1);	
// 	for (auto& vec : g2_Qplus) {
// 		vec.resize(H + 1);	
// 		for (auto& vec_ : vec) {
// 			vec_.resize(Q + 1);
// 			for (auto& vec__ : vec_)
// 				vec__.assign(I.getNumberPotentialRequests() + 1, 0.0);	
// 		}
// 	}

// 	/* Start computation */
// 	for (int route=1; route <= routes->getNumberRoutes(); route++) {		// 
// 		const VRP_request* prev_r = NULL;
// 		SolutionElement w_prev_r;
// 		int pos_w_prev_r = -1;

// 		for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {		// -1 because we exclude the ending depot
// 			SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
// 			const VRP_vertex& w_vertex = getVertexAtElement(w);

// 			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
// 			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
// 				const VRP_request& r = *it;
// 				double p_r = r.p / 100.0;

// 				/* Computing f(r) */
// 				double f_r = 0.0;
// 				for (int t = 1; t <= H; t++) {

// 					for (int q = 0; q <= Q; q++) {

// 						/**********  g1_Qplus_w  ~~~  g2_Qplus_w  **********/

// 						if(prev_r == NULL) {			// if first request of the ROUTE
// 							g1_Qplus_w[r.id][t][q] = p_r * (t == max( getArrivalTimeAtElement(w), t_min_Rplus(r, w_vertex, w) ) && q == 0 );
// 							g2_Qplus_w[r.id][t][q] = (1.0 - p_r) * (t == max(getArrivalTimeAtElement(w), r.revealTime ) && q == 0 );
// 						} 
// 						else if (w_prev_r != w) {		// if it is the first request of the waiting location
// 							/* G1 */
// 							if (t == max( getArrivalTimeAtElement(w), t_min_Rplus(r, w_vertex, w)))
// 								for (int t_ = getArrivalTimeAtElement(w_prev_r); t_ <= getDepartureTimeAtElement(w_prev_r); t_++) {
// 									g1_Qplus_w[r.id][t][q] += p_r * h_Qplus_w[prev_r->id][t_][q];
// 									for (const VRP_request& r_ : requests_ordered_per_waiting_locations[route][pos_w_prev_r])
// 										g1_Qplus_w[r.id][t][q] += p_r * h_Qplus[prev_r->id][t_][q][r_.id];
// 								}
							
// 							/* G2 */
// 							if (t == max( getArrivalTimeAtElement(w), r.revealTime))
// 								for (int t_ = getArrivalTimeAtElement(w_prev_r); t_ <= getDepartureTimeAtElement(w_prev_r); t_++) {
// 									g2_Qplus_w[r.id][t][q] += (1.0 - p_r) * h_Qplus_w[prev_r->id][t_][q];
// 									for (const VRP_request& r_ : requests_ordered_per_waiting_locations[route][pos_w_prev_r])
// 										g2_Qplus_w[r.id][t][q] += (1.0 - p_r) * h_Qplus[prev_r->id][t_][q][r_.id];
// 								}
// 						}
// 						else {
// 							/* G1 */
// 							if (t == t_min_Rplus(r, w_vertex, w)) {
// 								for (int t_ = getArrivalTimeAtElement(w); t_ <= t_min_Rplus(r, w_vertex, w); t_++)
// 									g1_Qplus_w[r.id][t][q] += p_r * h_Qplus_w[prev_r->id][t_][q];
// 							}
// 							else if (t > t_min_Rplus(r, w_vertex, w))
// 								g1_Qplus_w[r.id][t][q] = p_r * h_Qplus_w[prev_r->id][t][q];
						
// 							/* G2 */
// 							if (t == max(getArrivalTimeAtElement(w), r.revealTime) ) {
// 								for (int t_ = getArrivalTimeAtElement(w); t_ <= max(getArrivalTimeAtElement(w), r.revealTime); t_++)
// 									g2_Qplus_w[r.id][t][q] += (1.0 - p_r) * h_Qplus_w[prev_r->id][t_][q];
// 							}
// 							else if (t > max(getArrivalTimeAtElement(w), r.revealTime) )
// 								g2_Qplus_w[r.id][t][q] = (1.0 - p_r) * h_Qplus_w[prev_r->id][t][q];
// 						}


// 						/**********  g1_Qplus  ~~~  g2_Qplus    : r' < r  **********/
// 						for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;

// 							ASSERT(prev_r, "");
// 							/* G1 */
// 							if (t == t_min_Rplus(r, r_.getVertex(), w)) {
// 								for (int t_ = getArrivalTimeAtElement(w); t_ <= t_min_Rplus(r, r_.getVertex(), w); t_++)
// 									g1_Qplus[r.id][t][q][r_.id] += p_r * h_Qplus[prev_r->id][t_][q][r_.id];
// 							}
// 							else if (t > t_min_Rplus(r, r_.getVertex(), w))
// 								g1_Qplus[r.id][t][q][r_.id] = p_r * h_Qplus[prev_r->id][t][q][r_.id];
						
// 							/* G2 */
// 							if (t == max(getArrivalTimeAtElement(w), r.revealTime) ) {
// 								for (int t_ = getArrivalTimeAtElement(w); t_ <= max(getArrivalTimeAtElement(w), r.revealTime); t_++)
// 									g2_Qplus[r.id][t][q][r_.id] += (1.0 - p_r) * h_Qplus[prev_r->id][t_][q][r_.id];
// 							}
// 							else if (t > max(getArrivalTimeAtElement(w), r.revealTime) )
// 								g2_Qplus[r.id][t][q][r_.id] = (1.0 - p_r) * h_Qplus[prev_r->id][t][q][r_.id];
// 						}

						


// 						int next_revealtime = 0;
// 						if (it+1 != waiting_loc.cend()) next_revealtime = (it+1)->revealTime;

// 						/**********  h_Qplus_w  **********/
							
// 						h_Qplus_w[r.id][t][q] += g1_Qplus_w[r.id][t][q] * (1- satisfiable_Rplus(r, t, w_vertex, q, w)) + g2_Qplus_w[r.id][t][q];

// 						if (t - I.travelTime(r.getVertex(), w_vertex) < next_revealtime) {
// 							if (q - r.demand >= 0) {
// 								int t_ = max(0, t - (int) floor( I.travelTime(w_vertex,r.getVertex()) + r.duration + I.travelTime(r.getVertex(), w_vertex)));
// 								h_Qplus_w[r.id][t][q] += g1_Qplus_w[r.id][t_][q-(int)r.demand] * satisfiable_Rplus(r, t_, w_vertex, q - r.demand, w);

// 								for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
// 									int t_ = max(0, t - (int) floor( I.travelTime(r_.getVertex(),r.getVertex()) + r.duration + I.travelTime(r.getVertex(), w_vertex)));
// 									h_Qplus_w[r.id][t][q] += g1_Qplus[r.id][t_][q-(int)r.demand][r_.id] * satisfiable_Rplus(r, t_, r_.getVertex(), q - r.demand, w);
// 								}
// 							}
// 						}

// 						for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
// 							int t_ = max(0, t - (int) floor( I.travelTime(r_.getVertex(), w_vertex)));
// 							if (t_ < next_revealtime) {
// 								h_Qplus_w[r.id][t][q] += g1_Qplus[r.id][t_][q][r_.id] * (1 - satisfiable_Rplus(r, t_, r_.getVertex(), q, w));
// 								h_Qplus_w[r.id][t][q] += g2_Qplus[r.id][t_][q][r_.id];
// 							}
// 						}

// 						/**********  h_Qplus  : r  **********/
// 						if (t >= next_revealtime && q - r.demand >= 0) {
// 							int t_ = max(0, t - (int) floor( I.travelTime(w_vertex,r.getVertex()) + r.duration));
// 							h_Qplus[r.id][t][q][r.id] += g1_Qplus_w[r.id][t_][q - r.demand] * satisfiable_Rplus(r, t_, w_vertex, q - r.demand, w);
// 							for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
// 								int t_ = max(0, t - (int) floor( I.travelTime(r_.getVertex(),r.getVertex()) + r.duration));
// 								h_Qplus[r.id][t][q][r.id] += g1_Qplus[r.id][t_][q - r.demand][r_.id] * satisfiable_Rplus(r, t_, r_.getVertex(), q - r.demand, w);
// 							}
// 						}

// 						/**********  h_Qplus  : r' < r  **********/
// 						if (t >= next_revealtime) {
// 							for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
// 								h_Qplus[r.id][t][q][r_.id] += g1_Qplus[r.id][t][q][r_.id] * (1 - satisfiable_Rplus(r, t, r_.getVertex(), q, w)) + g2_Qplus[r.id][t][q][r_.id];
// 							}
// 						}

							
// 						/**********  f(r)  **********/
// 						f_r += g1_Qplus_w[r.id][t][q] * (t_min_Rplus(r, w_vertex, w) <= t && satisfiable_Rplus(r, t, w_vertex, q, w));
// 						for (const VRP_request& r_ : waiting_loc) { 	if (r_ >= r) break;
// 							f_r += g1_Qplus[r.id][t][q][r_.id] * (t_min_Rplus(r, r_.getVertex(), w) <= t && satisfiable_Rplus(r, t, r_.getVertex(), q, w));
// 						}	
						
// 					}
// 				}
// 				expectedNumberRejects += p_r - f_r;
// 				prev_r = &r;
// 				getSolutionElementAtPosition(&w_prev_r, route, pos);
// 				pos_w_prev_r = pos;
// 			}
// 		}
// 	}
// 	/* add probability mass of all unassigned requests */
// 	for (auto& r : unassigned_requests)
// 		expectedNumberRejects += r.p / 100.0;
	
// 	expectedCost = expectedNumberRejects;	
// }


string& Solution_SS_VRPTW_CR::toString_gProbas() {
	// updateExpectedCost();

	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(2);
	for (int route=1; route <= routes->getNumberRoutes(); route++) {	
		const VRP_VehicleType& veh = routes->getVehicleTypeFromRoute(route);
		out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

		out << outputMisc::greenExpr(true) << "H:                                                                              "; 
		for (int t=1; t <= I.getHorizon(); t++) 
			out << setw(4) << t << "  "; 
		out << endl << "Req:" << endl << outputMisc::resetColor() ;

		for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {
			SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
			const VRP_vertex& w_vertex = getVertexAtElement(w);
			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
				auto& r = *it;
				
				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
				out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) t_min(r,w) << " tmax=" << setw(3) << (int) t_max(r,w) << " S=" << setw(3) << (int) veh.travelTime(w_vertex,r.getVertex()) + (int) veh.totalServiceTime(r) + (int) veh.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
				for (int t=1; t <= I.getHorizon(); t++) {
					double g1_t = 0.0, g2_t = 0.0;
					
					switch (recourseStrategy) {
						case R_INFTY:
							g1_t += g1[r.id][t];
							g2_t += g2[r.id][t];
							break;
						case R_CAPA:
							ASSERT((int) hProbas_Q[r.id][t].size() == veh.getCapacity()+1, "");
							for (int q=0; q <= veh.getCapacity(); q++) {
								g1_t += g1_Q[r.id][t][q];
								g2_t += g2_Q[r.id][t][q];
							}
							break;
						default: _ASSERT_(false, "");
					}

					
					out << outputMisc::greenExpr(g1_t > 0) << setfill('0') << setw(2) << (int)round(g1_t * 100) << outputMisc::resetColor() << setfill(' ');
					out << outputMisc::redExpr(g2_t > 0) << setfill('0') << setw(2) << (int)round(g2_t * 100) << outputMisc::resetColor() << setfill(' ');
					out << " "; if (g1_t < 0.995 && g2_t < 0.995) out << " ";
				}
				out << endl;
			}
		} 
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}



// string& Solution_SS_VRPTW_CR::toString_hProbas() {
// 	// updateExpectedCost();

// 	ostringstream out; 
// 	out.setf(std::ios::fixed);
// 	out.precision(2);
// 	for (int route=1; route <= routes->getNumberRoutes(); route++) {	
// 		out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

// 		out << outputMisc::greenExpr(true) << "H:                                                                                                "; 
// 		for (int t=1; t <= I.getHorizon(); t++) 
// 			out << setw(3) << t << "   "; 
// 		out << endl << "Req:" << endl << outputMisc::resetColor() ;

// 		for (int pos=1; pos <= routes->getRouteSize(route)-1; pos++) {
// 			SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
// 			const VRP_vertex& w_vertex = getVertexAtElement(w);
// 			auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
// 			for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
// 				auto& r = *it;
				
// 				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
// 				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
// 				if (recourseStrategy == R_CAPA_PLUS) {
// 					out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) t_min_Rplus(r,w_vertex,w) << " tmax=" << setw(3) << (int) t_max_Rplus(r,w_vertex,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
// 				} else
// 					out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) t_min(r,w) << " tmax=" << setw(3) << (int) t_max(r,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
// 				for (int t=1; t <= I.getHorizon(); t++) {
// 					double h_t = 0.0;
					
// 					switch (recourseStrategy) {
// 						case R_INFTY:
// 							h_t += hProbas[r.id][t];
// 							break;
// 						case R_CAPA:
// 							ASSERT((int)hProbas_Q[r.id][t].size() == I.getVehicleCapacity()+1, "");
// 							for (int q=0; q <= I.getVehicleCapacity(); q++) {
// 								h_t += hProbas_Q[r.id][t][q];
// 							}
// 							break;
// 						case R_CAPA_PLUS:
// 							for (int q=0; q <= I.getVehicleCapacity(); q++) {
// 								h_t += h_Qplus_w[r.id][t][q];
// 								for (auto& r_ : requests_ordered_per_waiting_locations[route][pos])	
// 									h_t += h_Qplus[r.id][t][q][r_.id];
// 							}
// 							break;
							
// 						default: _ASSERT_(false, "");
// 					}

// 					out << " "; if (h_t < 0.995) out << " ";
// 					out << outputMisc::blueExpr(h_t > 0) << setfill('0') << setw(2) << (int)round(h_t * 100) << outputMisc::resetColor() << setfill(' ') << "  ";
					
// 				}
// 				out << endl;
// 			}
// 		} 
// 		out << endl;
// 	}
// 	static string str = ""; str = out.str();
// 	return (str);
// }


// string& Solution_SS_VRPTW_CR::toString_h_v(int route, int request_id, int n) {
// 	ASSERT(recourseStrategy == R_CAPA_PLUS, "");
// 	ostringstream out; 
// 	out.setf(std::ios::fixed);
// 	out.precision(2);
	

// 	out << outputMisc::greenExpr(true) << "Vehicle " << route << outputMisc::resetColor() << "\t\t t_min: min. handle time fr/ waiting loc \t t_max: max. handle time fr/ waiting loc if fixed arrival times" << endl;

// 	out << outputMisc::greenExpr(true) << "H:                                                                               "; 
// 	for (int t=1; t <= I.getHorizon(); t++) 
// 		out << setw(3) << t << "   "; 
// 	out << endl << "Req:" << endl << outputMisc::resetColor() ;

// 	int n_ = 0;

// 	for (int pos=1; pos <= getRouteSize(route)-1; pos++) {

// 		Solution_VRPTW::SolutionElement w; getSolutionElementAtPosition(&w, route, pos);
// 		const VRP_vertex& w_vertex = getVertexAtElement(w);
// 		auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
// 		for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
// 			auto& r = *it;
			
// 			if (r.id == request_id) 
// 				n_ = n;

// 			if (n_ > 0) {
// 				out << outputMisc::blueExpr(true) << w_vertex.toString() << outputMisc::resetColor();
// 				out << outputMisc::greenExpr(true) << r.toString(true) << outputMisc::resetColor() << "  ";
// 				out << outputMisc::blueExpr(true) << "tmin=" << setw(3) << (int) t_min(r,w) << " tmax=" << setw(3) << (int) t_max(r,w) << " S=" << setw(3) << (int) I.travelTime(w_vertex,r.getVertex()) + (int) r.duration + (int) I.travelTime(r.getVertex(), w_vertex) << outputMisc::resetColor() << "  ";
// 				for (int t=1; t <= I.getHorizon(); t++) {
// 					double h_t = 0.0;
					
// 					for (int q=0; q <= I.getVehicleCapacity(); q++) {
// 						h_t += h_Qplus_w[r.id][t][q];
// 						for (auto& r_ : requests_ordered_per_waiting_locations[route][pos])	
// 							h_t += h_Qplus[r.id][t][q][r_.id];
// 					}


// 					out << " "; if (h_t < 0.995) out << " ";
// 					out << outputMisc::blueExpr(h_t > 0) << setfill('0') << setw(2) << (int)round(h_t * 100) << outputMisc::resetColor() << setfill(' ') << "  ";		
// 				}
// 				out << endl;

// 				double h_t_w = 0.0;
// 				for (int t=1; t <= I.getHorizon(); t++) 
// 					for (int q=0; q <= I.getVehicleCapacity(); q++)
// 						h_t_w += h_Qplus_w[r.id][t][q];
// 				if (h_t_w > 0) {
// 					out << outputMisc::magentaExpr(true);
// 					out << "\tFrom waiting vertex: \t\t\t\t\t\t\t";
// 					for (int t=1; t <= I.getHorizon(); t++) {
// 						h_t_w = 0.0;
// 						for (int q=0; q <= I.getVehicleCapacity(); q++)
// 							h_t_w += h_Qplus_w[r.id][t][q];
// 						out << " "; if (h_t_w < 0.995) out << " ";
// 						if (h_t_w > 0)
// 							out << setfill('0') << setw(2) << (int)round(h_t_w * 100) << setfill(' ') << "  ";
// 						else out << "    ";
// 					}
// 					out << outputMisc::resetColor() <<  endl;
// 				}

// 				for (int pos=1; pos <= getRouteSize(route)-1; pos++) {
// 					auto& waiting_loc = requests_ordered_per_waiting_locations[route][pos];
// 					for (auto it = waiting_loc.cbegin(); it != waiting_loc.cend(); it++) {		
// 						auto& r_ = *it;

// 						double h_t_rid = 0.0;
// 						for (int t=1; t <= I.getHorizon(); t++) 
// 							for (int q=0; q <= I.getVehicleCapacity(); q++)
// 								h_t_rid += h_Qplus[r.id][t][q][r_.id];
// 						if (h_t_rid > 0) {
// 							out << outputMisc::magentaExpr(true);
// 							out << "\tFrom request " << r_.id << ": \t\t\t\t\t\t\t";
// 							for (int t=1; t <= I.getHorizon(); t++) {
// 								h_t_rid = 0.0;
// 								for (int q=0; q <= I.getVehicleCapacity(); q++)
// 									h_t_rid += h_Qplus[r.id][t][q][r_.id];
// 								out << " "; if (h_t_rid < 0.995) out << " ";
// 								if (h_t_rid > 0)
// 									out << setfill('0') << setw(2) << (int)round(h_t_rid * 100) << setfill(' ') << "  ";
// 								else out << "    ";
// 							}
// 							out << outputMisc::resetColor() <<  endl;
// 						}
// 					}
// 				}

// 				n_--;
// 			}
// 		}
// 	}
// 	static string str = ""; str = out.str();
// 	return (str);
// }

void Solution_SS_VRPTW_CR::print_unvisitedWaitingVertices() const {
	cout << "Unvisited waiting vertices: ";
	for (const VRP_vertex* pvertex : unvisitedWaitingVertices)
		cout << pvertex->toString() << " - ";
	cout << flush;
}







