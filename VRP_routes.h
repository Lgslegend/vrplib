/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/* VRP_routes +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	Everything that you need to represent a set of VRP routes, 
	including arrival times, departure times, etc

	Stores only integer IDs to represent the vertices (0 is the depot)

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#ifndef VRP_ROUTES_H
#define	VRP_ROUTES_H

#include <iostream>
#include <vector>
#include <cmath>
#include <unordered_map>
#include "tools.h"

using namespace std;

enum WaitingStrategy {
	DRIVE_FIRST,
	DRIVE_FIRST_DELAY_LAST,		// idem DRIVE_FIRST, but the departure on the last visited node (just before the ending depot) is delayed as much as possible
	WAIT_FIRST,
	RELOCATION_ONLY,
	CUSTOM_WAIT
};

enum ActionType {
	NO_ACTION,
	SERVE,			// the vehicle is serving a request
	MOVE_TO,		// the vehicle is moving towards some vertex
	WAIT_AT,		// the vehicle is waiting at a vertex, after having serviced its request (if any), before departing from the vertex
	WAIT_SERVE		// the vehicle has arrived at a vertex, but is waiting for being allowed to serve it
};

class VRP_routes { 
public:

	enum Constants {
		MAX_ROUTES 				= 100,									// max number of routes
		MAX_NODES_PER_ROUTE 	= 1000,									// max number of nodes per route
		NODE_POOL_SIZE 			= MAX_NODES_PER_ROUTE*MAX_ROUTES		// max number of nodes in total
	};
	enum AssertLevel {
		ASSERT_NO_FIXED_LEFT,
		ASSERT_ALL,
		ASSERT_NOTHING
	};

protected:
	const VRP_instance& I;
	AssertLevel assertLevel = ASSERT_ALL;
	WaitingStrategy waitingStrategy = DRIVE_FIRST;

	struct NodeAttr {
		double 	waiting_time;	

		/* the following attributes are updated by updateRouteInfos() --- if the node is not fixed ! (otherwise the attributes must remain fixed as well) */
		int 	pos;				// position of the vertex in its route (0 for the starting depot)
		int 	veh;				// route number in which the vertex is
		int 	current_load = 0;	// current vehicle load at that node

		double 	arrival_time;	// times are computed w.r.t. distances and vehicle velocity
		double 	service_time;
		double 	departure_time;

		bool fixed;				// tells whether the vehicle already visited the node
		bool left;				// tells whether the vehicle already visited and left the node
		bool served;			// tells whether the vehicle already started to serve the request at the node

		bool operator == (const VRP_routes::NodeAttr& other) const;
		bool operator != (const VRP_routes::NodeAttr& other) const { return !operator==(other);}
	};

	struct Node {			// A node in the double linked list
		const VRP_vertex* vertex;
		const VRP_request* online_request = nullptr;	// pointer to the online request considered for that node; for vertice type REGULAR_ONLINE only
		NodeAttr	attr;
		Node* 		next_node;
		Node* 		prev_node;
	};

	struct Route {
		const VRP_VehicleType* vehicle = NULL;
		Node* 	first_node = NULL;					// designed to be the depot; never leaves this route
		Node* 	last_node = NULL;					// idem, the depot

		// const VRP_vertex* unload_depot = NULL;	// used with activateAutomaticVehicleUnload
		bool unload_depot = false;				// indicate whether the vehicle is allowed to automatically unload at the depot when capacity is reached

		/* the following attributes are updated at each call of updateRouteInfos() */
		Node* 	pos2node[MAX_NODES_PER_ROUTE];	// pos2node[n] == &e iff node *e (pointeur to Node object) is at position n in current route --> allows fast lookup on random access
		int 	nNodes;							// number of nodes in the route (including both starting and ending depots)
		int 	firstFreeNodePos;				// position of the first non fixed  node on the route (-1 if route fixed)
		int 	firstUNLEFTpos = -1;			// position of the first UNLEFT node on the route (-1 if no such)
		int 	firstFreeWaitingPos;			// position of the first non fixed  node (containing a WAITING VERTEX) on the route (-1 if no such)
		int 	firstUNLEFTWaitingPos;			// position of the first UNLEFT node (containing a WAITING VERTEX) on the route (-1 if no such)
		int 	nFreeVertices;					// number of remaining free vertices in the route (including the last depot)
		int 	nFreeWaitingVertices;			// number of remaining free WAITING vertices in the route
		int 	nUNLEFTWaitingVertices;			// number of remaining UNLEFT WAITING vertices in the route
		int 	nMoveableVertices;				// number of remaining free vertices in the route (EXCLUDING the last depot)
		int 	routeLoad;						// cumulative load of the route
		double 	routeLength;					// cost of the route (= total distance)
	};
	
	Route 	routes[MAX_ROUTES];				// array containing all the routes;
	int 	nRoutes;

	// unordered_map<const VRP_vertex*, Node*> vpointer2vnode;			// hash table containing as key a pointer to an instance vertex and value the pointer to the corresponding node, if the vertex is part of the solution; otherwise the element is not in the container

	double currentTime;						// in dynamic context, the current time; it allows to decide whether a node is moveable, free/fixed, left/unleft

	Node 	node_pool[NODE_POOL_SIZE];		// array containing available nodes - avoid dynamic memory allocation
	vector<Node*> node_pool_free_addr;		// this vector contains pointers of each free available node in the array node_pool
	/* node pool management */
	Node*	getNodePtrFromPool();
	void	pushNodePtrBackToPool(Node* node_ptr);

	/* protected accessors */
	Node&	pos2node(int route, int pos, bool routeZero=false) const;	// returns a reference pointer to the node in position pos in route 

	/* misc. non-public stuffs */
	void 	updateRouteInfos(int route=-42, bool routeZero=false, bool newCurrentTime=false);		// update informations of a specific route (all route if route=42); routeZero must be set to TRUE if one wants to apply on temporary route 0; if newCurrentTime==true then we do not touch to the arrival/service/departure times, only the fixed/left predicates etc
	double  travelTimeToNextNodeFrom(const Node& node) const;
	// void 	removeUnloadDepotNode(Node& node);
	// void 	insertUnloadDepotNode_after(const VRP_vertex* udepot, Node& node);
	int 	nMoveableVertices;
	int 	nFreeVertices;
	int 	nFreeWaitingVertices;
	int 	nUNLEFTWaitingVertices;
	void	checkParamRoute(int route, int routeZero, bool enforceRouteZero) const;
	void	checkParamPosition(int route, int pos, bool routeZero, bool exclude_depot, bool exclude_fixed, bool UNLEFTonly=false) const;
	void	checkParamSize(int size) const;

	// the following functions are directly called by their public interface (see public section for their description), because route 0 should not exist from the public point of view
	void 	insertVertex(const VRP_vertex* vertex, int to_route, int to_pos, bool copyingSol, int timeslot = -42);	// see description in public interface; copyingSol==true when called from copy operator=
	void 	moveSegment(int from_route, int from_pos_1, int from_pos_2, int to_route, int to_pos, bool routeZero, bool updateRoutes);	
	int 	getRouteSize(int route, bool routeZero) const;
	void	addRoute(const VRP_vertex* depot, const VRP_VehicleType* vehicle, bool init);	// see description below; init = true means that it is called in constructor, and used to create the dummy route 0

public:

	// VRP_routes(const VRP_instance& instance, const VRP_vertex* depot);
	VRP_routes(const VRP_instance& instance);
	VRP_routes& operator = (const VRP_routes& r);			// copy operator
	bool operator == (const VRP_routes& other) const;
	bool operator != (const VRP_routes& other) const { return ! operator==(other);}
	~VRP_routes();

	/* altering functions */
	void	addRoute(const VRP_vertex* depot, const VRP_VehicleType* vehicle);		// add a new route with vertex 'depot' as depot, and the given vehicle characteristics
	// void 	activateAutomaticVehicleUnload(int route, const VRP_vertex& depot);		// the vehicle assigned to the route will automatically (the corresponding node is automatically inserted/removed at the adequate positions at each updateRouteInfos()) make round trips to the given depot whenever its capacity is full
	void 	activateAutomaticVehicleUnload(int route);									// the vehicle assigned to the route will automatically (the corresponding node is automatically inserted/removed at the adequate positions at each updateRouteInfos()) make round trips to the given depot whenever its capacity is full
	void 	clearRoute(int route);																// clear the route by removing all the vertices from it (constant time, that is faster than calling removeVertex() n times)
	void	removeAllRoutes();																	// clear and delete all routes 
	void 	copyRouteFrom(const VRP_routes& other_routes, int route);

	void 	insertVertex(const VRP_vertex* vertex, int to_route, int to_pos);					// insert a new vertex in a given route at a given position; a new node will be created
	void 	insertVertexLastPos(const VRP_vertex* vertex, int to_route);						// insert a new vertex in a given route at a the end of the route (just before the ending depot node -- that is, at its position); a new node will be created
	void 	removeVertex(int route, int pos, bool checkFixed=true);								// removes the vertex at position pos from a given route; node will be deleted (but not the vertex object of course) -- setting notifyRouteCHange to false is useful whenever one wants to remove several nodes from the same route (but then one MUST proceed in decreasing order of the node positions !!) 
	void 	moveSegment(int from_route, int from_pos_1, int from_pos_2, int to_route, int to_pos);	// moves a sequence of vertices, from a route into another
	void 	moveVertex(int from_route, int from_pos, int to_route, int to_pos);					// idem with one vertex
	void	swapSegments(int route_1, int pos_1_1, int pos_1_2, int route_2, int pos_2_1, int pos_2_2);
	void	swapVertices(int route_1, int pos_1, int route_2, int pos_2);
	void 	invertSegment(int route, int pos_1, int pos_2);										// inverts a vertex sequence beginning at from_pos_1 and ending at from_pos_2 ; e.g.  D-->A-->B-->C-->D-->E-->D, invertSegment(B,D) produces: D-->A-->D-->C-->B-->E-->D
	void	setWaitingTime(int route, int pos, double w);										// set the waiting time to w at the given waiting vertex
	double	removeMaxWaitingTime(int route, int pos);							// reomve as much waiting time as possible (w.r.t. current time) from the given waiting vertex; returns the waiting time BEFORE removal

	
	/* selection functions */
	void 	selectRandomMoveableVertex(int* found_route, int* found_pos, int route) const;			// selects a random MOVEABLE (= free && non-depot) node (stores its route and position in arguments)
	void	selectRandomMoveableSegment(int* found_pos1, int* found_pos2, int route, int min_size, int max_size) const;	// select a random MOVEABLE segment of nodes in route, s.t. min_size ≤ length ≤ max_size
	void 	selectRandomFreeVertex(int* found_route, int* found_pos, int route) const;				// selects a random FREE (= non-fixed) node (stores its route and position in arguments)
	void 	selectRandomFreeWaitingVertex(int* found_route, int* found_pos, int route) const;		// selects a random FREE (= non-fixed) node (stores its route and position in arguments)
	void 	selectRandomUNLEFTWaitingVertex(int* found_route, int* found_pos, int route) const;		// selects a random UNLEFT node (stores its route and position in arguments)


	/* querying accessors */
	const VRP_VehicleType&	getVehicleTypeFromRoute(int route) const;			// returns the vehicle type associated to a route
	const VRP_vertex&		getVertexFromPos(int route, int pos) const;			// returns the vertex associated to a node
	const VRP_request&		getRequestFromPos(int route, int pos) const;		// returns the request associated to a vertex at a node
	bool 					getAttributes(const VRP_vertex& v, double *waiting_time, int *pos, int *veh, double *arrival_time, double *service_time, double *departure_time, bool *fixed, bool *left, bool *served) const;	// return false is vertex v is not in any route; otherwise, set all its current attributes and return true
	int 					getRouteSize(int route) const;						// returns the number of vertices in the route, including the depot (only the ending depot !)
	double					getWaitingTime(int route, int pos) const;			// returns the waiting time planned at vertex in position pos in route
	int 					getNumberRoutes() const;							// returns the number of routes
	int 					getNumberNonEmptyRoutes(int route=-42) const; 		// returns the number of routes that are not empty that is, containing at least one node between starting and ending depot; ; if parameter route is set, then returns 1 if the route is not empty (0 otherwise)
	int 					getNumberModifiableRoutes(int route=-42) const; 	// returns the number of routes that are modifiable that is, containing at least one moveable node between starting and ending depot; ; if parameter route is set, then returns 1 if the route is modifiable (0 otherwise)
	int 					getNumberFreeRoutes(int route=-42) const;			// returns the number of routes that are not fixed that is, for which the ending depot is not fixed; if route is set, then returns 1 if the route is not fixed (0 otherwise)
	int 					getNumberFreeVertices(int route=-42) const;			// returns the number of vertices (including ending DEPOTs) that are not fixed in all routes (or in a specific route if route is set to >0)
	int 					getFirstFreePos(int route) const;					// returns the position of the first non fixed node in the route (returns -1 if no such)
	int 					getNumberMoveableVertices(int route=-42) const;		// returns the number of vertices (EXCLUDING ending DEPOTs) that are not fixed in all routes (or in a specific route if route is set to >0)
	int 					getNumberFreeWaitingVertices(int route=-42) const;	// returns the number of WAITING vertices that are not fixed in all routes (or in a specific route if route is set to >0)
	int 					getNumberUNLEFTWaitingVertices(int route=-42) const;// returns the number of WAITING vertices that are still UNLEFT, meaning that either the vehicle is still waiting on it or it has not been visited yet; useful to increase the waiting time of a waiting vertex
	double					getArrivalTimeAtPos(int route, int pos) const;		
	double					getServiceTimeAtPos(int route, int pos) const;		
	double					getDepartureTimeAtPos(int route, int pos) const;	
	double					getRouteLength(int route = -42) const;
	double					getRouteCost(int route = -42) const;
	bool 					automaticVehicleUnload(int route) const { return routes[route].unload_depot; }

	/* dynamic context */
	void	setCurrentTime(double t);					// sets the current time to t 
	double	getCurrentTime() {return currentTime;}		// sets the current time to t 
	void 	getCurrentAction(int route, int *pos, ActionType *action, double *start_time, double *end_time, double time=-42);		// in route according to a given time (current time if unspecified), get the action currently performed by the vehicle
	void 	getNextPlannedAction(int route, int *pos, ActionType *action, double *start_time, double *end_time, double time=-42);	// same but the next planned action from given time, instead of the current action
	void 	insertOnlineVertex(const VRP_vertex* vertex, int to_route, int to_pos, int timeslot);									// insert a vertex of type REGULAR_ONLINE in a given route at a given position; a new node will be created, and the request considered is specified using timeslot
	bool 	isFixed(int route, int pos) const;


	/* Misc */
	AssertLevel 	getAssertLevel();
	void 			setAssertLevel(AssertLevel level);
	WaitingStrategy getWaitingStrategy();
	void 			setWaitingStrategy(WaitingStrategy ws);

	string& toString(bool verbose=false) const;

};



#endif

