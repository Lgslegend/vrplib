/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



/* LS_Program +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	
	Here we define both heuristic and meta heuristic Local Search algorithms 
	for a solutions of template type Solution

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/


#ifndef LSBase_H
#define	LSBase_H

#include <iostream>

#include "NM.h"


enum CallbackReturnType {
	NONE		,	// nothing happens (nothing else than what is defined in the callback function)
	STOP_LS			// if the callback function returns STOP_LS, it stops the LS procedure
};

/*
	class LS_Program

	This is the main template class. It is an ABSTRACT class, which must be derived.
	By implementing the virtual methods, a class that extends LS_Program defines the (meta)heuristic. 
	The only method implemented here is "run"; it drives the local search according to the definitions of virtual methods.
*/

template <class Solution, class NM> class LS_Program {
private:
	CallbackReturnType (*end_iteration_callback)			(Solution &, double &, Solution &, double &) = NULL; 	// function to be called after each iteration (e.g. checking some time limits, online requests, printing stuffs, ..)
	CallbackReturnType (*new_bestSolution_callback)			(Solution &, double&) = NULL; 		// function to be called each time a new best solution is found
	CallbackReturnType (*new_incumbentSolution_callback)	(Solution &) = NULL; 				// function to be called each time a new incumbent solution is accepted
	CallbackReturnType (*restart_callback)					(Solution &) = NULL; 				// function to be called each time a restart occurs 
	CallbackReturnType (*time_callback)						(Solution &, double &, Solution &, double &, double) = NULL; 	// function to be called every every t seconds (e.g. recording cost of best/incumbent solution, ..)
	double time_callback_trigger;
protected:
	double 			   (*evaluation_callback)				(Solution &) = NULL; 				// function to be called to evaluate a solution
	int current_distance;
	int iter, max_iter;
	double timeout_sec, elapsed_time;
public:

	LS_Program();	// constructor - callback can be set to a function that must be called at the end of every iteration of the search
	int run(Solution &solution, NM &nm, bool verbose = false);			// apply the LS algorithm defined in the function in order to improve solution, based on neighborhood manager nm (which must be coupled with solution); returns the number of iterations performed
	void setTimeOut(double cpuSeconds);								// sets the timeout for the next call to run() to numberCPUSeconds
	bool terminationCondition() const; 									// decides whether the LS procedure should stop or not
	// Callbacks
	// void set_EndOfIteration_Callback(CallbackReturnType (*callback)(Solution& bestSolution, Solution& incumbentSolution)) 	{end_iteration_callback  		= callback;}
	void set_EndOfIteration_Callback(CallbackReturnType (*callback)(Solution& bestSolution, double& best_eval, Solution& incumbentSolution, double& incumbent_eval)) 	{end_iteration_callback  		= callback;}
	void set_NewBestSolution_Callback(CallbackReturnType (*callback)(Solution& bestSolution, double& best_eval)) 			{new_bestSolution_callback  	= callback;}
	void set_NewIncumbentSolution_Callback(CallbackReturnType (*callback)(Solution& incumbentSolution))	 					{new_incumbentSolution_callback = callback;}
	void set_Restart_Callback(CallbackReturnType (*callback)(Solution& bestSolution))	 									{restart_callback  				= callback;}
	void set_Time_Callback(CallbackReturnType (*callback)(Solution& bestSolution, double& best_eval, Solution& incumbentSolution, double& incumbent_eval, double elapsed_time), double t)	{time_callback  				= callback;		time_callback_trigger = t;}
	void set_Evaluation_Callback(double (*callback)(Solution& solution))	 												{evaluation_callback  			= callback;}
	// Function that MUST be implemented in derived classes (i.e. in metaheuristics)
	// virtual bool 	acceptanceCriterion(const Solution& candidateSolution, const Solution& incumbentSolution) const =0;	// tells whether candidate solution must be accepted, according to the incumbent solutions
	virtual bool 	acceptanceCriterion(double evalCandidate, double evalIncumbent) const =0;	// tells whether candidate solution must be accepted, according to the incumbent solutions
	virtual double 	computeSolutionEval(const Solution& solution) const =0;		
	virtual bool 	shouldWeRestart() const = 0;		// tells whether the search should restart from the best solution encountered so far
	
	// Functions that COULD be implemented in derived classes
	virtual void 	restart() {}						// informs the meta heuristic that a restart occured
	virtual void 	improvement() {} 					// informs the meta heuristic that a new improving solution has been found
	virtual void	iterationDone() {}					// informs the meta heuristic that an iteration as been performed
};


/* 
	Basic Local Search
	We conLrol:
		- the maximal number of iterations (max_iter)
		- a simple diversification mechanism (a degrading solution is accepted with probability of diversification%)
		- restart heuristic: the search restarts at the best solution whenever the number of accepted neighbors since the last update of the best solution exceeds max_distance
*/
template <class Solution, class NM> 
class LS_Program_Basic : public LS_Program<Solution, NM> {
protected:
	int diversification;
	int max_distance;
public:
	LS_Program_Basic() : LS_Program<Solution, NM>() {}

	double computeSolutionEval(const Solution & solution) const {
		// return solution.getCost() + 10000 * solution.getNumberViolations();
		// return solution.getCost() + 10000 * solution.getWeightViolations();
		ASSERT(solution.getType() != VRPTYPE_NO_SET, "");
		if (solution.getType() == SS_VRPTW_CR && solution.getCost() == numeric_limits<double>::max())				// for SS-VRPTW-CR, solution.getCost() would return numeric_limits<double>::max() in case of infeasibility; but then it screws the LS process that will never prefer another less violated solution
			return 10000 + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();
		
		ASSERT(solution.getCost() < numeric_limits<double>::max(), "");
		return solution.getCost() + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();
	}
	bool acceptanceCriterion(double candidate_eval, double incumbent_eval) const {
		if (candidate_eval < incumbent_eval) 
			return true;
		else return (rand() % 100 + 1 <= diversification);	// % diversification
		// return false;
	}
	bool shouldWeRestart() const {
		return this->current_distance >= max_distance;
	}
	int run(Solution &solution, NM &nm, long long max_iter, int diversification = 10, int max_distance = 2500, bool verbose = false) {
		this->max_iter = max_iter;
		this->max_distance = max_distance;
		this->diversification = diversification;
		return LS_Program<Solution, NM>::run(solution, nm, verbose);
	}
};


/*
	Simulated Annealing
*/
template <class Solution, class NM> 
class LS_Program_SimulatedAnnealing : public LS_Program<Solution,NM> {
protected:	
	double 	temp;
	double 	init_temp, initial_init_temp;
	double 	min_temp, initial_min_temp;
	double 	lindecrease;
	double 	cooling_factor;
	int 	nb_restarts;
public:
	LS_Program_SimulatedAnnealing() : LS_Program<Solution,NM>() {}

	int run(Solution &solution, NM &nm, long long max_iter, double init_temp, double cooling_factor, bool verbose = false) {
		this->max_iter = max_iter;
		this->init_temp = init_temp;
		this->min_temp = init_temp / 100;
		//lindecrease = (init_temp-min_temp)/500;
		this->cooling_factor = cooling_factor;
		initial_min_temp = min_temp;
		initial_init_temp = init_temp;

		nb_restarts = 0;
		temp = init_temp;
		return LS_Program<Solution,NM>::run(solution, nm, verbose);
	}
	double computeSolutionEval(const Solution & solution) const {

		// return solution.getCost() + 10000 * solution.getNumberViolations();
		ASSERT(solution.getType() != VRPTYPE_NO_SET, "");
		if (solution.getType() == SS_VRPTW_CR && solution.getCost() == numeric_limits<double>::max())				// for SS-VRPTW-CR, solution.getCost() would return numeric_limits<double>::max() in case of infeasibility; but then it screws the LS process that will never prefer another less violated solution
			return 10000 + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();
		
		ASSERT(solution.getCost() < numeric_limits<double>::max(), "");
		// cout << endl << solution.getCost() << "   10000 * solution.getWeightViolations()" << 10000 * solution.getWeightViolations() << "   1000000 * solution.getNumberViolations()" << 1000000 * solution.getNumberViolations() << endl;
		return solution.getCost() + 10000 * solution.getWeightViolations() + 1000000 * solution.getNumberViolations();
	}
	// bool acceptanceCriterion(const Solution& candidateSolution, const Solution& incumbentSolution) const {
	bool acceptanceCriterion(double evalCandidate, double evalIncumbent) const {
		// double evalCandidate = computeSolutionEval(candidateSolution);
		// double evalIncumbent = computeSolutionEval(incumbentSolution);

		// cout << endl << evalCandidate << " - " << evalIncumbent;
		
		if(evalCandidate == evalIncumbent)
			return false;

		double probability = 0.0;
		if(temp > 0.0)
			probability = exp( -(1-evalIncumbent/evalCandidate) / temp );	// relative probability
		//float probability = exp( -(eval_candidate-eval_curr) / temp );	// probability
		//float probability = exp( -(1-currentSolution.getCost()/candidateSolution.getCost()) / temp );

		// cout << "\ttemp: " << temp << "\t min t°:" << min_temp << "\t incumbent: " << evalIncumbent << " \t candidate: " << evalCandidate << " \t proba: " << probability;// << endl;
		if (evalCandidate < evalIncumbent || rand() % 100000 + 1 <= probability*100000 ) {
			// cout << " \t accepted" << endl;
			return true; 
		}
		else {
			// cout << endl;
			return false;
		}
	}
	void iterationDone() {
		updateTemp();
	}
	void updateTemp() {
		temp = max(min_temp, temp * cooling_factor); 	// exponenitial decrease
		//temp =  max(min_temp, temp - lindecrease);	// linear decrease
	}
	bool shouldWeRestart() const {
		return temp <= min_temp;
	}
	void restart() {
		nb_restarts++;
		
		init_temp /= 2;
		min_temp /= 2;
		temp = init_temp;

		if (nb_restarts % 10 == 0) {
			//cout << "R" << flush;
			init_temp = initial_init_temp;
			min_temp = initial_min_temp;
		}
	}
};



#include "LS_Program.cpp"


#endif


