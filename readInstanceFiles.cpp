/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/



#include <iostream>
#include <fstream>	// file I/O
#include <sstream>	// ostringstream
#include <cstdlib>	// rand
#include <ctime>	// time
#include <cmath>	// sqrt
#include <cstring>	// memcpy
#include <limits>   // numeric_limits
#include <iomanip>	// setw, setfill
#include <algorithm>// std::max
#include <limits> 	// numeric_limits

#include "VRP_instance.h"
#include "tools.h"
















/* Handles instance files of Cordeau-Laporte in data/vrptw/old 	-	static instances */
VRP_instance& VRP_instance_file::readInstanceFile_CordeauLaporte_VRPTW_old(const char *filename) {
	int i, nRegions, nVeh, Q;
	double x, y;

	std::string line;
	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	instance_file >> i; 						// skip first integer
	instance_file >> nVeh;						// retrieve number of vehicles
	instance_file >> nRegions; nRegions++;		// retrieve instance size: the number of regions (++ for the depot) 

	std::getline(instance_file, line); // skip end of line

	instance_file >> i;			// skip int
	instance_file >> Q;			// retrieve max capacity

	// double veh_velocity = 1.0;
	int horizon = numeric_limits<int>::max();

	VRP_instance* instance = new VRP_instance(nRegions, horizon);
	instance->_nVeh = nVeh;
	// instance->_Q = Q;
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), Q);
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	// cout << "instance file to vertex : " << depot.toString() << endl;

	// DEPOTS
	instance_file >> depot.id_instance_file;
	instance_file >> x; 							// retrieve x coordinate
	instance_file >> y; 							// retrieve y coordinate
	instance->setCoordRegion(0, x, y);

	instance_file >> depot.getRequest_mutable().add_service_time;	// retrieve service duration
	instance_file >> depot.getRequest_mutable().demand;				// retrieve demand
	instance_file >> i; 							// skip int
	instance_file >> i; 							// skip int
	//instance_file >> i; 						// skip int 	---->    /!\ One int less to skip in the depot line
	instance_file >> depot.getRequest_mutable().e;				// retrieve start TW
	instance_file >> depot.getRequest_mutable().l;				// retrieve end TW

	instance->H = depot.getRequest().l;
	// cout << "   " << depot.id_instance_file << "   " << x << "   " << y << "   " << depot.request_set[0].duration << "   " << depot.request_set[0].demand << "   " << depot.request_set[0].e << "   " << depot.request_set[0].l << endl;

	// REGULAR vertices
	for (int j = 1; j < nRegions; j++) {
		VRP_vertex& vertex = instance->addVertex(j, VRP_vertex::REGULAR);
	
		// cout << "instance file to vertex : " << vertex.toString();
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		instance->setCoordRegion(j, x, y);

		instance_file >> vertex.getRequest_mutable().add_service_time;	// retrieve service duration
		instance_file >> vertex.getRequest_mutable().demand;			// retrieve demand
		instance_file >> i; 							// skip int
		instance_file >> i; 							// skip int
		instance_file >> i; 							// skip int 	
		instance_file >> vertex.getRequest_mutable().e;				// retrieve start TW
		instance_file >> vertex.getRequest_mutable().l;				// retrieve end TW

		// cout << "   " << vertex.id_instance_file << "   " << x << "   " << y << "   " << vertex.requests_ts[0].duration 
		// 		<< "   " << vertex.requests_ts[0].demand << "   " << vertex.requests_ts[0].e 
		// 		<< "   " << vertex.requests_ts[0].l << endl;
	}

	instance_file.close();

	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);

	return *instance;
}


















/* Instance files used for the SS-VRPTW-CR, EvoSTOC conference paper */
VRP_instance& VRP_instance_file::readInstanceFile_SSVRPTW_CR(const char *filename, double scale) {
	int nRegions, Q, nVeh, nCustomerVertices, nWaitingVertices, horizon, nTimeSlots, orig_horizon, veh_velocity;
	double x, y;
	std::string line, word;

	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	instance_file >> word;	// skip word "nWaitingVertices:"
	instance_file >> nWaitingVertices;
	instance_file >> word;	// skip word "nCustomerVertices:"
	instance_file >> nCustomerVertices;
	nRegions = nCustomerVertices + nWaitingVertices + 1;	// +1 for the depot region
	instance_file >> word;	// skip word "Horizon:"
	instance_file >> orig_horizon; horizon = ceil(orig_horizon / scale);
	instance_file >> word;	// skip word "nTimeSlots:"
	instance_file >> nTimeSlots;	ASSERT(nTimeSlots <= horizon, "scale too small for the number of time slots !");
	instance_file >> word; // skip word "nVehicles:"
	instance_file >> nVeh;
	instance_file >> word; // skip word "vehicleCapacity:"
	instance_file >> Q;
	instance_file >> word; // skip word "vehicleVelocity:"
	instance_file >> veh_velocity;		veh_velocity *= scale;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	std::getline(instance_file, line); // skip line "Arguments: ..."
	std::getline(instance_file, line); // skip line	"TS: ..."
	std::getline(instance_file, line); // skip line "Time: ..."
	std::getline(instance_file, line); // skip line

	// Q = 0;
	// nVeh = 1;
	// cout << filename << endl;
	VRP_instance* instance = new VRP_instance(nRegions, horizon, nTimeSlots, orig_horizon, scale);
	instance->_nVeh = nVeh;
	// instance->_Q = Q;
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), Q, veh_velocity);
	// instance->_vehVelocity = veh_velocity;


	/* DEPOT and WAITING VERTICES */
	// depot
	std::getline(instance_file, line); // skip line
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	instance_file >> depot.id_instance_file;
	instance_file >> x; 							// retrieve x coordinate
	instance_file >> y; 							// retrieve y coordinate
	instance->setCoordRegion(0, x, y);
	depot.getRequest_mutable().l = horizon;
	std::getline(instance_file, line); // skip end of line
	// waitings	
	for (int j = 1; j <= nWaitingVertices; j++) {
		VRP_vertex& vertex = instance->addVertex(j, VRP_vertex::WAITING);
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		instance->setCoordRegion(j, x, y);
		std::getline(instance_file, line); // skip end of line
	}


	
	/* CUSTOMER REGIONS */
	std::getline(instance_file, line); // skip line
	std::getline(instance_file, line); // skip line
	int nReq = 0;
	for (int j = nWaitingVertices+1; j <= nWaitingVertices+nCustomerVertices; j++) {
		VRP_vertex& vertex = instance->addVertex(j, VRP_vertex::CUSTOMER);
		instance_file >> vertex.id_instance_file;
		instance_file >> x; 							// retrieve x coordinate
		instance_file >> y; 							// retrieve y coordinate
		instance->setCoordRegion(j, x, y);

		for (int ts=1; ts<=nTimeSlots; ts++) {
			instance_file >> vertex.getRequest_mutable(ts).p;
			if (vertex.getRequest(ts).p > 0) nReq++;
		}

		std::getline(instance_file, line); // skip end of line
	}
	std::getline(instance_file, line); // skip line


	/* REQUESTS ATTRIBUTES */
	instance_file >> word;	// skip word "nRequests"
	int nRequests;
	instance_file >> nRequests;
	ASSERT(nRequests == nReq, "");
	instance->nPotentialRequests = nRequests;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	for (int i=0; i<nReq; i++) {
		int req_id, region, ts;
		instance_file >> req_id;
		instance_file >> region;
		instance_file >> ts;					ASSERT(ts <= nTimeSlots, "ts=" << ts);
		VRP_request& req = instance->getVertexFromRegion_mutable(VRP_vertex::CUSTOMER, region)->getRequest_mutable(ts);
		req.id_instance_file = req_id;
		req.id = i+1;							ASSERT(req.id <= nRequests, "");
		req.revealTS = ts;
		instance_file >> req.revealTime; 
		req.revealTime_min = req.revealTime;
		req.revealTime_max = req.revealTime;
		instance_file >> req.demand;
		// req.demand = 0;
		instance_file >> req.add_service_time; 
		instance_file >> req.e; 
		instance_file >> req.l; 
		// req.revealTime = instance->ts2time(ts);

		req.revealTime 		= ceil(req.revealTime / scale);
		req.revealTime_min 	= ceil(req.revealTime_min / scale);
		req.revealTime_max 	= max(req.revealTime_min, ceil(req.revealTime_max / scale));
		req.add_service_time= ceil(req.add_service_time / scale);
		req.e 				= ceil(req.e / scale);
		req.l 				= max(req.e, ceil(req.l / scale));
		
		std::getline(instance_file, line); // skip end of line
	}




	instance_file.close();

	instance->setIntegerTravelTimes();
	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);		// integer_travel_times = true ---> ceil each distance to integer

	// cout << "Warning: readInstanceFile_SSVRPTW_CR() has hardcoded modifications" << endl;

	return *instance;
}






















/* Instance files used for the DS-VRPTW, from Bent & Van Hentenryck */
// WORK IN PROGRESS !
VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(const char *filename) {

	std::ifstream instance_file;
	instance_file.open(filename);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename << endl;
		exit (8);
	}

	int nCustomerRegions, Q, nVeh, horizon = 240, nTimeSlots = 3;
	double x, y;
	std::string line, word;

	instance_file >> word; 				// skip word "Customers:"
	instance_file >> nCustomerRegions;
	instance_file >> word; 				// skip word "Capacity:"
	instance_file >> Q;
	instance_file >> word; 				// skip word "Time"
	instance_file >> word; 				// skip word "Bins"
	instance_file >> word; 				// skip number
	instance_file >> word; 				// skip word "Vehicles"
	instance_file >> nVeh;
	std::getline(instance_file, line); 	// skip end of line

	std::getline(instance_file, line); 	// skip line
	std::getline(instance_file, line); 	// skip line "Cust   X     Y    [...]"


							// 	(int nRegions, int H=0, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	VRP_instance* instance = new VRP_instance(1+nCustomerRegions, horizon, nTimeSlots, horizon);
	instance->_nVeh = nVeh;
	// instance->_Q = Q;
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), Q);


	// DEPOT
	VRP_vertex& depot = instance->addVertex(0, VRP_vertex::DEPOT);
	instance->addVertex(0, VRP_vertex::WAITING, -1);	// also add a waiting vertex for the same region, it could be useful
	instance_file >> word; 								// skip word "D"
	depot.id_instance_file = -1;
	instance_file >> x; 								// retrieve x coordinate
	instance_file >> y; 								// retrieve y coordinate
	instance->setCoordRegion(0, x, y);
	instance_file >> depot.getRequest_mutable().demand;		// retrieve demand
	int e, l;
	instance_file >> e; e++;
	depot.getRequest_mutable().e = e;						// retrieve start TW
	instance_file >> l; l++;
	depot.getRequest_mutable().l = l;						// retrieve end TW
	ASSERT(l - e == 240, "");
	instance_file >> depot.getRequest_mutable().add_service_time;	// retrieve service duration
	std::getline(instance_file, line); 								// skip end of line

	// CUSTOMER REGIONS
	for (int i=1; i <= nCustomerRegions; i++) {
		int id;
		instance_file >> id;
		VRP_vertex& vertex = instance->addVertex(i, VRP_vertex::REGULAR_ONLINE, id);
		instance_file >> x; 								// retrieve x coordinate
		instance_file >> y; 								// retrieve y coordinate
		instance->setCoordRegion(vertex.id_instance_file + 1, x, y);

		instance->addVertex(vertex.id_instance_file + 1, VRP_vertex::WAITING, vertex.id_instance_file);	// also add a waiting vertex for the same region, it could be useful

		int demand, e, l, duration;
		instance_file >> demand;		// retrieve demand
		instance_file >> e; e++;		// retrieve start TW
		instance_file >> l;	l++;		// retrieve end TW
		instance_file >> duration;		// retrieve service duration

		for (int ts = 0; ts <= nTimeSlots; ts++) {
			vertex.getRequest_mutable(ts).demand 			= demand;
			vertex.getRequest_mutable(ts).e 				= e;
			vertex.getRequest_mutable(ts).l 				= l;
			vertex.getRequest_mutable(ts).add_service_time 	= duration;

			vertex.getRequest_mutable(ts).revealTS 		 = ts;
			vertex.getRequest_mutable(ts).revealTime_min = ((ts-1) * 80 + 1 ) * (ts > 0);
			if (ts == 0) vertex.getRequest_mutable(ts).revealTime_max = 0;
			// revealTime_max is defined below, because it depends on the time needed to reach the request location from the depot


			instance_file >> vertex.getRequest_mutable(ts).p;		// retrieve request probability
			vertex.getRequest_mutable(ts).appeared = false;	
		}

		std::getline(instance_file, line); // skip end of line
	}
	std::getline(instance_file, line); // skip line

	// KNOWN REQUESTS
	instance_file >> word; 				// skip word "Known"
	instance_file >> word; 				// skip word "Requests"
	int n_deterministic_requests;
	instance_file >> n_deterministic_requests;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	for (int i = 0; i < n_deterministic_requests; i++) {
		int region; 
		instance_file >> region; region++;
		instance->set_OnlineRequest_asAppeared(region, 0);
		std::getline(instance_file, line); // skip end of line
	}
	std::getline(instance_file, line); // skip line


	veh_type.preComputeEuclideanDistances(instance->coordX, instance->coordY);


	for (int i=1; i <= nCustomerRegions; i++) {
		VRP_vertex& depot = *instance->getVertexFromRegion_mutable(VRP_vertex::DEPOT, 0);
		VRP_vertex& vertex = *instance->getVertexFromRegion_mutable(VRP_vertex::REGULAR_ONLINE, i);
		for (int ts = 1; ts <= nTimeSlots; ts++) {
			int latest_dep_time = horizon - veh_type.travelTime(depot, vertex) + vertex.getRequest(ts).add_service_time + veh_type.travelTime(vertex, depot);
			vertex.getRequest_mutable(ts).revealTime_max = min(latest_dep_time, (int) vertex.getRequest_mutable(ts).revealTime_min + 79) ;
		}
	}




	// ONLINE REQUESTS
	instance_file >> word; 				// skip word "Unknown"
	instance_file >> word; 				// skip word "Requests"
	int n_online_requests;
	instance_file >> n_online_requests;
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip line
	for (int i = 0; i < n_online_requests; i++) {
		int region, reveal_time, time_slot;
		instance_file >> region; region++;
		instance_file >> reveal_time; reveal_time++;
		if (reveal_time <= 80) time_slot = 1;
		else if (reveal_time <= 160) time_slot = 2;
		else {ASSERT(reveal_time <= 240, ""); time_slot = 3;}
		
		instance->addAppearingOnlineRequest(region, time_slot, reveal_time);
		
		std::getline(instance_file, line); // skip end of line
	}


	instance_file.close();	


	return *instance;
}
























/* Instance files used for the DS-VRPTW, from Rizzo & Saint-Guillain */
VRP_instance& VRP_instance_file::readInstanceFile_DS_VRPTW__Rizzo(const char *oc_directory, int scenario_number, bool use_lockers) {
	const int NB_TU_PER_HOUR = 60;

	/************ open all instance files *************/
	
	string oc_dir_path(oc_directory);
	if (oc_dir_path[oc_dir_path.size()-1] == '/') oc_dir_path = oc_dir_path.substr(0, oc_dir_path.size()-1);
	// string dir_name = oc_dir_path.substr(oc_dir_path.rfind('/') + 1);

	std::stringstream ss;
	// ss.str(""); ss << oc_dir_path << "/" << dir_name << "-behaviors.txt";
	ss.str(""); 
	if (use_lockers)
		ss << oc_dir_path << "/lockers-yes/behaviors.txt";
	else 
		ss << oc_dir_path << "/lockers-no/behaviors.txt";
	std::ifstream cust_behaviors; cust_behaviors.open(ss.str());
	if (cust_behaviors.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }
	
	// // ss.str(""); ss << oc_dir_path << "/" << dir_name << "-network-distances.txt";
	ss.str(""); ss << oc_dir_path << "/../distances.txt";
	std::ifstream travel_distances; travel_distances.open(ss.str());
	if (travel_distances.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }
	
	// // ss.str(""); ss << oc_dir_path << "/" << dir_name << "-network-distances.txt";
	// ss.str(""); ss << oc_dir_path << "/../locations.txt";
	// std::ifstream locations; locations.open(ss.str());
	// if (locations.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }
	
	// ss.str(""); ss << oc_dir_path << "/" << dir_name << "-times.txt";
	ss.str(""); ss << oc_dir_path << "/../travel-times.txt";
	std::ifstream travel_times; travel_times.open(ss.str());
	if (travel_times.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }

	ss.str(""); ss << oc_dir_path << "/../service-times.txt";
	std::ifstream service_times; service_times.open(ss.str());
	if (service_times.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }

	// ss.str(""); ss << oc_dir_path << "/" << dir_name << "-instances/" << dir_name << "-IN-" << setfill('0') << setw(2) << scenario_number << ".txt";
	ss.str(""); ss << oc_dir_path << "/instances/IN-" << setfill('0') << setw(2) << scenario_number << ".txt";
	std::ifstream scenario_file; scenario_file.open(ss.str());
	if (scenario_file.fail()) { std::cerr << endl << " Error: Unable to open " << ss.str() << endl; exit (8); }



	/************ retrieve horizon and graph specifications from customers-behaviors.txt *************/
	bool instanceID_in_operational_context[MAX_ID];
	for (int i=0; i<MAX_ID; i++) instanceID_in_operational_context[i] = false;
	int nCustomerRegions, horizon, nTimeSlots;
	std::string line, word;

	cust_behaviors >> word;		// skip word NODES:
	cust_behaviors >> nCustomerRegions;
	std::getline(cust_behaviors, line); // skip end of line
	cust_behaviors >> word;	cust_behaviors >> word;		// skip words TIME HORIZON:
	double horizon_nb_hours, horizon_start_hour; 
	cust_behaviors >> horizon_start_hour;
	cust_behaviors >> horizon_nb_hours;
	std::getline(cust_behaviors, line); // skip end of line
	for (int i=0; i<4; i++) cust_behaviors >> word;		// skip words TIME BUCKETS IN HORIZON:
	cust_behaviors >> nTimeSlots;

	horizon = horizon_nb_hours * NB_TU_PER_HOUR;	// ==> one time unit per minute
	// cout << "Horizon: " << horizon_start_hour << " to " << horizon_start_hour + horizon_nb_hours << "(" << horizon << " TU)" << endl;
									// 	(int nRegions, int H=0, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	VRP_instance* instance = new VRP_instance(2+nCustomerRegions, horizon, nTimeSlots, horizon);	// ==> 2 depots (otherwise: 1+nCustomerRegions)
	
	/****  vehicles  ****/
	VRP_VehicleType& veh_type_van = instance->addVehicleType(string("van"), 700);
	VRP_VehicleType& veh_type_bike = instance->addVehicleType(string("bike"), 70);
	// cout << instance->toStringVehicleTypes() << endl;

	/**** time slots ****/
	std::vector<int> ts_start(nTimeSlots+1, -1), ts_end(nTimeSlots+1, -1);
	for (int ts=1; ts <= nTimeSlots; ts++) {
		int ts_start_hour, ts_len;
		cust_behaviors >> ts_start_hour;
		ts_start[ts] = (ts_start_hour - horizon_start_hour) * NB_TU_PER_HOUR + 1;
		cust_behaviors >> ts_len;
		ts_end[ts] = ts_start[ts] + NB_TU_PER_HOUR*ts_len -1;
		// cout << "Time Slot: " << ts_start[ts] << " -> " << ts_end[ts] << endl;
		cust_behaviors >> word;		// skip word [09:00-17:00]
	} std::getline(cust_behaviors, line); // skip end of line
	ts_start[0] = 0;
	ts_end[0] = 0;

	/************ network locations & customers behaviors *************/
	// DEPOT
	instance->addVertex(0, VRP_vertex::DEPOT, 1);
	instance->addVertex(1, VRP_vertex::DEPOT, 2);
	// instance->addVertex(0, VRP_vertex::UNLOAD_DEPOT, 1);// add a depot where vehicle can unload during their route, if allowed
	// instance->addVertex(1, VRP_vertex::UNLOAD_DEPOT, 2);//
	instance->addVertex(0, VRP_vertex::WAITING, 1);		// also add a waiting vertex for the same region, it could be useful
	instance->addVertex(1, VRP_vertex::WAITING, 2);		// 
	instanceID_in_operational_context[1] = true;
	instanceID_in_operational_context[2] = true;
	
	for (int i=0; i<4; i++) std::getline(cust_behaviors, line); // skip 4 lines

	for (int region = 2; region <= nCustomerRegions+1; region++) {		// ==> 2 depots (otherwise: (int region = 1; region <= nCustomerRegions; region++) )
		int id_instance_file, demand, e, l; int offline;
		cust_behaviors >> id_instance_file; 
		cust_behaviors >> word; 		// skip TYPE
		cust_behaviors >> demand;
		cust_behaviors >> e; e = (e - horizon_start_hour) * NB_TU_PER_HOUR + 1;
		cust_behaviors >> l; l = e + NB_TU_PER_HOUR*l -1;
		cust_behaviors >> word;		// skip word [09:00-17:00]
		cust_behaviors >> offline;
		VRP_vertex& vertex = instance->addVertex(region, VRP_vertex::REGULAR_ONLINE, id_instance_file);
		instanceID_in_operational_context[id_instance_file] = true;
		instance->addVertex(region, VRP_vertex::WAITING, id_instance_file);	// also add a waiting vertex for the same region, it could be useful
		// std::cout << "Customer vertex: " << region << "(" << id_instance_file << ")  " << demand << "  " << e << "  " << l << "   OFFLINE:" << offline;

		for (int ts = 0; ts <= nTimeSlots; ts++) {		
			VRP_request& req = vertex.getRequest_mutable(ts);
			req.demand 		= demand;
			req.e 			= e;
			req.l 			= l;
			req.add_service_time = 0;
			req.revealTS 		 = ts;
			req.revealTime_min = ts_start[ts];
			req.revealTime_max = ts_end[ts];
			if (ts > 0) {
				cust_behaviors >> req.p; req.p *= 100;		// retrieve request probability (stored in %)
				req.appeared = false;	
				// std::cout << "     " << req.p;
			} else 
				req.appeared = offline;
		}
		// std::cout << endl;


		std::getline(cust_behaviors, line); // skip end of line
	}
	cust_behaviors.close();


	// cout << instance->toStringVehicleTypes() << endl;

	/************ travel times *************/
	int nPairs;
	travel_times >> word;		// skip word PAIRS:
	travel_times >> nPairs;
	std::getline(travel_times, line); // skip end of line
	for (int i=0; i<5; i++) std::getline(travel_times, line); // skip 5 lines
	for (int pair=1; pair <= nPairs; pair++) {
		int id1, id2, travel_time_van_seconds, travel_time_bike_seconds;
		travel_times >> id1;	
		travel_times >> id2;	
		// std::cout << "(" << id1 << "<" << instanceID_in_operational_context[id1] << ">" << "," << id2 << "<" << instanceID_in_operational_context[id2] << ">" << ")";
		if (instanceID_in_operational_context[id1] && instanceID_in_operational_context[id2]) {
			travel_times >> travel_time_van_seconds;
			travel_times >> travel_time_bike_seconds;
			VRP_vertex::VertexType vertex_type_1 = VRP_vertex::REGULAR_ONLINE, vertex_type_2 = VRP_vertex::REGULAR_ONLINE;
			if (id1 <= 2) vertex_type_1 = VRP_vertex::DEPOT;
			if (id2 <= 2) vertex_type_2 = VRP_vertex::DEPOT;
			const VRP_vertex& v1 = * instance->getVertexFromInstanceFileID(vertex_type_1, id1), v2 = * instance->getVertexFromInstanceFileID(vertex_type_2, id2);
			veh_type_van.setTravelTime(v1.getRegion(), v2.getRegion(), (double) travel_time_van_seconds / 60.0);
			veh_type_bike.setTravelTime(v1.getRegion(), v2.getRegion(), (double) travel_time_bike_seconds / 60.0);
			// std::cout << "Travel time between vertex " << v1.toString() << " and " << v2.toString() << ": " << (double) travel_time_van_seconds / 60.0 << " minutes." << endl;
		}
		std::getline(travel_times, line); // skip end of line
	}
	travel_times.close();


	/************ travel distances *************/
	travel_distances >> word;		// skip word PAIRS:
	travel_distances >> nPairs;
	std::getline(travel_distances, line); // skip end of line
	for (int i=0; i<3; i++) std::getline(travel_distances, line); // skip 3 lines
	for (int pair=1; pair <= nPairs; pair++) {
		int id1, id2, travel_dist_van_meters, travel_dist_bike_meters;
		travel_distances >> id1;	
		travel_distances >> id2;	
		// std::cout << "(" << id1 << "<" << instanceID_in_operational_context[id1] << ">" << "," << id2 << "<" << instanceID_in_operational_context[id2] << ">" << ")";
		if (instanceID_in_operational_context[id1] && instanceID_in_operational_context[id2]) {
			travel_distances >> travel_dist_van_meters;
			// travel_distances >> travel_dist_bike_meters;
			travel_dist_bike_meters = travel_dist_van_meters;
			VRP_vertex::VertexType vertex_type_1 = VRP_vertex::REGULAR_ONLINE, vertex_type_2 = VRP_vertex::REGULAR_ONLINE;
			if (id1 <= 2) vertex_type_1 = VRP_vertex::DEPOT;
			if (id2 <= 2) vertex_type_2 = VRP_vertex::DEPOT;
			const VRP_vertex& v1 = * instance->getVertexFromInstanceFileID(vertex_type_1, id1), v2 = * instance->getVertexFromInstanceFileID(vertex_type_2, id2);
			veh_type_van.setTravelDistance(v1.getRegion(), v2.getRegion(), (double) travel_dist_van_meters / 1000);
			veh_type_bike.setTravelDistance(v1.getRegion(), v2.getRegion(), (double) travel_dist_bike_meters / 1000);
			// std::cout << "Travel time between vertex " << v1.toString() << " and " << v2.toString() << ": " << (double) travel_time_van_seconds / 60.0 << " minutes." << endl;
		}
		std::getline(travel_distances, line); // skip end of line
	}
	travel_distances.close();



	/************ service times *************/
	int nNodes;
	service_times >> word;		// skip word NODES:
	service_times >> nNodes;
	std::getline(service_times, line); // skip end of line
	for (int i=0; i<5; i++) std::getline(service_times, line); // skip 5 lines
	for (int node=1; node <= nNodes; node++) {
		int id, service_time_van_seconds, service_time_bike_seconds;
		service_times >> id;	
		// std::cout << id << "<" << instanceID_in_operational_context[id] << ">";
		if (instanceID_in_operational_context[id]) {
			service_times >> word;		// skip TYPE
			service_times >> service_time_van_seconds;
			service_times >> service_time_bike_seconds;
			VRP_vertex::VertexType vertex_type = VRP_vertex::REGULAR_ONLINE;
			if (id <= 2) vertex_type = VRP_vertex::DEPOT;
			const VRP_vertex& v = * instance->getVertexFromInstanceFileID(vertex_type, id);
			veh_type_van.setServiceTime(v.getRegion(), (double) service_time_van_seconds / 60.0);
			veh_type_bike.setServiceTime(v.getRegion(), (double) service_time_bike_seconds / 60.0);
			// std::cout << "Service time at " << v.toString() << ": " << (double) service_time_van_seconds / 60.0 << "(van):" << (double) service_time_bike_seconds / 60.0 << "(bike) minutes." << endl;
		}
		std::getline(service_times, line); // skip end of line
	}
	service_times.close();

	// cout << instance->toStringVehicleTypes() << endl;

	/************ scenario instance *************/
	int n_requests;
	scenario_file >> word; scenario_file >> word;		// skip words ONLINE REQUESTS:
	scenario_file >> n_requests;
	std::getline(scenario_file, line); // ski end of line
	for (int i=0; i<6; i++) std::getline(scenario_file, line); // skip 6 lines
	for (int i = 0; i < n_requests; i++) {
		int id_instance_file, ts;
		std::string reveal_time_str;
		scenario_file >> id_instance_file;	
		scenario_file >> word; 	// skip demand
		scenario_file >> word; scenario_file >> word; scenario_file >> word;  // skip TW
		scenario_file >> reveal_time_str; 
		scenario_file >> ts;

		int reveal_time = (stoi(split<string>(reveal_time_str, ':')[0]) - horizon_start_hour)*NB_TU_PER_HOUR + stoi(split<string>(reveal_time_str, ':')[1]) + 1;		
		int region = instance->getVertexFromInstanceFileID(VRP_vertex::REGULAR_ONLINE, id_instance_file)->getRegion();
		instance->addAppearingOnlineRequest(region, ts, reveal_time);
		// cout << "Online req: " << id_instance_file << " internal_id: " << instance->getVertexFromInstanceFileID(VRP_vertex::REGULAR_ONLINE, id_instance_file)->getRegion() << "   revealTime=" << reveal_time << "   (ts " << ts << ")" << endl;

		std::getline(scenario_file, line); // skip end of line
	}
	scenario_file.close();	

	// cout << instance->toStringVehicleTypes() << endl;

	return *instance;
}












/* Instance files used for the SS-VRPTW-CR, journal paper, part 1: models */
VRP_instance& VRP_instance_file::readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs(const char *filename_distances, const char *filename_instance, int vehicle_capacity, double scale) {
	int nRegions_total, nRegions, nWaitingVertices, horizon, orig_horizon, nTimeSlots;
	int day_duration_hours;//, waiting_time_step;
	std::string line, word;

	
	

	/* read distance matrix file */
	std::ifstream distances_file;
	distances_file.open(filename_distances);
	if (distances_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename_distances << endl;
		exit (8);
	}

	distances_file >> word; distances_file >> word; distances_file >> word;	// skip words "Number of vertices:"
	distances_file >> nRegions_total;
	std::getline(distances_file, line); // skip end of line
	std::getline(distances_file, line); // skip line "Average travel times ..."

	vector< vector<double> > dist;
	for (int v1=0; v1 < nRegions_total; v1++) 
		dist.push_back(vector<double>(nRegions_total, 0.0));

	for (int i=0; i < nRegions_total; i++) {
		distances_file >> word; distances_file >> word;	// skip words "i :"
		for (int j = 0; j < nRegions_total; j++) {
			distances_file >> dist[i][j];
		}
		std::getline(distances_file, line); // skip end of line
	}

	distances_file.close();





	/* read instance file */
	std::ifstream instance_file;
	instance_file.open(filename_instance);
	if (instance_file.fail()) {
		std::cerr << endl << " Error: Unable to open " << filename_instance << endl;
		exit (8);
	}

	std::getline(instance_file, line);	// skip line "INSTANCE ATTRIBUTES""
	instance_file >> word; 				// skip word "#CustomerRegions:"
	instance_file >> nRegions;
	instance_file >> word; 				// skip word "#WaitingVertices:"
	instance_file >> nWaitingVertices;
	// instance_file >> word; 				// skip word "VehicleCapacity:"
	// instance_file >> Q;
	std::getline(instance_file, line); // skip end of line

	instance_file >> word; 				// skip word "DayDuration:"
	instance_file >> day_duration_hours;
	instance_file >> word; 				// skip word "HorizonSize:"
	instance_file >> orig_horizon;
	instance_file >> word; 				// skip word "#TimeSlots:"
	instance_file >> nTimeSlots;
	std::getline(instance_file, line); // skip end of line

	horizon = ceil(orig_horizon / scale);
	

	int day_duration_sec	= day_duration_hours * 3600;
	int nbsec_per_timeunit 	= day_duration_sec / horizon;	

							// 	(int nRegions, int horizon, int nTimeSlots=0, int orig_horizon=0, double scale=1)
	VRP_instance* instance = new VRP_instance(1+nWaitingVertices+nRegions, horizon, nTimeSlots, orig_horizon, scale);
	VRP_VehicleType& veh_type = instance->addVehicleType(string(""), vehicle_capacity);

	std::getline(instance_file, line);	
	std::getline(instance_file, line);	// skip line "INSTANCE VERTICES"
	instance_file >> word; 				// skip word "Depot:"
	int depot_instance_id;
	instance_file >> depot_instance_id;
	instance->addVertex(0, VRP_vertex::DEPOT, depot_instance_id);
	std::getline(instance_file, line); // skip end of line

	instance_file >> word; instance_file >> word; 	// skip words "Customer vertices:"
	int * instanceID_to_id = new int[nRegions_total];
	for (int i=nWaitingVertices+1; i <= nWaitingVertices+nRegions; i++) {
		int region_id;
		instance_file >> region_id;
		instance->addVertex(i, VRP_vertex::CUSTOMER, region_id);
		instanceID_to_id[region_id] = i;
	}
	std::getline(instance_file, line); // skip end of line

	instance_file >> word; instance_file >> word; 	// skip words "Waiting vertices:"
	for (int i=1; i <= nWaitingVertices; i++) {
		int region_id;
		instance_file >> region_id;
		instance->addVertex(i, VRP_vertex::WAITING, region_id);
	}
	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line); // skip empty line
	std::getline(instance_file, line);	// skip line "POTENTIAL REQUESTS"
	int nRequests;
	instance_file >> word; 				// skip word "#PotentialRequests:"
	instance_file >> nRequests;
	instance->nPotentialRequests = nRequests;

	std::getline(instance_file, line); // skip end of line
	std::getline(instance_file, line);	// skip line "(everything has been ..."
	std::getline(instance_file, line);	// skip line 
	bool tw_doubled = false, tw_x3 = false, zero_demands = false;
	for (int i=0; i < nRequests; i++) {
		int region, ts, revealTime, prob, duration, demand, e, l;
		instance_file >> region;
		instance_file >> ts;			ASSERT(ts <= nTimeSlots, "ts=" << ts);
		instance_file >> revealTime;
		instance_file >> prob;
		instance_file >> duration;
		instance_file >> demand;
		instance_file >> e;
		instance_file >> l;

		VRP_request& req = instance->getVertexFromRegion_mutable(VRP_vertex::CUSTOMER, instanceID_to_id[region])->getRequest_mutable(ts);
		req.id = i+1;
		req.id_instance_file = req.id;
		req.revealTS = ts;
		req.revealTime = revealTime;
		req.revealTime_min = req.revealTime;
		req.revealTime_max = req.revealTime;
		req.p = prob;
		req.demand = demand;
										req.demand = 0; zero_demands = true;
		req.add_service_time = duration;
		req.e = e;
		req.l = l;
										// req.l += max(0, l - e); tw_doubled = true; // TW doubled !
										// req.l += 2 * max(0, l - e); tw_x3 = true; // TW tripled !

		req.revealTime		= ceil(req.revealTime / scale);
		req.revealTime_min 	= ceil(req.revealTime_min / scale);
		req.revealTime_max 	= max(req.revealTime_min, ceil(req.revealTime_max / scale));
		req.add_service_time 	= ceil(req.add_service_time / scale);
		req.e 				= ceil(req.e / scale);
		req.l 				= max(req.e, floor(req.l / scale));

		std::getline(instance_file, line); // skip end of line
		// cout << req.toString(true) << endl << flush;
	}
	if (zero_demands) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs :: demands hardcoded to zero !!" << outputMisc::resetColor() << endl << endl;
	if (tw_doubled) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs :: all TW doubled !!" << outputMisc::resetColor() << endl << endl;
	if (tw_x3) cout << endl << outputMisc::redExpr(true) << "!!!! readInstanceFile_SSVRPTW_CR_journal_part_1___REASSIGN_IDs :: all TW tripled !!" << outputMisc::resetColor() << endl << endl;

	instance_file.close();


	// Set all the travel times between each couple of vertices added to the instance

	instance->setIntegerTravelTimes();

	for (int v1=0; v1 < 1+nWaitingVertices+nRegions; v1++) {
		for (int v2=0; v2 < 1+nWaitingVertices+nRegions; v2++) {
			const VRP_vertex *v_1 = nullptr, *v_2 = nullptr;

			if (v1 == 0) v_1 = instance->getVertexFromRegion(VRP_vertex::DEPOT, v1);
			if (0 < v1 && v1 <= nWaitingVertices) v_1 = instance->getVertexFromRegion(VRP_vertex::WAITING, v1);
			if (nWaitingVertices < v1 && v1 <= nWaitingVertices+nRegions) v_1 = instance->getVertexFromRegion(VRP_vertex::CUSTOMER, v1);


			if (v2 == 0) v_2 = instance->getVertexFromRegion(VRP_vertex::DEPOT, v2);
			if (0 < v2 && v2 <= nWaitingVertices) v_2 = instance->getVertexFromRegion(VRP_vertex::WAITING, v2);
			if (nWaitingVertices < v2 && v2 <= nWaitingVertices+nRegions) v_2 = instance->getVertexFromRegion(VRP_vertex::CUSTOMER, v2);

			ASSERT(v_1->getIdInstance() >= 0 && v_1->getIdInstance() <= nRegions_total, "argh");
			veh_type.setTravelDistance(v1, v2, dist[v_1->getIdInstance()][v_2->getIdInstance()]/nbsec_per_timeunit);

		}
	}


	return *instance;
}







