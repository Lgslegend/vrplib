#include "../VRP_instance.h"
#include "../VRP_solution.h"
#include "../VRP_scenario.h"
#include "../NM.h"
#include "../LS_Program.h"
#include "../tools.h"
#include <queue>
#include <cmath>
#include <limits>

using namespace std;






VRP_instance* instance_VRPTW;

vector<VRP_instance*> instances_DS_VRPTW;
vector<const char *> instance_files_DS_VRPTW;

vector<VRP_instance*> instances_SS_VRPTW_CR;
vector<VRP_instance*> instances_SS_VRPTW_CR_scaled_5;
vector<VRP_instance*> instances_SS_VRPTW_CR_scaled_10;
vector<const char *> instance_files_SS_VRPTW_CR;


const char * instance_file_VRPTW = "TU_instance_VRPTW.txt";


void initialize_instances() {
	instance_VRPTW = & VRP_instance_file::readInstanceFile_CordeauLaporte_VRPTW_old(instance_file_VRPTW);

	instance_files_SS_VRPTW_CR.push_back("TU_instance_SSVRPTWCR_1.txt");
	instance_files_SS_VRPTW_CR.push_back("TU_instance_SSVRPTWCR_2.txt");
	instance_files_SS_VRPTW_CR.push_back("TU_instance_SSVRPTWCR_3.txt");
	for(auto filename : instance_files_SS_VRPTW_CR) {
		instances_SS_VRPTW_CR.push_back(& VRP_instance_file::readInstanceFile_SSVRPTW_CR(filename, 1.0));
		instances_SS_VRPTW_CR_scaled_5.push_back(& VRP_instance_file::readInstanceFile_SSVRPTW_CR(filename, 5.0));
		instances_SS_VRPTW_CR_scaled_10.push_back(& VRP_instance_file::readInstanceFile_SSVRPTW_CR(filename, 10.0));
	}


	instance_files_DS_VRPTW.push_back("TU_instance_DSVRPTW_1_Bent.txt");
	instance_files_DS_VRPTW.push_back("TU_instance_DSVRPTW_2_Bent.txt");
	instance_files_DS_VRPTW.push_back("TU_instance_DSVRPTW_3_Bent.txt");
	for(auto filename : instance_files_DS_VRPTW) {
		instances_DS_VRPTW.push_back(& VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(filename));
	}
}





bool VRP_request::operator<(const VRP_request& other) const {
	if (operator==(other)) return false;

	if (revealTime < other.revealTime)							// reveal time first
		return true;
	if (revealTime == other.revealTime) {
		if (l < other.l)										// end of time window second)
			return true;
		if (l == other.l) {
			ASSERT(vertex->region != other.vertex->region, "no tie breaking !");
			return vertex->region < other.vertex->region;		// region number to break ties
		}
	}

	return false;
}

VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> *scenarioPool;
VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR> *scenarioPool_big;
vector<RecourseStrategy> strategies;

int nb_iter = 0;
VRP_instance* curr_instance;
CallbackReturnType checkSimulation(Solution_SS_VRPTW_CR& bestSolution, double &best_eval) {
	UNUSED(best_eval);
	if (bestSolution.getNumberViolations() == 0) {
		// RecourseStrategy strategy = R_CAPA;
		// RecourseStrategy strategy = R_CAPA_PLUS;
		// Solution_SS_VRPTW_CR &s = bestSolution;
		Solution_SS_VRPTW_CR s = bestSolution;
		// cout << endl << bestSolution.toString(false);
		for (auto strategy : strategies) {
			s.setRecourseStrategy(strategy);
			double experimental_expected_cost = scenarioPool->computeExperimentalExpectedCost(s, strategy, false);	// debug = false, for efficiency
			// cout << "Exp. cost: " << s.getCost() << "   Experimental: " << experimental_expected_cost << endl;
			if (abs(s.getCost() - experimental_expected_cost) > 0.1) {
				double exp_cost = scenarioPool_big->computeExperimentalExpectedCost(s, strategy);
				if (abs(s.getCost() - exp_cost) > 0.01) {
					cout << endl << "Recourse strategy: " << strategy << endl;
					cout << curr_instance->toString() << endl;
					cout << s.toString(false) << s.toString(true);
					ASSERT(false, s.getCost() << " != experimental: " << exp_cost);
				}
			}
		}
	} 
	nb_iter++;
	return NONE;
}
void test_expected_cost(VRP_instance* I, int timeout) {
	strategies.clear();
	strategies.push_back(R_INFTY);
	strategies.push_back(R_CAPA);
	strategies.push_back(R_CAPA_PLUS);

	// RecourseStrategy strategy = R_INFTY;
	RecourseStrategy strategy = R_CAPA;
	// RecourseStrategy strategy = R_CAPA_PLUS;
	curr_instance = I;

	// SOLUTION 
	int initialWaitingTime = floor(I->getHorizon() / 10);
	Solution_SS_VRPTW_CR s(*I, strategy, initialWaitingTime);
	s.addRoutes(I->_getNumberVehicles());
	
	// NEIGHBORHOOD MANAGER
	int maxIncrement = floor(I->getHorizon() / 5), minIncrement = max(0.0, floor(I->getHorizon() / 20));
	NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, SS_VRPTW_CR, I->getHorizon(), minIncrement, maxIncrement);
	// NeighborhoodManager<Solution_SS_VRPTW_CR> nm(s, "VRP_basic");

	// SCENARIO POOLS
	scenarioPool = new VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>(*I, 	100000, SS_VRPTW_CR);
	scenarioPool_big = new VRP_ScenarioPool_Volatile<Scenario_SS_DS_VRPTW_CR, Solution_SS_VRPTW_CR>(*I,1000000, SS_VRPTW_CR);

	s.generateInitialSolution(false);

	LS_Program_SimulatedAnnealing<Solution_SS_VRPTW_CR,NeighborhoodManager<Solution_SS_VRPTW_CR>> lsprog;
	lsprog.set_NewBestSolution_Callback(&checkSimulation);
	lsprog.setTimeOut(timeout);
	lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	// cout << endl << s.toString() << endl;

}
void test_expected_cost_main() {
	int timeout = 100;	// in seconds
	int i = 0;
	for (auto instance : instances_SS_VRPTW_CR_scaled_10) {
		cout << "Testing expected cost computations using LS (" << timeout << "s) on instance file " << instance_files_SS_VRPTW_CR[i] << " (scale: 10.0) ... " << flush;
		// cout << endl << instance->toString() << endl;
		test_expected_cost(instance, timeout);	
		i++;
		cout << outputMisc::greenExpr(true) << "OK. (#iter: " << nb_iter << ")" << outputMisc::resetColor() << endl; nb_iter = 0;
	}
	i = 0;
	for (auto instance : instances_SS_VRPTW_CR_scaled_5) {
		cout << "Testing expected cost computations using LS (" << timeout << "s) on instance file " << instance_files_SS_VRPTW_CR[i] << " (scale: 5.0) ... " << flush;
		test_expected_cost(instance, timeout);	
		i++;
		cout << outputMisc::greenExpr(true) << "OK. (#iter: " << nb_iter << ")" << outputMisc::resetColor() << endl; nb_iter = 0;
	}
	i = 0;
	for (auto instance : instances_SS_VRPTW_CR) {
		cout << "Testing expected cost computations using LS (" << timeout << "s) on instance file " << instance_files_SS_VRPTW_CR[i] << " (scale: 1.0) ... " << flush;
		test_expected_cost(instance, timeout);	
		i++;
		cout << outputMisc::greenExpr(true) << "OK. (#iter: " << nb_iter << ")" << outputMisc::resetColor() << endl; nb_iter = 0;
	}
}


void test_LS_VRPTW() {
	int timeout = 2;
	cout << "Testing LS (" << timeout << "s) on VRPTW instance (" << instance_file_VRPTW << ") ... " << flush;

	Solution_DS_VRPTW s(*instance_VRPTW);
	s.addRoutes(instance_VRPTW->_getNumberVehicles());
	// s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);

	// NEIGHBORHOOD MANAGER
	NeighborhoodManager<Solution_DS_VRPTW> nm(s, VRPTW);
	// cout << instance_VRPTW->toString() << endl;

	LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;

	s.generateInitialSolution();
	lsprog.setTimeOut(timeout);		// timeout for the local search : 0.005 second
	int n_iterations = lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
	// cout << s.toString() << endl << s.getCost() << "    " << s.getNumberViolations() << endl << s.toString(true) << endl;

	ASSERT(s.getNumberViolations() == 0, endl << s.toString() << endl << "The LS should have found a feasible solution yet ! (did you try compiling with -03 ?)" << endl);// << s.toString(true));

	s.setCurrentTime(200);
	lsprog.setTimeOut(timeout);	
	lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, false);
	cout << outputMisc::greenExpr(true) << "OK. (#iter: " << n_iterations << ")" << outputMisc::resetColor() << endl;
}

template <class SolutionType>
void test_NM_restore(VRP_instance* instance, VRP_type vrp_type) {
	SolutionType *incumbent, *s;

	if (vrp_type == SS_VRPTW_CR)
		cout << "Testing NM shakeSolution + restore on SS_VRPTW_CR ... " << flush;
	else cout << "Testing NM shakeSolution + restore on VRPTW ... " << flush;

	incumbent 	= new SolutionType(*instance);
	incumbent->addRoutes(instance->_getNumberVehicles());
	s 			= new SolutionType(*instance);
	s->addRoutes(instance->_getNumberVehicles());

	NeighborhoodManager<SolutionType> nm(*s, vrp_type);
	s->generateInitialSolution();
	*incumbent = *s;

	for (int i=0; i<100000; i++) {
		bool b = nm.shakeSolution();
		if (b) { 
			if(s->getCost() < incumbent->getCost()) {
				*incumbent = *s;
			} else if(s->getCost() > incumbent->getCost() * 1.1) {
				nm.restorePreviousSolution(*incumbent, false);
				ASSERT((*s) == (*incumbent), "");
			}
		}
	}
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;
}
void test_NM_restore_main() {
	test_NM_restore<Solution_VRPTW>(instance_VRPTW, VRPTW);
	// test_NM_restore<Solution_SS_VRPTW_CR>(instances_SS_VRPTW_CR[0], SS_VRPTW_CR);
}

void test_init_sol() {
	cout << "Testing VRPTW solution initialization + generating initial solution ... " << flush;
	Solution_VRPTW s(*instance_VRPTW);
	s.addRoutes(instance_VRPTW->_getNumberVehicles());
	s.generateInitialSolution();
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;

	cout << "Testing SS-VRPTW-CR solution initialization + generating initial solution ... " << flush;
	Solution_SS_VRPTW_CR s_(*instances_SS_VRPTW_CR[0], R_INFTY);
	s_.addRoutes(instances_SS_VRPTW_CR[0]->_getNumberVehicles());
	s_.generateInitialSolution();
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;
}

void test_scale_unscale() {
	double precision = 5.0;

	cout << "Testing VRPTW solution (UN/)SCALING ... " << flush;
	Solution_VRPTW vrptw(*instance_VRPTW);
	vrptw.addRoutes(instance_VRPTW->_getNumberVehicles());
	vrptw.generateInitialSolution();
	Solution_VRPTW vrptw_initial = vrptw;
	VRP_instance instance_vrp_scaled = VRP_instance_file::readInstanceFile_CordeauLaporte_VRPTW_old(instance_file_VRPTW);
	Solution_VRPTW vrptw_scaled(instance_vrp_scaled);
	vrptw_scaled.addRoutes(instance_vrp_scaled._getNumberVehicles());
	vrptw_scaled.generateInitialSolution();
	vrptw_scaled.copyFromSol_scaleWaitingTimes(vrptw);
	vrptw.copyFromSol_scaleWaitingTimes(vrptw_scaled);
	ASSERT(vrptw == vrptw_initial, "");
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;

	cout << "Testing SS-VRPTW-CR solution (UN/)SCALING ... " << flush;
	double initialWaitingTime = 10;
	Solution_SS_VRPTW_CR ssvrp(*instances_SS_VRPTW_CR[0], R_INFTY, initialWaitingTime);
	ssvrp.addRoutes(instances_SS_VRPTW_CR[0]->_getNumberVehicles());
	ssvrp.generateInitialSolution();
	Solution_SS_VRPTW_CR ssvrp_initial = ssvrp;
	VRP_instance instance_ssvrp_scaled = VRP_instance_file::readInstanceFile_SSVRPTW_CR(instance_files_SS_VRPTW_CR[0], precision);
	Solution_SS_VRPTW_CR ssvrp_scaled(instance_ssvrp_scaled, R_INFTY, initialWaitingTime);
	ssvrp_scaled.addRoutes(instance_ssvrp_scaled._getNumberVehicles());
	ssvrp_scaled.generateInitialSolution(true);
	ssvrp_scaled.copyFromSol_scaleWaitingTimes(ssvrp);
	ssvrp.copyFromSol_scaleWaitingTimes(ssvrp_scaled);
	ASSERT(ssvrp == ssvrp_initial, endl << "ssvrp:" << endl << ssvrp.toString() << "ssvrp_initial:" << endl << ssvrp_initial.toString());
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;
}

void test_DS_VRPTW() {
	int i = 0;
	for (auto instance : instances_DS_VRPTW) {
		(void) instance;
		cout << "Testing DS-VRPTW on instance file " << instance_files_DS_VRPTW[i] << " ... " << flush;

		VRP_instance I = VRP_instance_file::readInstanceFile_DS_VRPTW__Bent_PVH(instance_files_DS_VRPTW[i]);

		Solution_DS_VRPTW s(I);
		s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
		s.addRoutes(I._getNumberVehicles());
		NeighborhoodManager<Solution_DS_VRPTW> nm(s, DS_VRPTW);

		s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
		LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;
		lsprog.setTimeOut(0.5);		
		lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)

		Scenario_SS_DS_VRPTW_CR scenario(I, DS_VRPTW, 0);
		for (const VRP_instance::RequestAttributes& req_attr : scenario.getRequestSequence())
			s.insertOnlineRequest(*req_attr.request);
		lsprog.setTimeOut(2);		
		lsprog.run(s, nm, numeric_limits<int>::max(), 10.0, 0.95, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		
		s.removeAllSampled_violated_OnlineRequests();
		
		for (const VRP_instance::RequestAttributes& req : I.getAppearingOnlineRequests()) {
			ASSERT(req.vertex == & req.request->getVertex() && req.request != nullptr, "argh");
			s.setCurrentTime(req.reveal_time);
			I.set_OnlineRequest_asAppeared(req.vertex->getRegion(), req.request->revealTS);		// inform the instance object that the request is from now marked as appeared

			s.tryInsertOnlineRequest(*req.vertex, req.request->revealTS);
		}

		for (VehicleAction action : s.getCurrentActions()) {
			(void) action;
		}
		for (VehicleAction action : s.getNextPlannedActions()) {
			(void) action;
		}

		cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;
		i++;
	}
}



void test_scenario_gen_GSA() {
	int i = 0;
	for (auto instance : instances_DS_VRPTW) {

		cout << "Testing scenario generation and GSA evaluation on DS-VRPTW on instance file " << instance_files_DS_VRPTW[i] << " ... " << flush;

		VRP_instance& I = *instance;

		Solution_DS_VRPTW s(I);
		s.setWaitingStrategy(DRIVE_FIRST_DELAY_LAST);
		s.addRoutes(I._getNumberVehicles());
		s.generateInitialSolution();		// here all the requests with prebin probability > 0 will be inserted
		
		NeighborhoodManager<Solution_DS_VRPTW> nm(s, DS_VRPTW);
		LS_Program_SimulatedAnnealing<Solution_DS_VRPTW,NeighborhoodManager<Solution_DS_VRPTW>> lsprog;
		lsprog.setTimeOut(2);
		lsprog.run(s, nm, numeric_limits<int>::max(), 5.0, 0.995, false);	// args: (Solution &solution, NM &nm, int max_iter, double init_temp, double cooling_factor, bool verbose = false)
		ASSERT(s.getNumberViolations() == 0, "");

		VRP_ScenarioPool<Scenario_SS_DS_VRPTW_CR, Solution_DS_VRPTW> scenarioPool(I, 200, DS_VRPTW, 0);
		s.setCurrentTime(0);
		scenarioPool.evalSolution_GSA(s);

		cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;
		i++;
	}
}



int main(int argc, char **argv) {
	(void) argc;
	(void) argv;

	srand(time(NULL));

	initialize_instances();

	test_init_sol();
	test_scale_unscale();
	test_NM_restore_main();
	test_LS_VRPTW();

	test_DS_VRPTW();
	test_scenario_gen_GSA();

	test_expected_cost_main();
	

}






