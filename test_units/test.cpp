#include "../VRP_instance.h"
#include "../VRP_solution.h"
#include "../VRP_scenario.h"
#include "../NM.h"
#include "../LS_Program.h"
#include "../tools.h"
#include <queue>
#include <cmath>
#include <limits>

using namespace std;

bool VRP_request::operator<(const VRP_request& other) const {
	return id_instance_file < other.id_instance_file;
}

int main(int argc, char **argv) {

	const VRP_instance* instance_VRPTW = & VRP_instance_file::readInstanceFile_CordeauLaporte_VRPTW_old("TU_instance_VRPTW.txt");
	const VRP_instance* instances_SS_VRPTW_CR = & VRP_instance_file::readInstanceFile_SSVRPTW_CR("TU_instance_SSVRPTWCR_1.txt", 1.0);


	cout << "Testing VRPTW solution initialization + generating initial solution ... " << flush;
	Solution_VRPTW s(*instance_VRPTW);
	s.addRoutes(instance_VRPTW->_getNumberVehicles());
	s.generateInitialSolution();
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;

	cout << "Testing SS-VRPTW-CR solution initialization + generating initial solution ... " << flush;
	Solution_SS_VRPTW_CR s_(*instances_SS_VRPTW_CR, R_INFTY);
	s_.addRoutes(instances_SS_VRPTW_CR->_getNumberVehicles());
	s_.generateInitialSolution();
	cout << outputMisc::greenExpr(true) << "OK." << outputMisc::resetColor() << endl;
}



