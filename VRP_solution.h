/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/





/* VRP_solution +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	This class is used to represent a VRP solution.

	Its construction takes an object of type VRP_instance, describing the current problem.

	Its representation of a solution, that is a set of vehicle routes, 
	relies on an object of type VRP_routes (created locally, based on the instance object).
	The class provides various methods:
		- initialization methods
		- solution evaluation methods
		- methods for extern neighborhood operators: 
			* selection and differentiation methods
			* altering methods (used to modify the solution)
			* informative methods (answering question such as: # of routes, # of vertices, etc)
		  The later methods communicate with the extern neighborhood operator
		  by using the SolutionElement datastructure.

	Dynamic context is supported. We use the concept that a vertex can be either "free" or "fixed";
	a fixed vertex cannot be moved in the solution (more specifically, the solution cannot be modified
	up to the fixed vertex). It represents the fact that part of the solution, at some given time t, 
	could be already processed by the vehicles; then it makes no sense to modify something that already 
	happened in reality. Finally, we said that a route itself if fixed iff the ending depot is fixed,
	meaning that we can't insert/modify anything before it.

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#ifndef VRP_SOLUTION_H
#define	VRP_SOLUTION_H

#include "VRP_routes.h"
#include "VRP_instance.h"

#include <set>
#include <vector>
#include <unordered_map>



enum VRP_type {
	VRPTYPE_NO_SET,
	TSP,
	MTSP,
	// VRP_BASIC,
	VRP,
	// VRPTW_BASIC,
	VRPTW,
	DS_VRPTW,
	DS_VRPTW_GSA,
	SS_VRPTW_CR
}; 

struct VehicleAction {
	int vehicle = 0;					// the vehicle concerned
	int pos = 0;						// the position in the corresponding route
	const VRP_vertex* pvertex = NULL; 	// pointer to the vector at such position
	ActionType action = NO_ACTION;		// the action type itself
	double start_time = -1;				// time at which has started / will start the action
	double end_time = -1;				// time at which will stop the action
	bool operator==(const VehicleAction& other) const { return vehicle == other.vehicle && pos == other.pos && pvertex == other.pvertex && action == other.action && start_time == other.start_time && end_time == other.end_time; }
	bool operator != (const VehicleAction& other) const { return ! (*this == other); }
	string& toString() const;
};







class VRP_solution {
protected:
	const VRP_instance& I;					// the problem instance 
	VRP_routes* routes;						// the set of vehicle routes
	bool initial_solution_generated = false;// true if generateInitialSolution() has already been called, false otherwise

	VRP_type type = VRPTYPE_NO_SET;
	double scale = 0;						// scale of the horizon in the instance corresponding to the solution

	int selectRandomRoute(int min_size, bool free, bool modifiable, bool containsFreeWaitingV, bool containsUNLEFTwaiting, int not_route =-42) const;	// returns a random route, s.t. it contains at least min_size vertices that are free (if free==true) and moveable (if modifiable==true), and contains free waiting vertices (if containsFreeWaitingV=true); if not_route is set, then avoid that route; when set to false, the conditions are not checked (e.g. free==false ==> may contain or not free vertices)
	void clearAllRoutes();

	/* the solution always knows the number of violated constraints, and the total 'weight' of the violations; this is updated each time the solution is modified, by calling solution_changed() (which itself calls update_violations()) */
	vector<int> n_violations_route;
	vector<double> w_violations_route;
	int n_violations;
	double w_violations;

	/* notify the class whenever something changed in the solution (will call the adequate methods in order to keep things up-to-date) */
	virtual void solution_changed(int route = -42);
	void update_violations(int route = -42);

public:

	static const int 	ALL_CONSTRAINTS		=	0;
	static const int 	CAPACITY_CONSTRAINT	=	1;
	static const int 	TW_CONSTRAINT		=	2;

	enum SolutionElementType {
		VERTEX,				// a planned vertex
		SEGMENT,			// a planned sequence of vertices
		POSITION,			// a position in a route
		WAITING_LOC,		// a waiting location of the problem instance graph (≠ planned waiting vertex !)
		ROUTE, 				// only the route info
		NO_TYPE
	};
	struct SolutionElement {
		SolutionElementType type;		// type of the element: vertex, segment or position
		int route, route_1, route_2;
		int pos, pos_1, pos_2;
		int region;
		const VRP_request* request;		

		SolutionElement() : type(NO_TYPE), route(-1), route_1(-1), route_2(-1), pos(-1), pos_1(-1), pos_2(-1), region(-1), request(NULL) {}
		void getPos(SolutionElement* e) const;
		void getVertex(SolutionElement* e) const;
		void getRegion(SolutionElement* e) const;
		void getSegment(int length, SolutionElement* e) const;
		void getRoute(SolutionElement* e) const;
		int  getLength() const;
		bool operator == (const VRP_solution::SolutionElement& other) const;
		bool operator != (const VRP_solution::SolutionElement& other) const { return !operator==(other);}
		bool operator <  (const VRP_solution::SolutionElement& other) const;
		void clear() { SolutionElement(); }
		string& toString() const;
	};

	void init();
	VRP_solution(const VRP_instance& instance);							// constructor *** allocate the VRP_routes object routes and add I.nVeh routes in it 
	VRP_solution(const VRP_solution& sol);								// copy constructor ***
	VRP_solution& operator = (const VRP_solution& sol);					// copy operator
	void copyRouteFrom(const VRP_solution& sol, SolutionElement route);
	virtual ~VRP_solution();
	bool operator == (const VRP_solution& other) const;
	bool operator != (const VRP_solution& other) const { return ! operator==(other);}

	int addRoute(const VRP_vertex& depot, const VRP_VehicleType& vehicle, int n = 1);	// add a new vehicle route in the solution (n times); return the number of routes in the solution (can be used as an identifyer the the just created route [e.g. for function activateAutomaticVehicleUnload])
	void addRoutes(int n);																// add n vehicle routes, with the same (only) depot, and the same (only) vehicle type
	// void activateAutomaticVehicleUnload(int route, const VRP_vertex& depot);			// the vehicle assigned to the route will automatically make round trips to the given depot whenever its capacity is full
	void activateAutomaticVehicleUnload(int route);										// the vehicle assigned to the route will automatically make round trips to the associate depot whenever its capacity is full

	// void 	initializeFromSol_scaleWaitingTimes(const VRP_solution& sol, double scale);	// set a solution from an existing one (i.e. clear all routes and copy), while scaling the waiting times
	void 	copyFromSol_scaleWaitingTimes(const VRP_solution& sol, bool try_preserve_feasibility = false);	// set a solution from an existing one (i.e. clear all routes and copy), while scaling the waiting times ACCORDING to the two solutions' scales

	/* evaluation functions */
	double 	getCost(int route = -42) const;					// returns the cost of the solution - specify route for the cost of a particular route
	int 	getNumberViolations(int route = -42) const;		// returns the number of violations in the solution - specify route for a particular route
	double 	getWeightViolations(int route = -42) const;		// returns the total weight of all violations in the solution - specify route for a particular route

	void 	generateInitialSolution();
	void 	generateInitialSolutionFrom(const VRP_solution& sol);
	

	/* selection functions - unless specified, selection methods return only non fixed (= free) vertices (or segments) */
	void selectRandomMoveableVertex(SolutionElement* v, int route =-42, int not_route =-42) const;									// returns a random planned vertex v (one being not fixed and moveable); one can specify the route, and also specify to not consider a given route (not_route)
	void selectRandomInsertionPosition(SolutionElement* p, int route =-42, int not_route =-42) const;								// returns a random position p in a random route; or one can specify the route, and also specify to not consider a given route (not_route)
	void selectRandomInsertionPosition(const SolutionElement& v, SolutionElement* p, int route =-42, int not_route =-42) const;		// returns a random position p (≠ v's position and v's position +1 !) in a random route (from a vehicle that can handle v's request demand); or one can specify the route, and also specify to not consider a given route (not_route)
	void selectRandomMoveableSegment(SolutionElement* s, int min_size, int max_size, int route =-42, int not_route =-42) const;	// returns a random moveable sequence s of vertices of minimum length min_size (≥ 1) and maximum random lentgh max_size; one can specify the route, and also specify to not consider a given route (not_route)
	void selectRandomWaitingLocation(SolutionElement* wl) const;							// returns a random waiting location wl (≠ planned waiting vertex !!)
	void selectRandomFreePlannedWaitingVertex(SolutionElement* w, int route =-42) const;	// returns a random non-fixed waiting vertex w planned in a random route; or one can specify the route
	void selectRandomUNLEFTWaitingVertex(SolutionElement* w, int route =-42) const;			// returns a random unleft (= s.t. the vehicle didn't left that waiting vertex yet) waiting vertex w in a random route; or one can specify the route

	/* checking conditions */
	bool exchangeable(const SolutionElement& v1, const SolutionElement& v2) const;		// returns false only if vertices v1, v2 can't be exchanged for some reason (e.g. v1's route vehicle cannot handle demand of v2's request)

	/* differentiation functions */
	void selectBestInsertionPosition(const SolutionElement& v, SolutionElement* p);		// returns the best position p to insert vertex v, according to violations and cost diffferences
	void selectBestRelocationPosition(const SolutionElement& v, SolutionElement* p);		// returns the best position p to relocate a vertex v, according to violations and cost diffferences
	void selectWorstMoveableVertex(SolutionElement* v);									// returns the worst planned vertex v in the solution, according to violations and cost differences



	/* altering functions - works only if the targeted parts of the solution to be modified are not fixed (otherwise some underlying assertion will not be fulfilled and the program will exit with an error) */
	void moveVertex(const SolutionElement& v, const SolutionElement& p);				// moves a vertex from its current position in its current route, to another position (possibly in another existing route)
	void moveSegment(const SolutionElement& v, const SolutionElement& p);				// moves a segment of vertices from its current position in its current route, to another position (possibly in another existing route)
	void swapVertices(const SolutionElement& v1, const SolutionElement& v2);			// swaps the position of two different vertices (possibly from different routes)
	void swapVertices(int route1, int pos1, int route2, int pos2);
	void swapSegments(const SolutionElement& s1, const SolutionElement& s2);			// idem for sequences of vertices
	void invertSegment(const SolutionElement& s);										// given a sequence s of vertices (= consecutive vertices from the same route), inverts the order of the vertices in the sequence

	void 	insertWaitingVertex(const SolutionElement& w_loc, const SolutionElement& pos);	// implemented in Solution_DS_VRPTW; fails if called from class VRP_solution (why don't we use so-called "pure virtual functions" ? because otherwise we are forced to implement the functions in every derived class, not only Solution_DS_VRPTW)
	double 	removeWaitingVertex(const SolutionElement& w_vertex);							// implemented in Solution_DS_VRPTW; fails if called from class VRP_solution (why don't we use so-called "pure virtual functions" ? because otherwise we are forced to implement the functions in every derived class, not only Solution_DS_VRPTW)
	double 	increaseWaitingTime(const SolutionElement& w_vertex, double increment);		// implemented in Solution_DS_VRPTW; fails if called from class VRP_solution (why don't we use so-called "pure virtual functions" ? because otherwise we are forced to implement the functions in every derived class, not only Solution_DS_VRPTW)
	double 	decreaseWaitingTime(const SolutionElement& w_vertex, double decrement);		// implemented in Solution_DS_VRPTW; fails if called from class VRP_solution (why don't we use so-called "pure virtual functions" ? because otherwise we are forced to implement the functions in every derived class, not only Solution_DS_VRPTW)


	/* comm */
	VRP_type getType() const { return type; }
	double  getScale() const { return scale; }

	int 	getNumberRoutes() const;							// returns the number of routes in the solution
	int 	getRouteSize(int route) const;						// returns the number of vertices in the route, including the depot
	int 	getNumberNonFixedRoutes() const;					// returns the number of routes for which the ending depot is not fixed
	int 	getNumberNonEmptyRoutes() const;					// returns the number of routes containing at least one (fixed or not) vertex (EXCLUDING the depot)
	int 	getNumberModifiableRoutes() const;					// returns the number of routes containing at least one moveable vertex 
	int 	getNumberFreePostions(int route = -42) const;		// returns the number of non fixed vertices in the route (EXCLUDING the depot); if no argument, in all routes
	int 	getNumberMoveableVertices(int route = -42) const;	// returns the number of moveable (non-fixed and non-depot) vertices in the route; if no argument, in all routes
	int 	getNumberFreeWaitingVertices(int route = -42) const;// returns the number of non fixed WAITING vertices in the route; if no argument, in all routes
	int 	getNumberUNLEFTWaitingVertices(int route = -42) const;// returns the number of UNLEFT WAITING vertices in the route; if no argument, in all routes
	int 	getNumberUnvisitedWaitingVertices() const;			// returns the number of different waiting vertices that are not part of the solution
	bool 	isRouteFixed(int route) const;						// returns true iff the ending depot of the route is not fixed
	bool 	isPositionFixed(int route, int pos) const;
	double	getArrivalTimeAtElement(const SolutionElement& v) const;									// 	
	double	getServiceTimeAtElement(const SolutionElement& v) const;									// 	
	double	getDepartureTimeAtElement(const SolutionElement& v) const;									// 
	
	void 				getSolutionElementAtPosition(SolutionElement* v, int route, int pos) const; 	// given a position in a route, set v
	void 				getSolutionElementLastPositionFromRoute(SolutionElement* pos, int route) const; // set pos to the last position of given route
	const VRP_vertex&	getVertexAtElement(const SolutionElement& v) const;								// given a SolutionElement object, return the vertex associate to that position
	const VRP_vertex&	getVertexAtPosition(int route, int pos) const;									// returns the vertex that is at position pos in the given route
	const VRP_request&	getRequestAtPosition(int route, int pos) const;									// returns the request object that is associated to the vertex at position pos in the given route
	const VRP_VehicleType&	getVehicleTypeAtRoute(int route) const;										// returns the vehicle type associated to a route
	struct VertexAttr {
		bool 	visited;		// 1 if part of the solution, 0 otherwise (always 1 for REGULAR vertices)
		double 	waiting_time;	
		int 	pos;			// position of the vertex in its route (0 for the starting depot)
		int 	route;			// route (vehicle) number in which the vertex is
		double 	arrival_time;	// times are computed w.r.t. distances and vehicle velocity
		double 	service_time;
		double 	departure_time;
		bool fixed;				// tells whether the vehicle already visited the node
		bool left;				// tells whether the vehicle already visited and left the node
		bool served;			// tells whether the vehicle already started to serve the request at the node
	};
	VertexAttr			getCurrentAttributes(const VRP_vertex& v) const;


	double getTraveledDistancePerVehicleType(const VRP_VehicleType& veh) const;
	string& toString(bool verbose = false) const;
	void checkSolutionConsistency() const;

};





/* 
 * Static and Deterministic (SD) VRP's (with Time Windows): the classical one
*/
class Solution_VRPTW : public VRP_solution {
public:
	Solution_VRPTW(const VRP_instance& instance);
	Solution_VRPTW(const Solution_VRPTW &sol);		// copy constructor
	Solution_VRPTW& operator = (const Solution_VRPTW& sol);
	void 	generateInitialSolution();

	/* notify the class whenever something changed in the solution (will call the adequate methods in order to keep things up-to-date) */
	virtual void solution_changed(int route = -42);
	void update_violations(int route = -42);
	bool 	isViolated(int route, int pos) const;

	// /* differentiation functions - these are redefined to take TW violations into account */
	// void selectBestInsertionPosition(const SolutionElement& v, SolutionElement* p);		// returns the best position p to insert vertex v, according to violations and cost diffferences
	// void selectWorstMoveableVertex(SolutionElement* v);									// returns the worst planned vertex v in the solution, according to violations and cost differences

	string& toString(bool verbose = false) const;
};






// Different recourse strategies
enum RecourseStrategy {
	NOT_SET,
	LOCAL_SEARCH,	// for dynamic VRP's
	R_INFTY,		// static and stochastic only: see paper on SS-VRPTW-CR
	R_CAPA,			// static and stochastic only: see paper on SS-VRPTW-CR
	R_CAPA_PLUS, 	// static and stochastic only: see paper on SS-VRPTW-CR
	R_BASIC_INFTY, 	// static and stochastic only: see paper on SS-VRPTW-CR
	R_BASIC_CAPA, 	// static and stochastic only: see paper on SS-VRPTW-CR
	R_GSA 			// dynamic and stochastic only: see paper on DS-VRPTW
};







/* 
 * Dynamic and Stochastic (DS) VRP's (with Time Windows): the "online" problem
*/
class Solution_DS_VRPTW : public Solution_VRPTW {
protected:
	double	initialWaitingTime = 0;					// initial waiting time when inserting a waiting vertex
	// double 	waiting_time_multiple = 0.0000001;		// allowed multiples for waiting times
	double 	waiting_time_multiple = 1.0;		// allowed multiples for waiting times

	// unordered_map<const VRP_vertex*, bool> revealed_at_region;		// maintained; true if the vertex has been revealed (meaning a request occured there)
	// unordered_map<const VRP_vertex*, bool> in_solution;				// maintained; true if the vertex is part of the solution
	bool **in_solution;								// knows if an online request at some region for some time slot is part of the solution

	vector<VehicleAction> current_actions;			// not maintained
	vector<VehicleAction> next_planned_actions;		// not maintained

	void 	removeSampledOnlineRequest(int route, int pos);					// remove the REGULAR_ONLINE request at the given position; the vertex must be of type REGULAR_ONLINE, with revealed attribute still to false

public:
	void init();
	Solution_DS_VRPTW(const VRP_instance& instance);
	virtual ~Solution_DS_VRPTW();
	Solution_DS_VRPTW(const Solution_DS_VRPTW &sol);		// copy constructor
	Solution_DS_VRPTW& operator = (const Solution_DS_VRPTW& sol);
	void 	copyRouteFrom(const Solution_DS_VRPTW& sol, SolutionElement route);

	void 	generateInitialSolution();										// generate an initial solution, including all the revealed requests at regular vertices

	/* Online requests management */
	bool 	tryInsertOnlineRequest(const VRP_vertex& v, int timeslot, bool pick_first_feasible = false);	// try to insert it at a feasible position only, and return true iff it succeeded; otherwise NO insertion is performed and false is returned
	bool 	tryInsertOnlineRequest(int region, int timeslot, bool pick_first_feasible = false);				// try to insert it at a feasible position only, and return true iff it succeeded; otherwise NO insertion is performed and false is returned
	bool	tryInsertOnlineRequest(const VRP_request& req, bool pick_first_feasible = false);				// idem
	void 	removeAllSampledOnlineRequests();								// remove from the solution all the vertices that host a sampled request, i.e. vertices of type REGULAR_ONLINE with reavealed == false
	void 	removeAllSampled_violated_OnlineRequests();						// remove from the solution all the vertices that host a sampled VIOLATED (acc. to time window) request
	bool	insertFeasible(const VRP_vertex& v, SolutionElement *pos, int timeslot, bool pick_first_feasible = false);		// returns true iff there exists at least one feasible position to insert the vertex v, and record the best (if bool pick_first_feasible = false, otherwise the first feasible position found) such position (in terms of travel costs) in pos	
	bool 	insertOnlineRequest(const VRP_vertex& v, int timeslot);			// inserts a new vertex (online request of type REGULAR_ONLINE) into the solution, at best possible place; returns false if there is no feasible insertion (but it is inserted anyway !); timeslot is the time slot of the online request at the vertex
	bool 	insertOnlineRequest(const VRP_request& req);	
	void 	insertOnlineRequest(const VRP_vertex& v, SolutionElement pos, int timeslot);	// inserts a new vertex (online request of type REGULAR_ONLINE) into the solution, at the given position; 
	
	
	// int 	getNumberRevealedRequests() const;								// returns the number of REGULAR vertices that has been revealed (using addRevealedRequest)
	// int 	getNumberVisitedVertices(VRP_vertex::VertexType type) const;	// returns the number of vertex of the given type (currently, only REGULAR) that are part of the solution
	// bool	visits(const VRP_vertex& v);									// returns true iff vertex v is part of the solution
	
	/* Waiting vertices management */
	void 	selectRandomWaitingLocation(SolutionElement* wl) const;
	void 	getWaitingLocationFromRegion(SolutionElement* wl, int region) const;	// set SolutionElement wl to the corresponding waiting location at given region, but only if not visited yet
	int 	getNumberUnvisitedWaitingVertices() const;
	void 	insertWaitingVertex(const SolutionElement& w_loc, const SolutionElement& pos, double waiting_time = 0.0);	// insert a new waiting vertex corresponding to a given waiting location w_loc, at a given position in the solution
	void 	insertWaitingVertex(const VRP_vertex& v, int route, int pos, double waiting_time = 0.0);	
	double 	removeWaitingVertex(const SolutionElement& w_vertex);							// remove a non fixed waiting vertex w_vertex from the solution; returns the waiting time that was previously assigned to w_vertex 
	double 	removeWaitingVertex(int route, int pos);										
	double 	increaseWaitingTime(const SolutionElement& w_vertex, double increment);			// increases the waiting time at a UNLEFT waiting vertex (= wait vertex s.t. the vehicle didn't leave it yet); returns the increment applied waiting time
	double 	increaseWaitingTime(int route, int pos, double increment);			
	double 	decreaseWaitingTime(const SolutionElement& w_vertex, double decrement);			// decreases the waiting time at a FREE waiting vertex (= wait vertex s.t. the vehicle didn't begin to visit it yet); returns the decrement applied waiting time (may be < decrement)
	double 	decreaseWaitingTime(int route, int pos, double decrement);			
	void 	setWaitingTime(const SolutionElement& w_vertex, double w_time);	
	void 	setWaitingTime(int route, int pos, double w_time);	

	void 	setWaitingTimeMultiple(double multiple);					// use it only if available waiting times, for a vehicle at a vertex, are constrained to be only a multiple (e.g if multiple = 5, then a vehicle can wait either 5, 10, 15, etc. time units at a vertex)
	void 	setWaitingStrategy(WaitingStrategy ws);						// set the waiting strategy (a property of VRP_routes)

	void setCurrentTime(double t);	
	double getCurrentTime() const { return routes->getCurrentTime();}			
	VehicleAction getCurrentActionsVeh(int vehicle, int time=-42) const;	// returns the current actions (at given time, current time is unspecified) performed by the vehicle
	const vector<VehicleAction>& getCurrentActions(int time=-42);			// returns a reference to the vector describing the current actions (at given time, current time is unspecified) performed by each vehicle
	VehicleAction getNextPlannedActionsVeh(int vehicle, int time=-42) const;// returns the next planned actions from a given time (if unspecifiec, current time) for a vehicle
	const vector<VehicleAction>& getNextPlannedActions(int time=-42);		// returns a reference to the vector of next planned actions from a given time (if unspecifiec, current time)

};

















/* 
 * Static and Stochastic (SS) VRP's: the 2-stage stochastic version, with STOCHASTIC CUSTOMERS and STOCHASTIC REVEAL TIMES (CR)
 *
 * ATTENTION: Solution_SS_VRPTW_CR allows the use of waiting vertices (and actually, only the use of these); but a waiting vertex, as defined in the instance, can happend at most once in the solution (this is enforced by the set unvisitedWaitingVertices)
*/
class Solution_SS_VRPTW_CR : public Solution_DS_VRPTW {
public:
	struct MIPSol_Arc { 
		int i, j, p; 
		MIPSol_Arc(int i, int j, int p = -1) {this->i = i; this->j = j; this->p = p; } 
	};
	struct MIPSol_WaitTime { 
		int i, l, p; 
		MIPSol_WaitTime(int i, int l, int p = -1) {this->i = i; this->l = l; this->p = p; } 
	};
protected:

	set<VRP_request> requests_ordered;			// Based on the instance, ordered set potential requests of positive propability --> fixed by the instance, should not be recomputed

	vector< vector< vector<VRP_request>>> requests_ordered_per_waiting_locations;	// requests_ordered_per_waiting_locations[1][4][12] = r --> r is the 12th request assigned to the 4th planned waiting location of route 1
																					// requests_ordered_per_waiting_locations[1][1][1] = r --> route 1, first planned waiting location, very first request
	vector<VRP_request> unassigned_requests;										// set of unassigned requests
	

	RecourseStrategy recourseStrategy;

	/* notify the class whenever something changed in the solution (will call the adequate methods in order to keep things up-to-date) */
	virtual void solution_changed(int route = -42);

	/* request assignment and ordering */
	void 	updateRequestAssignmentOrdering();
	bool	assignment_done = false;

	/* expected cost */
	double 	expectedCost;
	void 	updateExpectedCost();
	void	updateExpectedCost_R_INFTY();
	void	updateExpectedCost_R_CAPA();
	void	updateExpectedCost_R_CAPA_PLUS();

	/* misc */
	set<const VRP_vertex*> unvisitedWaitingVertices;		// contains the set of all unvisited waiting vertices

	/* MIP Solution representation */
	struct MIPSol {
		double ***x;	// x[i][j][k]=1 iff arc (i,j) is part of the route of vehicle k 
		double ***tau;	// tau[i][l][k]=1 iff vehicle k waits for 1 ≤ l ≤ h time units at vertex i
		double **y;		// y[i][k]=1 iff vehicle k visits waiting vertex i 
		vector<vector<MIPSol_Arc>> 	subtours;
		vector<MIPSol_Arc> 			arcs;
		vector<MIPSol_WaitTime> 	wait_times;
		bool uptodate_routes;		// = true iff mipsol is uptodate with (i.e. represents exactly) the current solution's routes
		bool uptodate_waiting;		// = true iff mipsol is uptodate with (i.e. represents exactly) the current solution's waiting times
	} mipsol;

	void clearAllRoutes();
	
private:														// these stuffs are put here even if they are method attributes; that only in order to avoid reallocations each time the corresponding method is called
	vector<SolutionElement> feasibleWaitingLocations;			// used for each request in requests_ordered in turn: the vector is first cleared, then we insert all the waiting locations that satisfy the feasibility property as described in the paper, then are sorted 
	
	vector<vector<double>> hProbas, g1, g2;					// used to compute expected cost under R_INFTY / R_CAPA; h_probas[r_id][t] = h_probas for request with id_instance_file=r_id at time t (time, NOT timeslote); see paper on SS-VRPTW-CR
	
	vector<vector<vector<double>>> hProbas_Q, g1_Q, g2_Q;
	
	// vector<vector<vector<double>>> h_Qplus_w, g1_Qplus_w, g2_Qplus_w;
	// vector<vector<vector<vector<double>>>> h_Qplus, g1_Qplus, g2_Qplus;
	vector<vector<double>> h_Qplus_w__prev, g1_Qplus_w__curr, g2_Qplus_w__curr;
	vector<vector<vector<double>>> h_Qplus__prev, g1_Qplus__curr, g2_Qplus__curr;

public:
	void init(RecourseStrategy strategy, double initialWaitingTime, double waiting_time_multiple);
	Solution_SS_VRPTW_CR(const VRP_instance& instance, RecourseStrategy strategy = NOT_SET, double initialWaitingTime = -42, double waiting_time_multiple = 1);	// constructor
	Solution_SS_VRPTW_CR(const Solution_SS_VRPTW_CR& sol);																	// copy constructor
	Solution_SS_VRPTW_CR& operator = (const Solution_SS_VRPTW_CR& sol);
	void 	copyRouteFrom(const Solution_SS_VRPTW_CR& sol, SolutionElement route);

	void 	setRecourseStrategy(RecourseStrategy recourseStrategy);		// change to a new recourse strategy

	void 	generateInitialSolution(bool empty=false);
	// void 	initializeFromSol_scaleWaitingTimes(const Solution_SS_VRPTW_CR& sol, double scale);	// set a solution from an existing one (i.e. clear all routes and copy), while scaling the waiting times
	void 	copyFromSol_scaleWaitingTimes(const Solution_SS_VRPTW_CR& sol, bool try_preserve_feasibility = false);	// set a solution from an existing one (i.e. clear all routes and copy), while scaling the waiting times ACCORDING to the two solutions' scales

	/* evaluation functions */
	double 	getCost(int route = -42) const;					// returns the cost of the solution - specify route for the cost of a particular route
	int 	getNumberViolations(int route = -42) const;		// returns the number of violations in the solution - specify route for a particular route
	double 	getWeightViolations(int route = -42) const;		// returns the total weight of all violations in the solution - specify route for a particular route


	/* selection fonctions */
	void 	selectRandomWaitingLocation(SolutionElement* wl) const;
	void 	getWaitingLocationFromRegion(SolutionElement* wl, int region) const;	// set SolutionElement wl to the corresponding waiting location at given region, but only if not visited yet
	int 	getNumberUnvisitedWaitingVertices() const;

	/* altering functions */
	// the following functions perform the modification to the first stage solution AND THEN update the potential request assignment and ordering accordingly (and also the expected cost !)
	void 	insertWaitingVertex(const SolutionElement& w_loc, const SolutionElement& pos);	// insert a new waiting vertex corresponding to a given waiting location w_loc, at a given position in the solution
	double 	removeWaitingVertex(const SolutionElement& w_vertex);							// remove a non fixed waiting vertex w_vertex from the solution; returns the waiting time that was previously assigned to w_vertex 
	double 	increaseWaitingTime(const SolutionElement& w_vertex, double increment);			// increases the waiting time at a UNLEFT waiting vertex (= wait vertex s.t. the vehicle didn't leave it yet); returns the increment applied waiting time
	double 	decreaseWaitingTime(const SolutionElement& w_vertex, double decrement);			// decreases the waiting time at a FREE waiting vertex (= wait vertex s.t. the vehicle didn't begin to visit it yet); returns the decrement applied waiting time (may be < decrement)
	void 	setWaitingTime(const SolutionElement& w_vertex, double w_time);	
	// // concerning the remaining altering functions, the Solution_SS_VRPTW_CR class has the responsibility 
	// // of calling the corresponding function in the base class AND THEN updating both 
	// // 				1) the potential request assignment (by calling updateRequestAssignmentOrdering())
	// // 				2) the expected cost (by calling updateExpectedCost())
	// //	1) and 2) are now both performed by calling solution_changed() 
	// void 	moveVertex(const SolutionElement& v, const SolutionElement& p);	
	// void 	moveSegment(const SolutionElement& v, const SolutionElement& p);	
	// void 	swapVertices(const SolutionElement& v1, const SolutionElement& v2);
	// void 	swapSegments(const SolutionElement& s1, const SolutionElement& s2);
	// void 	invertSegment(const SolutionElement& s);


	/* communication */
	const vector< vector< vector<VRP_request>>> & getRequestAssignment() const;							// returns the assignment in format a[route][pos]: ordered set of requests assigned to waiting vertex at pos in route
	const vector<VRP_request> & getUnassignedRequests() const;
	const set<VRP_request> & getOrderedRequests() const;												// returns the ordered set of requests (no assignment)
	double 	t_min(const VRP_request& r, const SolutionElement& v) const;								// See paper SS-VRPTW-CR
	double 	t_max(const VRP_request& r, const SolutionElement& v) const;								// idem
	double 	t_min_Rplus(const VRP_request& r, const VRP_vertex& v, const SolutionElement& w_r) const;	// idem
	double 	t_max_Rplus(const VRP_request& r, const VRP_vertex& v, const SolutionElement& w_r) const;	// idem
	bool	satisfiable(const VRP_request& r, int t, const SolutionElement& w, int q) const;
	bool	satisfiable_Rplus(const VRP_request& r, int t, const VRP_vertex& v, int q, const SolutionElement& w_r) const;

	/* request assignment and ordering */
	void 	print_OrderedSet_requests() const;
	void 	print_requestsAssignmentOrdering() const;
	string& toString_gProbas();
	string& toString_hProbas();
	string& toString_h_v(int route, int request_id, int n);

	/* MIP Solution */
	void 	mipsol_setX(int i, int j, const double *vals);
	void 	mipsol_setTAU(int i, int l, const double *vals);
	void 	mipsol_setY(int i, const double *vals);
	void 	mipsol_setX(int i, int j, int p, double val);
	void 	mipsol_setTAU(int i, int l, int p, double val);
	void 	mipsol_setY(int i, int p, double val);
	void 	update_from_mipsol();										// updates the solution routes and waiting times according to current values of mipsol
	void 	update_routes_from_mipsol();								// updates only the routes (avoids the waiting times, and therefore avoids recomputing requests assignment and expected cost)
	void 	update_waitingTimes_from_mipsol();					
	const vector<vector<MIPSol_Arc>> &  mipsol_getSubtours() const;	// returns the number of subtours in the current mipsol, that is tours without the depot
	const vector<MIPSol_Arc> & 			mipsol_getArcs() const;			// return the set of tuples (i,j,p) such that arc (i,j) is =1 in mipsol
	const vector<MIPSol_WaitTime> & 	mipsol_getWaitTimes() const;		// return the set of tuples (i,l,p) such that tau[i][l][p]=1 in mipsol

	/* misc debug */
	void 	print_unvisitedWaitingVertices() const;
};




#endif

