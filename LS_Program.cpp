/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* LS_Program +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See LS_Program.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

#include <iostream>
#include <iomanip>	// setprecision
#include <cmath>
#include <ctime>	// clock()
#include <climits>	// numeric_limits

#include "NM.h"
#include "tools.h"


template <class Solution, class NM>
LS_Program<Solution, NM>::LS_Program() {
	timeout_sec = numeric_limits<int>::max();
}

template <class Solution, class NM>
void LS_Program<Solution, NM>::setTimeOut(double cpuSeconds) {
	timeout_sec = cpuSeconds;
}

template <class Solution, class NM>
bool LS_Program<Solution, NM>::terminationCondition() const {
	return (iter >= max_iter) || (elapsed_time >= timeout_sec);
}

template <class Solution, class NM>
int LS_Program<Solution, NM>::run(Solution& solution, NM &nm, bool verbose){
	bool 		callbackStop = false;	
	double 		last_elapsed_time_print = 0.0, last_elapsed_time_callback = 0.0;
	clock_t		start_clock = clock();
	string 		action = "Start";
	

	Solution 	bestSolution = solution;
	Solution 	incumbentSolution = solution;			
	Solution& 	neighborSolution = solution;			
	double incumbent_eval = 0.0;
	if (evaluation_callback) incumbent_eval = evaluation_callback(solution);
	else incumbent_eval = computeSolutionEval(solution);
	double best_eval = incumbent_eval;
	// cout << endl << "Start LS:" << endl << bestSolution.toString(true) << endl;

	iter = 0; 
	current_distance = 0;
	elapsed_time = 0.0;

	if (verbose)
		cout << fixed << "CPU Time           Best      Iteration         Action" << endl;

	// --- Search loop
	while (!terminationCondition() && !callbackStop) {
		iter++;
		
		elapsed_time = (double) (clock() - start_clock) / CLOCKS_PER_SEC;



		if (verbose && (action != "" || elapsed_time >= (last_elapsed_time_print + 10)) ) {
			cout << setw(8)  << setprecision(1) << elapsed_time;
			if (bestSolution.getNumberViolations() == 0) {
				cout << setw(15) << setprecision(4) << best_eval; 
			}
			else {
				cout << "     Infeasible:" << setw(4) << bestSolution.getNumberViolations(); 
				// cout << endl << bestSolution.toString() << endl << "violation w.: " << bestSolution.getWeightViolations() << endl;
			}
			cout << setw(15) << iter;
			cout << setw(15) << action;
			cout << endl;
			last_elapsed_time_print = elapsed_time;
			action  = "";
		}

		nm.shakeSolution();
		// cout << "neighborSolution violation w.: " << neighborSolution.getWeightViolations() << endl;
		
		double neighbor_eval = 0.0; 
		if (evaluation_callback) neighbor_eval = evaluation_callback(neighborSolution);
		else neighbor_eval = computeSolutionEval(neighborSolution);
		// cout << "Neighbod eval: " << setw(10) << neighbor_eval << "   Incumbent eval: " << setw(10) << incumbent_eval << "   Best eval: " << setw(10) << best_eval << endl;
		// cout << endl << neighborSolution.toString(true) << endl << neighborSolution.getNumberViolations() << endl << neighborSolution.getWeightViolations() << endl;

		// if (acceptanceCriterion(neighborSolution, incumbentSolution)) {
		if (acceptanceCriterion(neighbor_eval, incumbent_eval)) {
			nm.neighborAccepted();					// inform the NM that the last operation was a success
			incumbentSolution = neighborSolution;
			incumbent_eval = neighbor_eval;
			current_distance++;
			// nm.intensify();
			if (new_incumbentSolution_callback)
				_ASSERT_(new_incumbentSolution_callback(incumbentSolution) == NONE, "Callback return type must be NONE");

			// if(computeSolutionEval(incumbentSolution) < computeSolutionEval(bestSolution)) {
			// if (evaluation_callback) best_eval = evaluation_callback(bestSolution);
			// else best_eval = computeSolutionEval(bestSolution);
			if(incumbent_eval < best_eval) {
				// if (incumbentSolution.getNumberViolations() < bestSolution.getNumberViolations() || incumbentSolution.getNumberViolations() == 0)
				if (incumbentSolution.getNumberViolations() == 0)
					action = "New best";
				bestSolution = incumbentSolution;
				best_eval = incumbent_eval;
				
				current_distance = 0;
				improvement();
				if (new_bestSolution_callback)
					_ASSERT_(new_bestSolution_callback(bestSolution, best_eval) == NONE, "Callback return type must be NONE");
			}
		}
		else {
			nm.restorePreviousSolution(incumbentSolution, false);	// usually cheaper than doing "neighborSolution = incumbentSolution"
			nm.diversify();
		} 
	
		if(shouldWeRestart()) {						// restart from best solution if gone too far away
			// action = "Restart";
			incumbentSolution = bestSolution; 
			neighborSolution = bestSolution;
			incumbent_eval = best_eval;
			current_distance = 0;
			// nm.intensify();
			restart();								// informs the LS engine meta heuristic that a restart operated
			if (restart_callback)
				_ASSERT_(restart_callback(bestSolution) == NONE, "Callback return type must be NONE");
		}

		iterationDone();
		if (end_iteration_callback) {
			int r = end_iteration_callback(bestSolution, best_eval, incumbentSolution, incumbent_eval);
			switch (r) {
				case STOP_LS: 	callbackStop = true; break;
				case NONE:		break;
				default : _ASSERT_(false, "not implemented");
			}
		}

		if (time_callback && elapsed_time >= (last_elapsed_time_callback + time_callback_trigger)) {
			int r = time_callback(bestSolution, best_eval, incumbentSolution, incumbent_eval, elapsed_time);
			switch (r) {
				case STOP_LS: 	callbackStop = true; break;
				case NONE:		break;
				default : _ASSERT_(false, "not implemented");
			}
			last_elapsed_time_callback = elapsed_time;
		}
	}
	solution = bestSolution;
	// cout << "ITER = " << iter << endl;
	return iter;
}













