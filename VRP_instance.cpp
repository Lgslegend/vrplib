/*
Copyright 2017 Michael Saint-Guillain.

This file is part of the library VRPlib.

VRPlib is free library: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License (LGPL) as 
published by the Free Software Foundation, either version 3 of the 
License, or (at your option) any later version.

VRPlib is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License v3 for more details.

You should have received a copy of the GNU Lesser General Public License
along with VRPlib. If not, see <http://www.gnu.org/licenses/>.
*/




/* VRP_instance +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_instance.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
#include <iostream>
#include <vector>
// #include <pair>
#include <set>
#include <cmath>
#include "VRP_instance.h"
#include "VRP_routes.h"
#include "tools.h"

using namespace std;

// VRP_instance::VRP_instance(int nRegions, int nVeh, int Q, double vehVelocity, int H, int nTimeSlots, int orig_horizon, double scale) {
VRP_instance::VRP_instance(int nRegions, int H, int nTimeSlots, int orig_horizon, double scale) {
	ASSERT(nRegions > 0, "argh");	// cout << "Instance " << this << " created    nRegions=" << nRegions << endl;
	this->nRegions 		= nRegions;
	// this->nVeh 			= nVeh;
	// this->Q 			= Q;
	// this->vehVelocity 	= vehVelocity;
	this->H 			= H;
	this->nTimeSlots	= nTimeSlots;
	this->orig_horizon	= orig_horizon;
	this->scale			= scale;

	for (int i=0; i<MAX_ID; i++) id_instance_file_to_region[i] = -42;

	// initialize distance related vectors
	coordX.assign(nRegions, 0.0);
	coordY.assign(nRegions, 0.0);

	for (int v1=0; v1 < nRegions; v1++) 
		online_requests_appearing_times.push_back(vector<int>(nTimeSlots+1, numeric_limits<int>::max()));

}

VRP_VehicleType& VRP_instance::addVehicleType(string name, int capacity, double velocity, double costPerDistanceUnit) {
	VRP_VehicleType* new_type = new VRP_VehicleType(nRegions, name, capacity, velocity, costPerDistanceUnit);
	// for (const VRP_VehicleType& veh_type : vehicle_types)
	// 	_ASSERT_(veh_type != new_type, "");
	vehicle_types.insert(new_type);
	return *new_type;
}

const VRP_VehicleType& VRP_instance::getVehicleType(string name) const {
	for (const VRP_VehicleType* veh_type : getVehicleTypes()) {
		if (veh_type->getName() == name) return *veh_type;		
	}	
	_ASSERT_(false, "Vehicle type \"" << name << "\" does not exists");
}

VRP_vertex& VRP_instance::addVertex(int region, VRP_vertex::VertexType type, int instance_id) {
	_ASSERT_(type != VRP_vertex::NO_TYPE, "argh");
	_ASSERT_(instance_id < MAX_ID, "");
	if (instance_id >= 0)
		id_instance_file_to_region[instance_id] = region;
	VRP_vertex* pvertex = new VRP_vertex(region, type, nTimeSlots, instance_id);
	// int id_instance_file = region;
	// if (instance_id != -42)
	// 	id_instance_file = instance_id;
	// VRP_vertex* pvertex = new VRP_vertex(region, type, nTimeSlots, id_instance_file);

	switch(type) {
		case VRP_vertex::DEPOT :
			depot_vertices.insert(pvertex);
			break;
		// case VRP_vertex::UNLOAD_DEPOT :
		// 	u_depot_vertices.insert(pvertex);
		// 	break;
		case VRP_vertex::REGULAR :
			reg_vertices.insert(pvertex);
			break;
		case VRP_vertex::CUSTOMER :
			cust_vertices.insert(pvertex);
			break;
		case VRP_vertex::WAITING :
			wait_vertices.insert(pvertex);
			break;
		case VRP_vertex::REGULAR_ONLINE :
			reg_online_vertices.insert(pvertex);
			for (int ts = 0; ts <= nTimeSlots; ts++) 
				pvertex->getRequest_mutable(ts).appeared = false;
			break;
		default :
			_ASSERT_(false, "wrong vertex type " << type);
	}
	return *pvertex;
}

// VRP_vertex& VRP_instance::addOnlineVertex(int region, VRP_vertex::VertexType type, int instance_id) {
// 	ASSERT(type == VRP_vertex::REGULAR_ONLINE, "argh");
// 	int id_instance_file = region;
// 	if (instance_id != -42)
// 		id_instance_file = instance_id;

// 	VRP_vertex* pvertex = new VRP_vertex(region, type, nTimeSlots, id_instance_file);
// 	reg_online_vertices.insert(pvertex);

// 	return *pvertex;
// }


int  VRP_instance::getNumberRegions() const {
	return nRegions;
}
int  VRP_instance::_getNumberVehicles() const {
	ASSERT(_nVeh > 0, "");
	return _nVeh;
}
// // void  VRP_instance::setNumberVehicles(int n) {
// // 	ASSERT(n >= 1, "Argh! n =" << n);
// // 	nVeh = n;
// // }
// int  VRP_instance::_getVehicleCapacity() const {
// 	ASSERT(_Q >= 0, "");
// 	return _Q;
// }
// // void  VRP_instance::setVehicleCapacity(int capa) {
// // 	ASSERT(capa >= 0, "Argh! capacity =" << capa);
// // 	Q = capa;
// // }
// int  VRP_instance::_getVehicleVelocity() const {
// 	ASSERT(_vehVelocity > 0, "");
// 	return _vehVelocity;
// }
int  VRP_instance::getHorizon() const {
	return H;
}
int  VRP_instance::getNumberTimeSlots() const {
	return nTimeSlots;
}
int  VRP_instance::getNumberPotentialRequests() const {
	return nPotentialRequests;
}



int VRP_instance::getNumberVertices(VRP_vertex::VertexType type) const {

	switch(type) {
		case VRP_vertex::DEPOT :
			return depot_vertices.size();
		case VRP_vertex::REGULAR :
			return reg_vertices.size();
		case VRP_vertex::CUSTOMER :
			return cust_vertices.size();
		case VRP_vertex::WAITING :
			return wait_vertices.size();
		default :
			_ASSERT_(false, "wrong vertex type " << type);
	}
	return 0;
}


VRP_vertex* VRP_instance::getVertexFromRegion_mutable(VRP_vertex::VertexType type, int region) const { 
	ASSERT(region < nRegions, "region " << region << " does not exist (nRegions=" << nRegions << ") instance:" << this);

	const set<VRP_vertex*> * pset = nullptr;
	switch(type) {
		case VRP_vertex::DEPOT :
			pset = & depot_vertices;
			break;
		// case VRP_vertex::UNLOAD_DEPOT :
		// 	pset = & u_depot_vertices;
		// 	break;
		case VRP_vertex::REGULAR :
			pset = & reg_vertices;
			break;
		case VRP_vertex::CUSTOMER :
			pset = & cust_vertices;
			break;
		case VRP_vertex::WAITING :
			pset = & wait_vertices;
			break;
		case VRP_vertex::REGULAR_ONLINE :
			pset = & reg_online_vertices;
			// for (VRP_vertex* v : reg_online_vertices[ts]) {
			// 	// cout << "found a vertex of type " << v->type << " at region " << v->region << endl;
			// 	if (v->region == region)
			// 		return v;
			// }
			// _ASSERT_(false, "vertex not found !");
			break;
		default :
			_ASSERT_(false, "wrong vertex type " << type);
	}

	for (VRP_vertex* v : *pset) {
		// cout << "found a vertex of type " << v->type << " at region " << v->region << endl;
		if (v->region == region)
			return v;
	}

	// for (const VRP_vertex* v : *pset) cout << v->toString() << " ";
	_ASSERT_(false, "vertex from region " << region << " not found ! ");
	return NULL;
}

const VRP_vertex* VRP_instance::getVertexFromRegion(VRP_vertex::VertexType type, int region) const { 
	return getVertexFromRegion_mutable(type, region);
}

const VRP_vertex* VRP_instance::getVertexFromInstanceFileID(VRP_vertex::VertexType type, int instance_id) const {
	// cout << "id_instance_file_to_region[" << instance_id << "] =" << id_instance_file_to_region[instance_id] << endl;
	return getVertexFromRegion(type, id_instance_file_to_region[instance_id]);
}


void VRP_instance::set_OnlineRequest_asAppeared(int region, int timeSlot) {
	ASSERT(0 <= timeSlot && timeSlot <= nTimeSlots, "");
	getVertexFromRegion_mutable(VRP_vertex::REGULAR_ONLINE, region)->getRequest_mutable(timeSlot).setAppeared();
}
void VRP_instance::set_OnlineRequest_asAppeared(const VRP_request& r) {
	set_OnlineRequest_asAppeared(r.getVertex().getRegion(), r.revealTS);
}

void VRP_instance::addAppearingOnlineRequest(int region, int timeSlot, int revealTime) {
	const VRP_vertex & vertex = * getVertexFromRegion(VRP_vertex::REGULAR_ONLINE, region);
	_ASSERT_(revealTime > 0 && revealTime <= H, "");
	_ASSERT_(vertex.getRequest(timeSlot).revealTime_min <= revealTime && revealTime <= vertex.getRequest(timeSlot).revealTime_max, "");
	_ASSERT_(region <= nRegions-1, region << " >= " << nRegions);
	_ASSERT_(0 < timeSlot && timeSlot <= nTimeSlots, timeSlot << " > " << nTimeSlots);
	
	bool already_there = false;
	for (auto& req : online_requests_appeared) {
		if (req.vertex->getRegion() == region && req.request->revealTS == timeSlot) {
			cout << "Request at region " << region << " (vertex instance_id: " << region-1 << ") duplicated !! Ignored..." << endl;
			already_there = true;
		}
	}
	_ASSERT_(! already_there, "request already in the set of appeared requests !");

	RequestAttributes req;
	req.vertex = & vertex;
	req.request = & vertex.getRequest(timeSlot);
	req.reveal_time = revealTime;
	req.time_slot = timeSlot;
	online_requests_appeared.push_back(req);

	_ASSERT_(region < (int) online_requests_appearing_times.size() && timeSlot < (int) online_requests_appearing_times[0].size(), "size: " << online_requests_appearing_times.size() << " x " << online_requests_appearing_times[0].size());
	online_requests_appearing_times[region][timeSlot] = revealTime;
}

bool VRP_instance::revealedAtTime(const VRP_request& r, int time) const {
	if (r.revealTime_max <= time) return true;
	if (time < r.revealTime_min)  return false;
	return online_requests_appearing_times[r.getVertex().getRegion()][r.revealTS] <= time;
}

void VRP_instance::setIntegerTravelTimes() {
	for (VRP_VehicleType* veh_type : vehicle_types)
		veh_type->integer_travel_times = true;
}


string& VRP_instance::toString(bool verbose) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);
	
	out << "Instance:"
		<< "   nRegions: " << nRegions << "(";
	if (depot_vertices.size()) out << "#depot:" << depot_vertices.size();
	if (reg_vertices.size()) out << ", #regular:" << reg_vertices.size();
	if (cust_vertices.size()) out << ", #customer:" << cust_vertices.size();
	if (wait_vertices.size()) out << ", #waiting:" << wait_vertices.size();
	out	<< ")   nVeh: " << _nVeh;
	if (H == numeric_limits<int>::max())
		out << 	"   H: +inf";
	else	
		out	<< "   H: " << H;
	out	<< "   nTimeSlots: " << nTimeSlots 
		<< "   (orig. H: " << orig_horizon << ", scale: " << scale << ")"
		<< endl;

	if (verbose) {}

	static string str = ""; str = out.str();
	return (str);
}


string& VRP_instance::toStringCoords(bool and_attributes) const {
	ostringstream out, inf("inf");
	out.setf(std::ios::fixed);
	out.precision(2);

	out << outputMisc::greenExpr(true) << "         x     y";
	if (and_attributes)
			out << "     q     s     e     l";
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(4) << i << "   " << outputMisc::resetColor();
		out << setw(4) << (int) coordX[i] << "  " << setw(4) << (int) coordY[i] << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}


string& VRP_instance::toStringProbasTS() const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	out << outputMisc::greenExpr(true) << "TS:  "; 
	for (int ts=1; ts < nTimeSlots+1; ts++) 
		out << setw(3) << ts << "  "; 
	out << endl << "V:" << endl << outputMisc::resetColor() ;

	for (const VRP_vertex* pvertex : getCustomerVertices()) {
		out << outputMisc::greenExpr(true) << setw(3) << pvertex->region << "   " << outputMisc::resetColor();
		for (int ts=1; ts < nTimeSlots+1; ts++) {
			VRP_request& req = pvertex->requests_ts[ts];
			out << outputMisc::redExpr(req.p > 0) << setw(2) << (int) req.p << outputMisc::resetColor() << setw(3) << "  ";
		}
		out << endl;
	}

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toStringPotentialRequests() const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	for (const VRP_vertex* pvertex : getCustomerVertices()) {
		for (int ts=1; ts < nTimeSlots+1; ts++) {
			VRP_request& req = pvertex->requests_ts[ts];
			if (req.p > 0)
				out << req.toString(true) << endl;
		}
	}

	static string str = ""; str = out.str();
	return (str);
}


string& VRP_instance::toStringPotentialOnlineRequests() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	int n = 0;
	double expected = 0.0;
	vector<double> exp_ts(nTimeSlots+1, 0.0);
	out << "Online requests probabilities (\%):";
	for (int ts=1; ts <= nTimeSlots; ts++) out << "     TS" << setw(2) << ts;
	out << endl;
	for (const VRP_vertex* pvertex : getOnlineVertices()) {
		double exp_nb_req = 0.0;
		for (int ts=1; ts <= nTimeSlots; ts++) {
			VRP_request& req = pvertex->requests_ts[ts];
			exp_nb_req += req.p / 100.0;
			exp_ts[ts] += req.p / 100.0;
		}
		if (exp_nb_req > 0) {
			n++;
			out << "                      " << pvertex->toString() << "(" << setw(5) << pvertex->getIdInstance() << "):    ";
			for (int ts=1; ts < nTimeSlots+1; ts++) {
				VRP_request& req = pvertex->requests_ts[ts];
				out << setw(6) << setprecision(2) << req.p << "   ";
			} out << endl;
		}
		expected += exp_nb_req;
	}
	out << "E[#requests] = " << expected << "   (" << n << " potential locations, out of " << getOnlineVertices().size();
	for (int ts=1; ts <= nTimeSlots; ts++) out << "   TS" << setw(2) << ts << ":" << exp_ts[ts];
	out << endl;
	static string str = ""; str = out.str();
	return (str);
}



string& VRP_instance::toStringInstanceMetrics() const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	// out << get_customer_vertices().size() << endl;
	for (auto veh_type : getVehicleTypes()) {
		out << "Metrics for vehicle type 1 " << veh_type->toString() << ":" << endl; 
		double avg_dist = 0.0;
		for (const VRP_vertex* pv_1 : getCustomerVertices())
			for (const VRP_vertex* pv_2 : getCustomerVertices())
				avg_dist += veh_type->travelTime(*pv_1, *pv_2) / (getCustomerVertices().size() * getCustomerVertices().size());
		out << "Average travel time between customer regions: " << avg_dist << endl;
		
		avg_dist = 0.0;
		for (const VRP_vertex* pw : getWaitingVertices())
			for (const VRP_vertex* pv : getCustomerVertices())
				avg_dist += veh_type->travelTime(*pw, *pv) / (getWaitingVertices().size() * getCustomerVertices().size());
		out << "Average travel time between customer regions and waiting locations: " << avg_dist << endl;


		double exp_tw = 0.0;
		for (const VRP_vertex* pv : getCustomerVertices()) {
			double exp_cust_tw = 0.0;
			for (int ts =1; ts <= nTimeSlots; ts++) {
				// cout << "ts:" << ts << "\t" << pv->getRequest(ts).e << " \t" << pv->getRequest(ts).l << " \t" << pv->getRequest(ts).p << endl;
				exp_cust_tw += (pv->getRequest(ts).l - pv->getRequest(ts).e +1) * pv->getRequest(ts).p/100 / 2;
			}
			exp_tw += exp_cust_tw / getCustomerVertices().size();
		}
		out << "Expected time window size: " << exp_tw << endl;
	}

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toStringVehicleTypes(bool verbose) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	for (auto* veh_type : getVehicleTypes())
		out << veh_type->toString(verbose) << endl;

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::toString_AppearingOnlineRequests(bool verbose) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	for (auto& req_attr : online_requests_appeared)
		out << req_attr.toString(verbose) << endl;

	static string str = ""; str = out.str();
	return (str);
}

string& VRP_instance::RequestAttributes::toString(bool verbose) const {
	ostringstream out; 
	out.setf(std::ios::fixed);

	out << "\tRequest at vertex " << vertex->toString(verbose) << " (instance_id: " << vertex->getIdInstance() << ", time slot " << setw(3) << request->revealTS << ") appears at time " << reveal_time; 

	static string str = ""; str = out.str();
	return (str);
}
















/* VRP_vertex +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_instance.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/
VRP_vertex::VRP_vertex(int region, VertexType type, int nTimeSlots, int instance_id) {
	this->region = region;
	this->id_instance_file = instance_id;
	this->type = type;
	this->nTimeSlots = nTimeSlots;
	if (type == VRP_vertex::CUSTOMER || type == VRP_vertex::REGULAR_ONLINE) {
		this->requests_ts = new VRP_request[nTimeSlots+1]; 
		for (int ts=0; ts <= nTimeSlots; ts++) 
			requests_ts[ts].vertex = this;
	}
	else
		this->requests_ts = new VRP_request(this); 

	// cout << "Creating vertex at region " << region << "(instance id: " << instance_id << ") of type " << type << endl;
}

bool VRP_vertex::operator== (const VRP_vertex& v) const {
	return region == v.region && id_instance_file == v.id_instance_file && type == v.type && nTimeSlots == v.nTimeSlots;
}

const VRP_request& VRP_vertex::getRequest(int timeSlot) const {
	ASSERT(timeSlot >= 0, "argh");
	ASSERT(!(timeSlot > 0) || (type == VRP_vertex::CUSTOMER || type == VRP_vertex::REGULAR_ONLINE), "only CUSTOMER or REGULAR_ONLINE vertex type has time slots !");
	if (timeSlot > 0) ASSERT(timeSlot <= nTimeSlots, "time slot=" << timeSlot << "), type=" << type);
	return requests_ts[timeSlot];
}
const VRP_request& VRP_vertex::getRequest() const {
	ASSERT(type != VRP_vertex::CUSTOMER && type != VRP_vertex::REGULAR_ONLINE, "REGULAR_ONLINE (or CUSTOMER) vertex must take a timeslot argument !");
	return getRequest(0);
}

VRP_request& VRP_vertex::getRequest_mutable(int timeSlot) {
	ASSERT(timeSlot >= 0, "argh");
	if (timeSlot > 0)
		ASSERT(timeSlot <= nTimeSlots && (type == VRP_vertex::CUSTOMER || type == VRP_vertex::REGULAR_ONLINE) , "time slot=" << timeSlot << "), type=" << type);
	return requests_ts[timeSlot];
}
VRP_request& VRP_vertex::getRequest_mutable() {
	ASSERT(type != VRP_vertex::CUSTOMER, "argh");
	return requests_ts[0];
}

string& VRP_vertex::toString(bool verbose) const {
	ostringstream out;
	out.setf(std::ios::fixed);
	out.precision(2);
	if (! verbose || type == VRP_vertex::WAITING || type == VRP_vertex::CUSTOMER) {
		switch(type) {
			case VRP_vertex::DEPOT: out << "D"; break;
			case VRP_vertex::REGULAR: out << "R"; break;
			case VRP_vertex::CUSTOMER: out << "C"; break;
			case VRP_vertex::WAITING: out << "W"; break;
			case VRP_vertex::REGULAR_ONLINE: out << "O"; break;
			default: out << "?";
		}

		// if (type == VRP_vertex::REGULAR_ONLINE && )

		out << setfill('0') << setw(3) << region; 
	}
	if (verbose && (type == VRP_vertex::REGULAR || type == VRP_vertex::REGULAR_ONLINE || type == VRP_vertex::DEPOT)) {
		out << getRequest(0).toString(true);
	}

	static string str = ""; str = out.str();
	return (str);
}

bool VRP_vertex::checkConsistency() const {
	// TODO
	return true;
}
















/* VRP_request +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ 
	See VRP_instance.h for description

	Author: Michael Saint-Guillain <michael.saint@uclouvain.be>
*/

bool VRP_request::operator==(const VRP_request& other) const {
	return id_instance_file == other.id_instance_file && id == other.id;
}

bool VRP_request::checkConsistency() const {
	ASSERT(e <= l, "");
	return true;
}

void VRP_request::setAppeared() {
	ASSERT(vertex->getType() == VRP_vertex::REGULAR_ONLINE, "argh");
	ASSERT(!appeared, "request" << toString(true) << "was already set as appeared !");
	appeared = true;
}


bool VRP_request::hasAppeared() const {
	ASSERT(vertex->getType() == VRP_vertex::REGULAR_ONLINE, "argh");
	return appeared;
}

string& VRP_request::toString(bool verbose) const {
	ostringstream out, tmp, inf;
	out.setf(std::ios::fixed);
	out.precision(2);
	if (!verbose) {
		if (vertex->type == VRP_vertex::CUSTOMER)
			out << setw(3) << id << "[" << setw(3) << vertex->region << ":" << setfill('0') << setw(2) << revealTS << "]"; // << "-" << setfill('0') << setw(3) << (int)revealTime << "]";
		if (vertex->type == VRP_vertex::REGULAR_ONLINE) 
			out << "O" << revealTS << "_" << setfill('0') << setw(3) << vertex->region;
		else
			out << "v:" << vertex->toString();
	}
	else {
		tmp << (int) l; inf << "inf";
		out 
			// << vertex->id_instance_file
			<< "["
			// << "id:" << setw(4) << id_instance_file 
			// << ", " << vertex->toString() 
			<< "e:" << setw(4) << (int) e 
			<< ",l:" << setw(4) << ( (l < numeric_limits<int>::max()) ? tmp.str() : inf.str())
			<< ",s-:" << setw(3) << (int) add_service_time
			<< ",q:" << setw(3) << (int) demand
			// << ",t:" << setw(5) << (int) revealTime 
			;
		if (vertex->getType() == VRP_vertex::REGULAR_ONLINE || vertex->getType() == VRP_vertex::CUSTOMER)
			out 
				<< ",ts:"<< setw(3) << (int) revealTS 
				<< ",p:" << setw(3) << (int) p; 
		out << "]";
	} 

	

	static string str = ""; str = out.str();
	return (str);
}





























VRP_VehicleType::VRP_VehicleType(int nRegions, string name, int capacity, double velocity, double costPerDistanceUnit) {
	ASSERT(nRegions > 0, "");
	this->nRegions = nRegions;

	this->name = name; 
	this->capacity = capacity; 
	this->velocity = velocity; 
	this->costPerDistanceUnit = costPerDistanceUnit;

	for (int v1=0; v1 < nRegions; v1++) {
		dist.push_back(vector<double>(nRegions, 0.0));
		travel_times.push_back(vector<double>(nRegions, 0.0));
	}
	service_times.assign(nRegions, 0.0);
}

bool VRP_VehicleType::operator == (const VRP_VehicleType& other) const {
	return nRegions == other.nRegions && name == other.name && capacity == other.capacity && velocity == other.velocity && costPerDistanceUnit == other.costPerDistanceUnit;
}



void VRP_VehicleType::setServiceTime(int region, double t) {
	_ASSERT_(region < nRegions && region >= 0, "argh");
	_ASSERT_(0 <= t, "argh");
	service_times[region] = t;
}



void VRP_VehicleType::preComputeEuclideanDistances(const vector<double>& coordX, const vector<double>& coordY) {
	// cout << nRegions << endl;
	for (int v1=0; v1 < nRegions; v1++) 
		for (int v2=0; v2 < nRegions; v2++) {
			dist[v1][v2] = ( sqrt(  pow(static_cast<double>(coordX[v1] - coordX[v2]),2) 
						 		  + pow(static_cast<double>(coordY[v1] - coordY[v2]),2) ) 
							);
		}
		
}

void VRP_VehicleType::setTravelDistance(int region1, int region2, double d) {
	_ASSERT_(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	_ASSERT_(0 <= d, "argh");
	dist[region1][region2] = d;
}

void VRP_VehicleType::setTravelTime(int region1, int region2, double t) {
	_ASSERT_(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	_ASSERT_(0 <= t, "argh");
	travel_times[region1][region2] = t;
	travel_times_NE_distances = true;
}


double 	VRP_VehicleType::distance(int region1, int region2) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	return dist[region1][region2]; 
}
double 	VRP_VehicleType::distance(const VRP_vertex& v1, const VRP_vertex& v2) const { 
	return dist[v1.getRegion()][v2.getRegion()]; 
}
double 	VRP_VehicleType::travelTime(const VRP_vertex& v1, const VRP_vertex& v2) const { 
	return travelTime(v1.getRegion(), v2.getRegion());
}	
double 	VRP_VehicleType::travelTime(int region1, int region2) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	if (travel_times_NE_distances)
		return travel_times[region1][region2];

	if (integer_travel_times)
		return ceil(dist[region1][region2] / velocity); 
	else
		return dist[region1][region2] / velocity; 
}	
double 	VRP_VehicleType::travelCost(const VRP_vertex& v1, const VRP_vertex& v2) const { 
	return travelCost(v1.getRegion(), v2.getRegion());
}	
double 	VRP_VehicleType::travelCost(int region1, int region2) const { 
	ASSERT(region1 < nRegions && region2 < nRegions && region1 >= 0 && region2 >= 0, "argh");
	return dist[region1][region2] * costPerDistanceUnit; 
}	


double 	VRP_VehicleType::totalServiceTime(const VRP_request& r) const { 
	return serviceTime(r.getVertex().getRegion()) + r.add_service_time;
}	
double 	VRP_VehicleType::serviceTime(const VRP_vertex& v) const { 
	return serviceTime(v.getRegion());
}	
double 	VRP_VehicleType::serviceTime(int region) const { 
	ASSERT(region < nRegions && region >= 0, "argh");
	return service_times[region]; 
}	


string& VRP_VehicleType::toString(bool verbose) const {
	UNUSED(verbose);
	ostringstream out; 
	// out.setf(std::ios::fixed);
	// out.precision(2);
	out << "\"" << name << "\"[Q=" << capacity << ",v=" << velocity << ",c=" << costPerDistanceUnit << "]";
	static string str = ""; str = out.str();
	return (str);
}


string& VRP_VehicleType::toStringDistances() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);

	out << outputMisc::greenExpr(true) << "        "; 
	for (int j=0; j<nRegions; j++) 
		out << setw(2) << j << "     "; 
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(2) << i << "   " << outputMisc::resetColor();
		for (int j=0; j<nRegions; j++)
			out << setw(5) << distance(i,j) << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}

string& VRP_VehicleType::toString_TravelTimes() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);

	out << outputMisc::greenExpr(true) << "        "; 
	for (int j=0; j<nRegions; j++) 
		out << setw(2) << j << "     "; 
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(2) << i << "   " << outputMisc::resetColor();
		for (int j=0; j<nRegions; j++)
			out << setw(5) << travelTime(i,j) << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}
string& VRP_VehicleType::toString_TravelCosts() const {
	ostringstream out; 
	out.setf(std::ios::fixed);
	out.precision(1);

	out << outputMisc::greenExpr(true) << "        "; 
	for (int j=0; j<nRegions; j++) 
		out << setw(2) << j << "     "; 
	out << endl << endl << outputMisc::resetColor() ;
	for (int i=0; i<nRegions; i++) {	
		out << outputMisc::greenExpr(true) << setw(2) << i << "   " << outputMisc::resetColor();
		for (int j=0; j<nRegions; j++)
			out << setw(5) << travelCost(i,j) << "  ";
		out << endl;
	}
	static string str = ""; str = out.str();
	return (str);
}









